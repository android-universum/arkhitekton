@Arkhitekton-Presentation
===============

This module groups the following modules into one **single group**:

- [Presentation-Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core)
- [Presentation-Base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-base)
- [Presentation-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-reactive)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-presentation:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-data-reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-reactive),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/master/library-view)