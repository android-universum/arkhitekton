/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Global registry providing reactive schedulers for **interaction** elements.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see InteractionSchedulers.primaryScheduler
 */
object InteractionSchedulers {

    /**
     * Scheduler that should be used for primary interactions.
     */
    private var primaryScheduler: Scheduler = Schedulers.computation()

    /**
     * Specifies a scheduler that should be used for primary interaction logic.
     *
     * @param scheduler The desired scheduler to be used across library.
     *
     * @see primaryScheduler
     */
    @JvmStatic fun setPrimaryScheduler(@NonNull scheduler: Scheduler) {
        this.primaryScheduler = scheduler
    }

    /**
     * Returns the scheduler that should be used for primary interaction logic.
     *
     * Default value: **[Schedulers.computation]**
     *
     * @return Primary scheduler ready to be used.
     *
     * @see setPrimaryScheduler
     */
    @JvmStatic @NonNull fun primaryScheduler(): Scheduler = primaryScheduler
}