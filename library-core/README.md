Arkhitekton-Core
===============

This module contains **core** architecture elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-core:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Factory](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core/src/main/java/universum/studios/android/arkhitekton/Factory.kt)
- [InstanceBuilder](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core/src/main/java/universum/studios/android/arkhitekton/InstanceBuilder.kt)
- [Provider](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core/src/main/java/universum/studios/android/arkhitekton/Provider.kt)
- [Condition](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core/src/main/java/universum/studios/android/arkhitekton/Condition.kt)
