/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import androidx.annotation.NonNull

/**
 * Provides instances of [T].
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @param T Type of the element of which instance this provider will provide.
 */
@FunctionalInterface
interface Provider<T> {

    /**
     * Contract for [Provider] element.
     */
    companion object Contract {

        /**
         * Creates a new `Provider` that will provide the given [element].
         *
         * @param element The element instance that should the new provider provide.
         * @return New provider ready to be used.
         */
        @NonNull fun <T> simple(@NonNull element: T): Provider<T> = object : Provider<T> {

            /*
             */
            override fun get(): T = element
        }
    }

    /**
     * Provides an instance of element of the type specific for this provider.
     *
     * @return Element instance ready to be used.
     */
    @NonNull fun get(): T
}