/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.annotation.VisibleForTesting
import universum.studios.android.logging.Logger
import universum.studios.android.logging.SimpleLogger

/**
 * Utility class used by the **Arkhitekton** library for logging purpose.
 *
 * Custom [Logger] may be specified via [setLogger] which may be used to control logging outputs of
 * the library.
 *
 * Default logger used by this class has specified [Log.ASSERT] log level which means the the library
 * by default does not print out any logs.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object ArkhitektonLogging {

    /**
     * Default logger used by the library for logging purpose.
     */
    @VisibleForTesting internal val LOGGER = SimpleLogger(Log.ASSERT)

    /**
     * Logger to which is this logging utility class delegating all log related requests.
     */
    @JvmStatic @NonNull var logger: Logger = LOGGER

    /**
     * Delegates to [Logger.d].
     */
    @JvmStatic @JvmOverloads
    fun d(@NonNull tag: String, @NonNull msg: String, @Nullable error: Throwable? = null) = logger.d(tag, msg, error)

    /**
     * Delegates to [Logger.v].
     */
    @JvmStatic @JvmOverloads
    fun v(@NonNull tag: String, @NonNull msg: String, @Nullable error: Throwable? = null) = logger.v(tag, msg, error)

    /**
     * Delegates to [Logger.i].
     */
    @JvmStatic @JvmOverloads
    fun i(@NonNull tag: String, @NonNull msg: String, @Nullable error: Throwable? = null) = logger.i(tag, msg, error)

    /**
     * Delegates to [Logger.w].
     */
    @JvmStatic @JvmOverloads
    fun w(@NonNull tag: String, @NonNull msg: String = "", @Nullable error: Throwable? = null) = logger.w(tag, msg, error)

    /**
     * Delegates to [Logger.e].
     */
    @JvmStatic @JvmOverloads
    fun e(@NonNull tag: String, @NonNull msg: String = "", @Nullable error: Throwable? = null) = logger.e(tag, msg, error)

    /**
     * Delegates to [Logger.wtf].
     */
    @JvmStatic @JvmOverloads
    fun wtf(@NonNull tag: String, @NonNull msg: String = "", @Nullable error: Throwable? = null) = logger.wtf(tag, msg, error)
}