/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import androidx.annotation.NonNull

/**
 * Basic interface for builders that may be used to build new instances of desired elements.
 *
 * Builder implementations are allowed to throw an [AssertionError] when [build] is called upon them
 * and there are some required arguments missing for a successful instance build.
 *
 * @param T Type of the element of which instance this builder can build.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface InstanceBuilder<out T> {

    /**
     * Builds a new instance of element specific for this builder.
     *
     * @return New element instance ready to be used.
     * @throws AssertionError If some of the required arguments is not specified.
     */
    @NonNull fun build(): T
}