/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import androidx.annotation.NonNull

/**
 * Represents a simple condition that may be checked via [check] to determine whether it is met or not.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface Condition {

    /**
     * Contract for [Condition] element.
     */
    companion object {

        /**
         * Empty instance of condition that is met.
         */
        private val PASS: Condition = object : Condition {

            /*
             */
            override fun check(): Boolean = true
        }

        /**
         * Empty instance of condition that is not met.
         */
        private val FAIL: Condition = object : Condition {

            /*
            */
            override fun check(): Boolean = false
        }

        /**
         * Returns an empty instance of condition that **is met** and so its check passes.
         *
         * @return Empty condition ready to be checked.
         */
        @NonNull fun pass(): Condition = PASS

        /**
         * Returns an empty instance of condition that **is not met** and so its check fails.
         *
         * @return Empty condition ready to be checked.
         */
        @NonNull fun fail(): Condition = FAIL
    }

    /**
     * Checks whether this condition is met or not.
     *
     * @return `True` if this condition is met, `false` otherwise.
     */
    fun check(): Boolean
}