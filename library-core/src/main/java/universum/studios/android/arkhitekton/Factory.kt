/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import androidx.annotation.NonNull

/**
 * Simple interface for factories that may be used to create new instances of desired elements.
 *
 * @param T Type of the element of which instances this factory can create.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface Factory<out T> {

    /**
     * Creates a new instance of element type of specific for this factory.
     *
     * @return New element instance ready to be used.
     */
    @NonNull fun create(): T
}