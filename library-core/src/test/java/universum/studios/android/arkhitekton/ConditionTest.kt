/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ConditionTest : AndroidTestCase() {

    @Test fun pass() {
        // Act:
        val condition = Condition.pass()

        // Assert:
        assertThat(condition, `is`(notNullValue()))
        assertThat(condition, `is`(Condition.pass()))
    }

    @Test fun passInstance() {
        // Arrange:
        val condition = Condition.pass()

        // Act + Assert:
        assertTrue(condition.check())
    }

    @Test fun fail() {
        // Act:
        val condition = Condition.fail()

        // Assert:
        assertThat(condition, `is`(notNullValue()))
        assertThat(condition, `is`(Condition.fail()))
    }

    @Test fun failInstance() {
        // Arrange:
        val condition = Condition.fail()

        // Act + Assert:
        assertFalse(condition.check())
    }
}