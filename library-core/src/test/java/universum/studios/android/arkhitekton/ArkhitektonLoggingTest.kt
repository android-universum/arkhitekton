/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import android.util.Log
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.logging.Logger
import universum.studios.android.logging.SimpleLogger
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ArkhitektonLoggingTest : AndroidTestCase() {

    companion object {

        const val TAG = "LoggingTest"
    }

    @After
    @Throws(Exception::class)
    fun afterTest() {
        // Ensure that the logging class has default logger.
        ArkhitektonLogging.logger = ArkhitektonLogging.LOGGER
    }

    @Test fun defaultLogger() {
        // Act:
        val logger = ArkhitektonLogging.logger

        // Assert:
        assertThat(logger, `is`(notNullValue()))
        assertThat(logger.logLevel, `is`(Log.ASSERT))
    }

    @Test fun logger() {
        // Arrange:
        val logger = SimpleLogger(Log.DEBUG)

        // Act:
        ArkhitektonLogging.logger = logger

        // Assert:
        assertThat(ArkhitektonLogging.logger, `is`(logger as Logger))
    }

    @Test fun v() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        ArkhitektonLogging.logger = mockLogger

        // Act:
        ArkhitektonLogging.v(TAG, "message.verbose.1")
        ArkhitektonLogging.v(TAG, "message.verbose.2", null)

        // Assert:
        verify(mockLogger).v(TAG, "message.verbose.1", null)
        verify(mockLogger).v(TAG, "message.verbose.2", null)
        verifyNoMoreInteractions(mockLogger)
    }

    @Test fun d() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        ArkhitektonLogging.logger = mockLogger

        // Act:
        ArkhitektonLogging.d(TAG, "message.debug.1")
        ArkhitektonLogging.d(TAG, "message.debug.2", null)

        // Assert:
        verify(mockLogger).d(TAG, "message.debug.1", null)
        verify(mockLogger).d(TAG, "message.debug.2", null)
        verifyNoMoreInteractions(mockLogger)
    }

    @Test fun i() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        ArkhitektonLogging.logger = mockLogger

        // Act:
        ArkhitektonLogging.i(TAG, "message.info.1")
        ArkhitektonLogging.i(TAG, "message.info.2", null)

        // Assert:
        verify(mockLogger).i(TAG, "message.info.1", null)
        verify(mockLogger).i(TAG, "message.info.2", null)
        verifyNoMoreInteractions(mockLogger)
    }

    @Test fun w() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        ArkhitektonLogging.logger = mockLogger

        // Act:
        ArkhitektonLogging.w(TAG, "message.warn.1")
        ArkhitektonLogging.w(TAG, "message.warn.2", null)
        ArkhitektonLogging.w(tag = TAG, error = null as Throwable?)

        // Assert:
        verify(mockLogger).w(TAG, "message.warn.1", null)
        verify(mockLogger).w(TAG, "message.warn.2", null)
        verify(mockLogger).w(TAG, "", null as Throwable?)
        verifyNoMoreInteractions(mockLogger)
    }

    @Test fun e() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        ArkhitektonLogging.logger = mockLogger

        // Act:
        ArkhitektonLogging.e(TAG, "message.error.1")
        ArkhitektonLogging.e(TAG, "message.error.2", null)

        // Assert:
        verify(mockLogger).e(TAG, "message.error.1", null)
        verify(mockLogger).e(TAG, "message.error.2", null)
        verifyNoMoreInteractions(mockLogger)
    }

    @Test fun wTF() {
        // Arrange:
        val mockLogger = mock(Logger::class.java)
        ArkhitektonLogging.logger = mockLogger

        // Act:
        ArkhitektonLogging.wtf(TAG, "message.wtf.1")
        ArkhitektonLogging.wtf(TAG, "message.wtf.2", null)
        ArkhitektonLogging.wtf(tag = TAG, error = null as Throwable?)

        // Assert:
        verify(mockLogger).wtf(TAG, "message.wtf.1", null)
        verify(mockLogger).wtf(TAG, "message.wtf.2", null)
        verify(mockLogger).wtf(TAG, "", null as Throwable?)
        verifyNoMoreInteractions(mockLogger)
    }
}