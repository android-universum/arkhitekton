/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ProviderTest : AndroidTestCase() {

    @Test fun simple() {
        // Act:
        val provider = Provider.simple("test")

        // Assert:
        assertThat(provider, `is`(notNullValue()))
        assertThat(provider, `is`(not(Provider.simple("test"))))
    }

    @Test fun simpleInstance() {
        // Arrange:
        val provider = Provider.simple("test")

        // Act + Assert:
        assertThat(provider.get(), `is`("test"))
        assertThat(provider.get(), `is`("test"))
    }
}