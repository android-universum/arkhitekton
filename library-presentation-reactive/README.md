Arkhitekton-Presentation-Reactive
===============

This module contains **reactive** presentation elements and utilities.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-presentation-reactive:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-data-reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-reactive),
[arkhitekton-presentation-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core),
[arkhitekton-presentation-base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-base),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ReactivePresenter](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-reactive/src/main/java/universum/studios/android/arkhitekton/view/presentation/ReactivePresenter.kt)
