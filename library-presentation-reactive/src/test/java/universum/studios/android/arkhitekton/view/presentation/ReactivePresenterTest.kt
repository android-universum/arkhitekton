/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view.presentation

import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.arkhitekton.data.DataSchedulers
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewModel
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ReactivePresenterTest : AndroidTestCase() {

    @Test fun instantiation() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)

        // Act:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()

        // Assert:
        assertThat(presenter, `is`(notNullValue()))
        assertThat(presenter.accessScheduler(), `is`(PresentationSchedulers.primaryScheduler()))
        assertThat(presenter.accessDataScheduler(), `is`(DataSchedulers.primaryScheduler()))
        verifyNoInteractions(mockViewModel)
    }

    @Test fun instantiationWithScheduler() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)

        // Act:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
            this.scheduler = Schedulers.trampoline()
        }.build()

        // Assert:
        assertThat(presenter.accessScheduler(), `is`(Schedulers.trampoline()))
        assertThat(presenter.accessDataScheduler(), `is`(Schedulers.single()))
        verifyNoInteractions(mockViewModel)
    }

    @Test fun instantiationWithDataScheduler() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)

        // Act:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
            this.dataScheduler = Schedulers.computation()
        }.build()

        // Assert:
        assertThat(presenter.accessScheduler(), `is`(PresentationSchedulers.primaryScheduler()))
        assertThat(presenter.accessDataScheduler(), `is`(Schedulers.computation()))
        verifyNoInteractions(mockViewModel)
    }

    @Test fun hasSubscriptions() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()

        // Act + Assert:
        assertFalse(presenter.hasSubscriptions())
        verifyNoInteractions(mockViewModel)
    }

    @Test fun keepSubscription() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()
        val mockSubscriptionFirst = mock(Disposable::class.java)
        val mockSubscriptionFirstSubsequent = mock(Disposable::class.java)
        val mockSubscriptionSecond = mock(Disposable::class.java)

        // Act + Assert:

        // 1)
        assertTrue(presenter.callKeepSubscription("first", mockSubscriptionFirst))
        assertTrue(presenter.callGetSubscriptionsRegistry().contains("first"))
        assertTrue(presenter.callKeepSubscription("second", mockSubscriptionSecond))
        assertTrue(presenter.callGetSubscriptionsRegistry().contains("second"))
        assertTrue(presenter.hasSubscriptions())

        // 2)
        assertTrue(presenter.callKeepSubscription("first", mockSubscriptionFirstSubsequent))
        assertTrue(presenter.callGetSubscriptionsRegistry().contains("first"))
        verify(mockSubscriptionFirst).dispose()
        verifyNoMoreInteractions(mockSubscriptionFirst)

        verifyNoInteractions(
            mockViewModel,
            mockSubscriptionFirstSubsequent,
            mockSubscriptionSecond
        )
    }

    @Test fun disposeSubscription() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()
        val mockSubscriptionFirst = mock(Disposable::class.java)
        val mockSubscriptionSecond = mock(Disposable::class.java)
        presenter.callKeepSubscription("first", mockSubscriptionFirst)
        presenter.callKeepSubscription("second", mockSubscriptionSecond)

        // Act + Assert:

        // 1)
        assertTrue(presenter.callDisposeSubscription("first"))
        assertFalse(presenter.callGetSubscriptionsRegistry().contains("first"))
        assertTrue(presenter.callGetSubscriptionsRegistry().contains("second"))
        assertTrue(presenter.hasSubscriptions())
        verify(mockSubscriptionFirst).dispose()

        // 2)
        assertTrue(presenter.callDisposeSubscription("second"))
        assertFalse(presenter.callGetSubscriptionsRegistry().contains("first"))
        assertFalse(presenter.callGetSubscriptionsRegistry().contains("second"))
        assertFalse(presenter.hasSubscriptions())
        verify(mockSubscriptionSecond).dispose()

        // 3)
        assertFalse(presenter.callDisposeSubscription("first"))
        assertFalse(presenter.callDisposeSubscription("second"))

        verifyNoMoreInteractions(mockSubscriptionFirst, mockSubscriptionSecond)
        verifyNoInteractions(mockViewModel)
    }

    @Test fun onDestroyedWithoutRegisteredSubscriptions() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()

        // Act:
        presenter.destroy()

        // Assert:
        verifyNoInteractions(mockViewModel)
    }

    @Test fun onDestroyedWithRegisteredSubscriptions() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()
        val mockSubscription1 = mock(Disposable::class.java)
        val mockSubscription2 = mock(Disposable::class.java)
        presenter.callKeepSubscription("first", mockSubscription1)
        presenter.callKeepSubscription("second", mockSubscription2)

        // Act:
        presenter.destroy()

        // Assert:
        assertFalse(presenter.hasSubscriptions())
        verify(mockSubscription1).dispose()
        verify(mockSubscription2).dispose()
        verifyNoMoreInteractions(mockSubscription1, mockSubscription2)
        verifyNoInteractions(mockViewModel)
    }

    private interface TestView : View<ViewModel>

    private class TestPresenter(builder: TestBuilder) : ReactivePresenter<TestView, ViewModel>(builder) {

        fun accessScheduler() = scheduler

        fun accessDataScheduler() = dataScheduler

        fun callKeepSubscription(key: String, subscription: Disposable) = keepSubscription(key, subscription)

        fun callDisposeSubscription(key: String) = disposeSubscription(key)

        fun callGetSubscriptionsRegistry() = getSubscriptionsRegistry()

        class TestBuilder : ReactivePresenter.BaseBuilder<TestBuilder, TestView, ViewModel>() {

            override val self = this
            override fun build() = TestPresenter(this)
        }
    }
}