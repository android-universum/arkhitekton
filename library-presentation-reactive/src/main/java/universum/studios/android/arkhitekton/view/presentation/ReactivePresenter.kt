/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view.presentation

import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.Disposable
import universum.studios.android.arkhitekton.data.DataSchedulers
import universum.studios.android.arkhitekton.reactive.CompositeDisposableRegistry
import universum.studios.android.arkhitekton.reactive.DisposableRegistry
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewModel

/**
 * A [BasePresenter] implementation which provides a convenient api supporting **reactive approach**
 * using *Reactive Extensions* in order to implement presentation logic for a desired presenter.
 *
 * @param V  Type of the view that may be attached to the presenter a then used by the presenter for
 * example to perform navigation actions.
 * @param VM Type of the view model of which state may the presenter update according to responses
 * for interaction events.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ReactivePresenter with the given `builder's` configuration.
 * @param builder The builder with configuration for the new presenter.
 */
abstract class ReactivePresenter<V : View<*>, out VM : ViewModel>
protected constructor(builder: BaseBuilder<*, V, VM>) : BasePresenter<V, VM>(builder) {

    /*
	 * Companion ===================================================================================
	 */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        // const internal val TAG = "ReactivePresenter"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
	 * Members =====================================================================================
	 */

    /**
     * Scheduler which should be used by this presenter for presentation logic.
     */
    @NonNull protected val scheduler: Scheduler = builder.scheduler

    /**
     * Scheduler which should by used by this presenter for observation of data sources of which data
     * this presenter presents.
     */
    @NonNull protected val dataScheduler: Scheduler = builder.dataScheduler

    /**
     * Registry containing subscriptions that have been kept/registered via [keepSubscription].
     * May be disposed, each respectively, via [disposeSubscription].
     *
     * Once this presenter is [destroyed][onDestroyed] all kept/registered subscriptions are disposed.
     *
     * @since 2.2
     */
    private val subscriptions by lazy { CompositeDisposableRegistry() }

    /*
     * Initialization ==============================================================================
     */

    /*
	 * Functions ===================================================================================
	 */

    /**
     * Checks whether this presenter has some subscriptions registered or not.
     *
     * @return `True` if there are some subscriptions registered, `false` otherwise.
     *
     * @see keepSubscription
     * @see disposeSubscription
     */
    @VisibleForTesting fun hasSubscriptions(): Boolean = subscriptions.size() > 0

    /**
     * A convenient delegate function to keep the specified `subscription` in this presenter's
     * [subscriptions registry][DisposableRegistry].
     *
     * All kept/registered subscriptions that are not manually disposed before this presenter's
     * destruction will be automatically disposed when [onDestroyed] is invoked for this presenter
     * as part of its destruction process.
     *
     * @param key Key of the desired subscription to keep. Previously kept subscription with the
     * same key will be disposed before the new one is kept.
     * @param subscription The desired subscription to keep.
     * @return `True` if subscription has been successfully registered, `false` if this presenter
     * is already **destroyed**.
     *
     * @since 2.2
     *
     * @see DisposableRegistry.keep
     * @see disposeSubscription
     * @see hasSubscriptions
     */
    protected fun keepSubscription(@NonNull key: String, @NonNull subscription: Disposable): Boolean {
        return subscriptions.keep(key, subscription)
    }

    /**
     * A convenient delegate function to dispose a subscription previously kept/registered for the
     * specified `key` from this presenter's [subscriptions registry][DisposableRegistry].
     *
     * @param key Key of the desired subscription to dispose.
     * @return `True` if subscription has been successfully unregistered and disposed, `false` if
     * the desired subscription was not found in the registry or this presenter is already **destroyed**.
     *
     * @since 2.2
     *
     * @see DisposableRegistry.dispose
     * @see keepSubscription
     * @see hasSubscriptions
     */
    protected fun disposeSubscription(@NonNull key: String): Boolean {
        return subscriptions.dispose(key)
    }

    /**
     * Returns this presenter's [subscriptions registry][DisposableRegistry].
     *
     * It is safe to call any function of the registry directly (in a thread safe fashion), however
     * this presenter provides two convenient delegate functions for [keeping][keepSubscription]
     * and [disposing][disposeSubscription] of subscriptions acquired for reactive streams used in
     * context of this presenter.
     *
     * Once this presenter is **destroyed**, the registry will no longer accept any new subscriptions
     * and all subscriptions registered at the time of destruction will be disposed.
     *
     * @return Registry containing subscriptions that have been kept/registered via [keepSubscription].
     *
     * @since 2.2
     */
    @NonNull protected fun getSubscriptionsRegistry(): DisposableRegistry = subscriptions

    /*
     */
    override fun onDestroyed() {
        super.onDestroyed()

        subscriptions.dispose()
    }

    /*
	 * Inner classes ===============================================================================
	 */

    /**
     * Extended base implementation of [BasePresenter.BaseBuilder] which should be used by
     * implementations of [ReactivePresenter].
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     * @param V  Type of the view of which instance may be associated with new presenter.
     * @param VM Type of the view model of which instance may be associated with new presenter.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with default [presentation scheduler][PresentationSchedulers.primaryScheduler]
     * and default [data scheduler][DataSchedulers.primaryScheduler].
     */
    abstract class BaseBuilder<B : BaseBuilder<B, V, VM>, V : View<*>, VM : ViewModel>
    protected constructor() : BasePresenter.BaseBuilder<B, V, VM>() {

        /**
         * See [ReactivePresenter.scheduler].
         */
        var scheduler: Scheduler = PresentationSchedulers.primaryScheduler()

        /**
         * See [ReactivePresenter.dataScheduler].
         */
        var dataScheduler: Scheduler = DataSchedulers.primaryScheduler()

        /**
         * Specifies a scheduler which should be used by the new presenter for presentation logic.
         *
         * Default value: **[PresentationSchedulers.primaryScheduler]**
         *
         * @param scheduler The desired scheduler to be used.
         * @return This builder to allow methods chaining.
         */
        fun scheduler(@NonNull scheduler: Scheduler): B {
            this.scheduler = scheduler

            return self
        }

        /**
         * Specifies a scheduler which should by used by the new presenter for observation of data
         * sources of which data the presenter will present.
         *
         * Default value: **[DataSchedulers.primaryScheduler]**
         *
         * @param scheduler The desired scheduler to be used.
         * @return This builder to allow methods chaining.
         */
        fun dataScheduler(@NonNull scheduler: Scheduler): B {
            this.dataScheduler = scheduler

            return self
        }
    }
}