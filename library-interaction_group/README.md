@Arkhitekton-Interaction
===============

This module groups the following modules into one **single group**:

- [Interaction-Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core)
- [Interaction-Base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-base)
- [Interaction-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction-reactive)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-interaction:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/master/library-util),
[arkhitekton-util](https://bitbucket.org/android-universum/arkhitekton/src/master/library-util)