/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.handler

import android.app.Activity
import android.content.Intent
import androidx.annotation.Nullable

/**
 * Simple interface that may be used to implement activity result handlers.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface ActivityResultHandler {

    /**
     * Called from the activity result aware context that has received activity result described
     * by the given parameters.
     *
     * @param requestCode The request code which allows to identify the result.
     * @param resultCode  The result code. May be one of [Activity.RESULT_OK], [Activity.RESULT_FIRST_USER]
     * or [Activity.RESULT_CANCELED].
     * @param data        The intent with result data. May be `null` if no data has been delivered as result.
     * @return `True` if the dispatched activity result has been processed by this handler,
     * `false` if not and should be passed for processing to another handler or to be processed
     * by the context itself.
     */
    fun handleActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?): Boolean
}