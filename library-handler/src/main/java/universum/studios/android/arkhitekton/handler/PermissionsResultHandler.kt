/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.handler

import android.content.pm.PackageManager
import androidx.annotation.NonNull

/**
 * Simple interface that may be used to implement permissions result handlers.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface PermissionsResultHandler {

    /**
     * Called from the permissions request aware context that has received permissions result
     * described by the given parameters.
     *
     * @param requestCode The request code which allows to identify the result.
     * @param permissions The requested permissions.
     * @param grantResults The grant results for the corresponding permissions which may be either
     * [PackageManager.PERMISSION_GRANTED] or [PackageManager.PERMISSION_DENIED].
     * @return `True` if the dispatched permissions result has been processed by this handler,
     * `false` if not and should be passed for processing to another handler or to be processed
     * by the context itself.
     */
    fun handlePermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray): Boolean
}