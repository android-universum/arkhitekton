Arkhitekton-Handler
===============

This module contains **declarative interfaces** of handlers which may be used for example to implement
a handler to which an `Activity` or a `Fragment` may delegate handling of **activity result** or **permissions result**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-handler:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ActivityResultHandler](https://bitbucket.org/android-universum/arkhitekton/src/main/library-handler/src/main/java/universum/studios/android/arkhitekton/handler/ActivityResultHandler.kt)
- [PermissionsResultHandler](https://bitbucket.org/android-universum/arkhitekton/src/main/library-handler/src/main/java/universum/studios/android/arkhitekton/handler/PermissionsResultHandler.kt)
