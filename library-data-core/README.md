Arkhitekton-Data-Core
===============

This module contains **core declaration** of data related elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-data-core:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-util](https://bitbucket.org/android-universum/arkhitekton/src/main/library-util)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Entity](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-source/src/main/java/universum/studios/android/arkhitekton/data/Entity.kt)
- [ModelMapper](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-source/src/main/java/universum/studios/android/arkhitekton/data/model/ModelMapper.kt)
