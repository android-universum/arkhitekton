/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.model

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.arkhitekton.util.Errors
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ErrorMapperTest : TestCase {

    @Test fun simple() {
        // Act + Assert:
        assertThat(ErrorMapper.simple(), `is`(notNullValue()))
        assertThat(ErrorMapper.simple(), `is`(ErrorMapper.simple()))
    }

    @Test fun simpleInstance() {
        // Arrange:
        val mapper = ErrorMapper.simple()
        val errorFirst = Errors.create("First")
        val errorSecond = Errors.create("Second")

        // Act + Assert:
        assertThat(mapper.map(errorFirst), `is`(errorFirst))
        assertThat(mapper.map(errorSecond), `is`(errorSecond))
    }
}