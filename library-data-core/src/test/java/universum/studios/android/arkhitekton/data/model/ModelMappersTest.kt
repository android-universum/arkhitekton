/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.model

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ModelMappersTest : AndroidTestCase() {

    @Test fun simpleMapper() {
        // Act:
        val mapper = ModelMappers.simpleMapper<String>()

        // Assert:
        assertThat(mapper, `is`(notNullValue()))
        assertThat(mapper, `is`(ModelMappers.simpleMapper()))
    }

    @Test fun simpleResourceMapper() {
        // Act:
        val mapper = ModelMappers.simpleResourceMapper<String>()

        // Assert:
        assertThat(mapper, `is`(notNullValue()))
        assertThat(mapper, `is`(ModelMappers.simpleResourceMapper()))
    }

    @Test fun listMapper() {
        // Act:
        val mapper = ModelMappers.listMapper(TestMapper())

        // Assert:
        assertThat(mapper, `is`(notNullValue()))
        assertThat(mapper, `is`(not(ModelMappers.listMapper(TestMapper()))))
    }

    @Test fun simpleMapperInstance() {
        // Arrange:
        val mapper = ModelMappers.simpleMapper<Any>()

        // Act + Assert:
        assertThat(mapper.map("Model"), `is`("Model"))
        assertThat(mapper.map(1000), `is`(1000))
    }

    @Test fun simpleResourceMapperInstance() {
        // Arrange:
        val mapper = ModelMappers.simpleResourceMapper<Any>()

        // Act + Assert:
        assertThat(mapper.map("Model", context.resources), `is`("Model"))
        assertThat(mapper.map(1000, context.resources), `is`(1000))
    }

    @Test fun listMapperInstance() {
        // Arrange:
        val mapper = ModelMappers.listMapper(TestMapper())

        // Act:
        val mappedModels = mapper.map(listOf(1, 2, 3, 4, 5))

        // Assert:
        assertThat(mappedModels[0], `is`("1"))
        assertThat(mappedModels[1], `is`("2"))
        assertThat(mappedModels[2], `is`("3"))
        assertThat(mappedModels[3], `is`("4"))
        assertThat(mappedModels[4], `is`("5"))
    }

    class TestMapper : ModelMapper<Int, String> {

        override fun map(model: Int): String = model.toString()
    }
}