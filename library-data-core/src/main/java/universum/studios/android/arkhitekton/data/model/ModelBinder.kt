/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.model

import androidx.annotation.NonNull

/**
 * Interface that may be used to implement binders responsible for binding of data of one model
 * instance to another one. The model instances may be of a different types and structures.
 *
 * The binding logic is a full responsibility of a specific binder implementation.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface ModelBinder<FromModel, ToModel> {

    /**
     * Binds current data of the given [fromModel] instance to the given [toModel] instance.
     *
     * **Note that if the specified models are actually collections, they should have the same count
     * of model instances presented.**
     *
     * @param fromModel The desired model of which data to bind to the second model.
     * @param toModel   The desired model to which to bind data from the first model.
     * @return The second model instance with data bound from the first one.
     */
    @NonNull fun bind(@NonNull fromModel: FromModel, @NonNull toModel: ToModel): ToModel
}