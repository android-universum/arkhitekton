/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.model

import android.content.res.Resources
import androidx.annotation.NonNull

/**
 * Factory class which provides some generally useful implementations of [model mappers][ModelMapper].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@Suppress("UNCHECKED_CAST")
object ModelMappers {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Log TAG.
     */
    // const internal val TAG = "ModelMappers"

    /*
	 * Interface ===================================================================================
	 */

    /*
	 * Members =====================================================================================
	 */

    /**
     * Simple mapper implementation which maps the passed model to the same instance.
     */
    private val SIMPLE_MAPPER = object : ModelMapper<Any, Any> {

        /*
         */
        override fun map(model: Any) = model
    }

    /**
     * Simple resource mapper implementation which maps the passed model to the same instance.
     */
    private val SIMPLE_RESOURCE_MAPPER = object : ResourceModelMapper<Any, Any> {

        /*
         */
        override fun map(model: Any, resources: Resources) = model
    }

    /*
	 * Functions ===================================================================================
	 */

    /**
     * Returns a mapper implementation which maps the passed model instance to the same instance.
     *
     * @return Simple model mapper ready to be used.
     * @param T Type of the model to be mapped.
     */
    @JvmStatic @NonNull
    fun <T> simpleMapper(): ModelMapper<T, T> = SIMPLE_MAPPER as ModelMapper<T, T>

    /**
     * Returns a resource mapper implementation which maps the passed model instance to the same instance.
     *
     * @return Simple model mapper ready to be used.
     * @param T Type of the model to be mapped.
     */
    @JvmStatic @NonNull
    fun <T> simpleResourceMapper(): ResourceModelMapper<T, T> = SIMPLE_RESOURCE_MAPPER as ResourceModelMapper<T, T>

    /**
     * Returns a mapper implementation which maps each element in source collection to same count of
     * elements in destination collection.
     *
     * @param elementMapper The desired element mapper which will be used to map each element in
     * source collection.
     * @param FromModel Type of the model from which to map elements contained in source collection.
     * @param ToModel Type of the model to which to map elements in destination collection.
     * @return Collection mapper ready to be used.
     */
    @JvmStatic @NonNull
    fun <FromModel, ToModel> listMapper(
        @NonNull elementMapper: ModelMapper<FromModel, ToModel>
    ): ModelMapper<List<FromModel>, List<ToModel>> =
        CollectionMapper(elementMapper) as ModelMapper<List<FromModel>, List<ToModel>>

    /*
	 * Inner classes ===============================================================================
	 */

    /**
     * A [ModelMapper] implementation which maps source collection of elements to destination collection
     * with the same count of elements mapped using provided element mapper.
     *
     * @param FromModel Type of the model from which to map elements contained in source collection.
     * @param ToModel Type of the model to which to map elements in destination collection.
     */
    private class CollectionMapper<in FromModel, out ToModel> internal constructor(
        /**
         * The desired mapper that will be used to map each element in source collection.
         */
        private val elementMapper: ModelMapper<FromModel, ToModel>
    ) : ModelMapper<Collection<FromModel>, Collection<ToModel>> {

        /*
         */
        override fun map(model: Collection<FromModel>): Collection<ToModel> = model.map { elementMapper.map(it) }
    }
}