/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.model

import android.content.res.Resources
import androidx.annotation.NonNull

/**
 * Same as [ModelMapper] where this mapper interface may be used to implement model mappers which
 * require also [Resources] for their mapping logic.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface ResourceModelMapper<in FromModel, out ToModel> {

    /**
     * Maps the given [model] instance to the model instance of type specific for this mapper.
     *
     * **Note that if the given model is actually a collection of model instances, the transformed
     * collection should have the same count of model instances presented.**
     *
     * @param model The desired model to be mapped.
     * @param resources Application resources which may be used for mapping purpose.
     * @return New model instance type of specific for this mapper with data mapped from the given [model].
     */
    @NonNull fun map(@NonNull model: FromModel, @NonNull resources: Resources): ToModel
}