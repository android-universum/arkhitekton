/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.model

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.util.Error

/**
 * A [ModelMapper] that may be used to implement mapper which maps a specific [Error] to another [Error].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface ErrorMapper : ModelMapper<Error, Error> {

    /**
     * Contract for [ErrorMapper] element.
     */
    companion object Contract {

        /**
         * Error mapper instance which maps the given error instance to the same instance.
         */
        private val SIMPLE = object : ErrorMapper {

            /*
             */
            @NonNull override fun map(@NonNull model: Error): Error = model
        }

        /**
         * Returns an error mapper implementation which maps the passed error instance to the same instance.
         *
         * @return Simple error mapper ready to be used.
         */
        @NonNull fun simple(): ErrorMapper = SIMPLE
    }
}