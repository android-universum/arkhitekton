/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data

/**
 * Simple interface for entities which can be uniquely identified.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Entity {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [Entity] element.
     */
    companion object Contract {

        /**
         * Constant that identifies invalid/unspecified id.
         */
        const val NO_ID = -1L
    }

    /*
     * Interface ===================================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns unique id of this entity.
     *
     * @return Id of this entity or [NO_ID] if this entity cannot be identified at this time.
     */
    fun getId(): Long
}