Arkhitekton-Util
===============

This module contains **utilities** for architecture.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-util:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Value](https://bitbucket.org/android-universum/arkhitekton/src/main/library-util/src/main/java/universum/studios/android/arkhitekton/util/Value.kt)
- [Error](https://bitbucket.org/android-universum/arkhitekton/src/main/library-util/src/main/java/universum/studios/android/arkhitekton/util/Error.kt)
- [Description](https://bitbucket.org/android-universum/arkhitekton/src/main/library-util/src/main/java/universum/studios/android/arkhitekton/util/Description.kt)
