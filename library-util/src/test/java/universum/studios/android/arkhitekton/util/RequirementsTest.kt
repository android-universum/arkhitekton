/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class RequirementsTest : TestCase {

    @Test fun requireTrue() {
        // Act + Assert:
        assertTrue(Requirements.requireTrue(true))
    }

    @Test fun requireTrueInvalid() {
        // Act + Assert:
        try {
            Requirements.requireTrue(false)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required true!")
        }
    }

    @Test fun requireFalse() {
        // Act + Assert:
        assertFalse(Requirements.requireFalse(false))
    }

    @Test fun requireFalseInvalid() {
        // Act + Assert:
        try {
            Requirements.requireFalse(true)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required false!")
        }
    }

    @Test fun requireNotNullValid() {
        // Arrange:
        listOf("value", 0, listOf(0), 1.0f).forEach {
            // Act + Assert:
            assertThat(Requirements.requireNotNull(it), `is`(it))
        }
    }

    @Test fun requireNotNullInvalid() {
        // Act + Assert:
        try {
            Requirements.requireNotNull<Any>(null)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required not null!")
        }
        try {
            Requirements.requireNotNull<Any>(null, "Message.")
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Message.")
        }
    }

    @Test fun requirePositiveValid() {
        // Arrange:
        listOf(1, 2, 3, 1000L).forEach {
            // Act + Assert:
            assertThat(Requirements.requirePositive(it), `is`(it))
        }
    }

    @Test fun requirePositiveInvalid() {
        // Act + Assert:
        try {
            Requirements.requirePositive(0)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required positive!")
        }
        try {
            Requirements.requirePositive(0, "Message.")
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Message.")
        }
    }

    @Test fun requireNonPositiveValid() {
        // Arrange:
        listOf(0, -1, -2, -3, -1000L).forEach {
            // Act + Assert:
            assertThat(Requirements.requireNonPositive(it), `is`(it))
        }
    }

    @Test fun requireNonPositiveInvalid() {
        // Act + Assert:
        try {
            Requirements.requireNonPositive(1)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required non positive!")
        }
        try {
            Requirements.requireNonPositive(1, "Message.")
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Message.")
        }
    }

    @Test fun requireNegativeValid() {
        // Arrange:
        listOf(-1, -2, -3, -1000L).forEach {
            // Act + Assert:
            assertThat(Requirements.requireNegative(it), `is`(it))
        }
    }

    @Test fun requireNegativeInvalid() {
        // Act + Assert:
        try {
            Requirements.requireNegative(0)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required negative!")
        }
        try {
            Requirements.requireNegative(0, "Message.")
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Message.")
        }
    }

    @Test fun requireNonNegativeValid() {
        // Arrange:
        listOf(0, 1, 2, 3, 1000L).forEach {
            // Act + Assert:
            assertThat(Requirements.requireNonNegative(it), `is`(it))
        }
    }

    @Test fun requireNonNegativeInvalid() {
        // Act + Assert:
        try {
            Requirements.requireNonNegative(-1)
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Required non negative!")
        }
        try {
            Requirements.requireNonNegative(-1, "Message.")
        } catch (e: Throwable) {
            assertRequirementErrorWithMessage(e, "Message.")
        }
    }

    private fun assertRequirementErrorWithMessage(throwable: Throwable, message: String) {
        assertTrue(throwable is RequirementError)
        assertThat(throwable.message, `is`(message))
    }
}