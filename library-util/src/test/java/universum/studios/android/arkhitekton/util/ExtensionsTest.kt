/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ExtensionsTest : TestCase {

    @Test fun hasFlag() {
        // Act + Assert:
        assertTrue(0x00000000.hasFlag(0x00000000))
        assertFalse(0x00000000.hasFlag(0x00000001))
        assertTrue(0x00000001.hasFlag(0x00000001))
        assertTrue((0x00000001 or 0x00000002).hasFlag(0x00000001))
        assertTrue((0x00000001 or 0x00000002).hasFlag(0x00000002))
        assertFalse((0x00000001 or 0x00000002).hasFlag(0x00000004))
    }

    @Test fun changeFlag() {
        // Act + Assert:
        
        // - addition:
        assertThat(0x00000000.changeFlag(0x0000000, true), `is`(0x00000000))
        assertThat(0x00000000.changeFlag(0x0000001, true), `is`(0x00000001))
        assertThat(0x00000000.changeFlag(0x0000002, true), `is`(0x00000002))
        
        // - removal:
        assertThat(0x00000000.changeFlag(0x0000000, false), `is`(0x00000000))
        assertThat(0x00000003.changeFlag(0x0000002, false), `is`(0x00000001))
        assertThat(0x00000001.changeFlag(0x0000001, false), `is`(0x00000000))
    }

    @Test fun addFlag() {
        // Act + Assert:
        assertThat(0x00000000.addFlag(0x00000000), `is`(0x00000000))
        assertThat(0x00000000.addFlag(0x00000001), `is`(0x00000001))
        assertThat(0x00000001.addFlag(0x00000001), `is`(0x00000001))
        assertThat(0x00000001.addFlag(0x00000002), `is`(0x00000003))
        assertThat(0x00000002.addFlag(0x00000004), `is`(0x00000006))
    }

    @Test fun removeFlag() {
        // Act + Assert:
        assertThat(0x00000000.removeFlag(0x00000000), `is`(0x00000000))
        assertThat(0x00000001.removeFlag(0x00000000), `is`(0x00000001))
        assertThat(0x00000001.removeFlag(0x00000001), `is`(0x00000000))
        assertThat(0x00000002.removeFlag(0x00000001), `is`(0x00000002))
        assertThat((0x00000003).removeFlag(0x00000001), `is`(0x00000002))
        assertThat((0x00000003).removeFlag(0x00000002), `is`(0x00000001))
    }
}