/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ErrorCauseTest : TestCase {

    @Test fun instantiation() {
        // Arrange:
        val error = Error.UNKNOWN

        // Act:
        val cause = ErrorCause(error)

        // Assert:
        assertThat(cause.error, `is`(error))
        assertThat(cause.code(), `is`(error.code()))
        assertThat(cause.description(), `is`(error.description()))
        assertThat(cause.cause(), `is`(error.cause()))
    }

    @Test fun equalsImplementation() {
        // Arrange + Act + Assert:
        assertEquals(ErrorCause(Error.UNKNOWN), ErrorCause(Error.UNKNOWN))
        assertEquals(ErrorCause(Errors.create("FATAL")), ErrorCause(Errors.create("FATAL")))
        assertNotEquals(ErrorCause(Error.NONE), ErrorCause(Error.UNKNOWN))
        assertNotEquals(
            ErrorCause(Errors.create("UNKNOWN", Description.builder().title("Unknown").build())),
            ErrorCause(Errors.create("FATAL", Description.builder().title("Fatal").build()))
        )
    }

    @Test fun hashCodeImplementation() {
        // Arrange + Act + Assert:
        assertThat(
            ErrorCause(Errors.create("UNKNOWN", Description.builder().title("Unknown").build())).hashCode(),
            `is`("UNKNOWN".hashCode())
        )
        assertThat(
            ErrorCause(Errors.create("FATAL", Description.builder().title("Fatal").build())).hashCode(),
            `is`("FATAL".hashCode())
        )
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val error = Errors.create("FATAL")
        val cause = ErrorCause(error)

        // Act + Assert:
        assertThat(cause.toString(), `is`("ErrorCause(error: $error)"))
    }
}