/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import java.io.Serializable
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class DescriptionTest : TestCase {

    @Test fun emptyInstance() {
        // Act + Assert:
        assertThat(Description.EMPTY.getTitle(), `is`(""))
        assertThat(Description.EMPTY.getSummary(), `is`(""))
        assertThat(Description.EMPTY.getDetails(), `is`(emptyMap()))
        assertThat(Description.EMPTY.toString(), `is`("EMPTY"))
    }

    @Test fun instantiation() {
        // Arrange:
        val details = mapOf<String, Serializable>(
            Pair("key:1", "string"),
            Pair("key:2", 2000)
        )

        // Act:
        val description = Description.Builder().apply {
            this.title = "Fatal error"
            this.summary = "Fatal error has occurred due to crashed Universe."
            this.details = details
        }.build()

        // Assert:
        assertThat(description.getTitle(), `is`("Fatal error"))
        assertThat(description.getSummary(), `is`("Fatal error has occurred due to crashed Universe."))
        assertThat(description.getDetails(), `is`(details))
    }

    @Test fun instantiationJavaStyle() {
        // Arrange:
        val details = mapOf<String, Serializable>(
            Pair("key:1", "string"),
            Pair("key:2", 2000)
        )

        // Act:
        val description = Description.Builder()
            .title("Fatal error")
            .summary("Fatal error has occurred due to crashed Universe.")
            .details(details)
            .build()

        // Assert:
        assertThat(description.getTitle(), `is`("Fatal error"))
        assertThat(description.getSummary(), `is`("Fatal error has occurred due to crashed Universe."))
        assertThat(description.getDetails(), `is`(details))
    }

    @Test fun instantiationWithoutAttributes() {
        // Act:
        val description = Description.Builder().build()

        // Assert:
        assertThat(description.getTitle(), `is`(""))
        assertThat(description.getSummary(), `is`(""))
        assertThat(description.getDetails(), `is`(emptyMap()))
    }

    @Test fun details() {
        // Arrange:
        val details = mapOf<String, Serializable>(
            Pair("key:1", "string"),
            Pair("key:2", 2000)
        )
        val description = Description.Builder().apply {
            this.title = "Fatal error"
            this.summary = "Fatal error has occurred due to crashed Universe."
            this.details = details
        }.build()

        // Act + Assert:
        assertThat(description.requireDetail("key:1"), `is`(details["key:1"]))
        assertThat(description.getDetail("key:1"), `is`(details["key:1"]))
        assertThat(description.requireDetail("key:2"), `is`(details["key:2"]))
        assertThat(description.getDetail("key:2"), `is`(details["key:2"]))
        assertThat(description.getDetail("key:3"), `is`(nullValue()))
    }

    @Test(expected = IllegalStateException::class)
    fun detailsWhenNoneSpecified() {
        // Arrange:
        val description = Description.Builder().apply {
            this.title = "Fatal error"
            this.summary = "Fatal error has occurred due to crashed Universe."
        }.build()

        // Act:
        description.requireDetail<String>("key:1")
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val details = mapOf<String, Serializable>(
            Pair("key:1", "string"),
            Pair("key:2", 2000)
        )
        val description = Description.Builder().apply {
            this.title = "Fatal error"
            this.summary = "Fatal error has occurred due to crashed Universe."
            this.details = details
        }.build()

        // Act + Assert:
        assertThat(
            description.toString(),
            `is`("Description(title: Fatal error, summary: Fatal error has occurred due to crashed Universe., details: $details)")
        )
    }
}