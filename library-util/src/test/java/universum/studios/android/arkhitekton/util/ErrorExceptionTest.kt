/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ErrorExceptionTest : TestCase {

    @Test fun noneInstance() {
        // Act + Assert:
        assertThat(ErrorException.NONE, `is`(notNullValue()))
        assertThat(ErrorException.NONE.code(), `is`(Error.NONE.code()))
        assertThat(ErrorException.NONE.description(), `is`(Error.NONE.description()))
        assertThat(ErrorException.NONE.cause(), `is`(Error.NONE.cause()))
    }

    @Test fun unknownInstance() {
        // Act + Assert:
        assertThat(ErrorException.UNKNOWN, `is`(notNullValue()))
        assertThat(ErrorException.UNKNOWN.code(), `is`(Error.UNKNOWN.code()))
        assertThat(ErrorException.UNKNOWN.description(), `is`(Error.UNKNOWN.description()))
        assertThat(ErrorException.UNKNOWN.cause(), `is`(Error.UNKNOWN.cause()))
    }

    @Test fun createWithCodeOnly() {
        // Act:
        val error = ErrorException.create("ERROR")

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(Description.EMPTY))
        assertThat(error.cause(), `is`(nullValue()))
    }

    @Test fun createWithCodeAndDescription() {
        // Arrange:
        val description = Description.Builder().apply {
            this.title = "Error"
            this.summary = "Some unspecified error."
        }.build()

        // Act:
        val error = ErrorException.create("ERROR", description)

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(description))
        assertThat(error.cause(), `is`(nullValue()))
    }

    @Test fun createWithCodeAndDescriptionAndCause() {
        // Arrange:
        val description = Description.Builder().apply {
            this.title = "Error"
            this.summary = "Some unspecified error."
        }.build()
        val cause = IllegalStateException()

        // Act:
        val error = ErrorException.create("ERROR", description, cause)

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(description))
        assertThat(error.cause(), `is`(cause))
    }

    @Test fun createWithCodeAndCause() {
        // Arrange:
        val cause = IllegalStateException()

        // Act:
        val error = ErrorException.create("ERROR", cause)

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(Description.EMPTY))
        assertThat(error.cause(), `is`(cause))
    }

    @Test fun equalsImplementation() {
        // Arrange + Act + Assert:
        assertEquals(ErrorException.UNKNOWN, ErrorException.UNKNOWN)
        assertEquals(ErrorException.create("FATAL"), ErrorException.create("FATAL"))
        assertNotEquals(ErrorException.NONE, ErrorException.UNKNOWN)
        assertNotEquals(
            ErrorException.create("UNKNOWN", Description.builder().title("Unknown").build()),
            ErrorException.create("FATAL", Description.builder().title("Fatal").build())
        )
    }

    @Test fun hashCodeImplementation() {
        // Arrange + Act + Assert:
        assertThat(
            ErrorException.create("UNKNOWN", Description.builder().title("Unknown").build()).hashCode(),
            `is`("UNKNOWN".hashCode())
        )
        assertThat(
            ErrorException.create("FATAL", Description.builder().title("Fatal").build()).hashCode(),
            `is`("FATAL".hashCode())
        )
    }
}