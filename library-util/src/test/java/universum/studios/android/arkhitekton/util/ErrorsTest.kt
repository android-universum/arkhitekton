/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ErrorsTest : TestCase {

    @Test fun createWithCodeOnly() {
        // Act:
        val error = Errors.create(code = "ERROR")

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(Description.EMPTY))
        assertThat(error.cause(), `is`(nullValue()))
    }

    @Test fun createWithCodeAndDescription() {
        // Arrange:
        val description = Description.Builder().apply {
            this.title = "Error"
            this.summary = "Some unspecified error."
        }.build()

        // Act:
        val error = Errors.create(code = "ERROR", description)

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(description))
        assertThat(error.cause(), `is`(nullValue()))
    }

    @Test fun createWithCodeAndCause() {
        // Arrange:
        val cause = IllegalStateException()

        // Act:
        val error = Errors.create(code = "ERROR", cause = cause)

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(Description.EMPTY))
        assertThat(error.cause(), `is`(cause))
    }

    @Test fun createWithCodeAndDescriptionAndCause() {
        // Arrange:
        val description = Description.Builder().apply {
            this.title = "Error"
            this.summary = "Some unspecified error."
        }.build()
        val cause = IllegalStateException()

        // Act:
        val error = Errors.create("ERROR", description, cause)

        // Assert:
        assertThat(error.code(), `is`("ERROR"))
        assertThat(error.description(), `is`(description))
        assertThat(error.cause(), `is`(cause))
    }

    @Test fun unwrapNotAnErrorWithCause() {
        // Arrange:
        val cause = IllegalStateException()
        val throwable = RuntimeException(cause)

        // Act:
        val error = Errors.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(not(Error.unknown())))
        assertTrue(error.hasCode(Error.unknown().code()))
        assertThat(error.cause(), `is`(throwable))
    }

    @Test fun unwrapNotAnErrorWithoutCause() {
        // Arrange:
        val throwable = RuntimeException()

        // Act:
        val error = Errors.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(not(Error.unknown())))
        assertTrue(error.hasCode(Error.unknown().code()))
        assertThat(error.cause(), `is`(throwable))
    }

    @Test fun unwrapFromErrorCause() {
        // Arrange:
        val causeError = Errors.create("ERROR")
        val throwable = ErrorCause(causeError)

        // Act:
        val error = Errors.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(causeError))
    }

    @Test fun unwrapError() {
        // Arrange:
        val throwable = ErrorException.create("ERROR")

        // Act:
        val error = throwable.unwrapError()

        // Assert:
        assertThat(error, `is`(throwable))
        assertTrue(error.hasCode("ERROR"))
    }
}