/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ErrorTest : TestCase {

    @Test fun none() {
        // Act + Assert:
        assertThat(Error.none(), `is`(notNullValue()))
        assertThat(Error.none(), `is`(Error.none()))
    }

    @Test fun noneInstance() {
        // Arrange:
        val error = Error.none()

        // Assert:
        assertTrue(error.hasCode("NONE"))
        assertThat(error.code(), `is`("NONE"))
        assertThat(error.description(), `is`(Description.EMPTY))
        assertThat(error.cause(), `is`(nullValue()))
        assertThat(error.toString(), `is`("NONE"))
    }

    @Test fun unknown() {
        // Act + Assert:
        assertThat(Error.unknown(), `is`(notNullValue()))
        assertThat(Error.unknown(), `is`(Error.unknown()))
    }

    @Test fun unknownInstance() {
        // Arrange:
        val error = Error.unknown()

        // Assert:
        assertTrue(error.hasCode("UNKNOWN"))
        assertThat(error.code(), `is`("UNKNOWN"))
        assertThat(error.description(), `is`(Description.EMPTY))
        assertThat(error.cause(), `is`(nullValue()))
        assertThat(error.toString(), `is`("UNKNOWN"))
    }

    @Test fun asError() {
        // Arrange:
        val throwable = ErrorException.create("ERROR")

        // Act:
        val error = Error.asError(throwable)

        // Assert:
        assertThat(error, `is`(throwable))
        assertTrue(error.hasCode("ERROR"))
    }

    @Test(expected = AssertionError::class)
    fun asErrorNotAnError() {
        // Arrange:
        val throwable = IllegalStateException()

        // Act:
        Error.asError(throwable)
    }

    @Test fun unwrap() {
        // Arrange:
        val throwable = ErrorException.create("ERROR")

        // Act:
        val error = Error.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(throwable))
        assertTrue(error.hasCode("ERROR"))
    }

    @Test fun unwrapCause() {
        // Arrange:
        val cause = ErrorException.create("ERROR")
        val throwable = IllegalStateException(cause)

        // Act:
        val error = Error.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(cause))
        assertTrue(error.hasCode("ERROR"))
    }

    @Test fun unwrapCauseWithinCause() {
        // Arrange:
        val causePrimary = ErrorException.create("ERROR")
        val causeSecondary = IllegalArgumentException(causePrimary)
        val throwable = IllegalStateException(causeSecondary)

        // Act:
        val error = Error.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(causePrimary))
        assertTrue(error.hasCode("ERROR"))
    }

    @Test fun unwrapCauseNotAnError() {
        // Arrange:
        val cause = IllegalStateException()
        val throwable = RuntimeException(cause)

        // Act:
        val error = Error.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(Error.unknown()))
    }

    @Test fun unwrapWithoutCause() {
        // Arrange:
        val throwable = RuntimeException()

        // Act:
        val error = Error.unwrap(throwable)

        // Assert:
        assertThat(error, `is`(Error.unknown()))
    }

    @Test fun isError() {
        // Arrange + Act + Assert:
        assertFalse(RuntimeException().isError())
        assertFalse(IllegalStateException().isError())
        assertTrue(ErrorException.create("ERROR").isError())
    }
}