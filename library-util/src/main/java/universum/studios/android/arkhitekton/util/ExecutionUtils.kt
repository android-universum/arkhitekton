/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

/**
 * Utility class used by architecture elements to control execution flow.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object ExecutionUtils {

    /**
     * Sleeps current execution on the actual thread for the specified duration.
     *
     * @param durationInMillis The desired duration in milliseconds to sleep the actual thread for.
     */
    @JvmStatic fun sleepFor(durationInMillis: Long) {
        try { Thread.sleep(durationInMillis) }
        catch (e: InterruptedException) { e.printStackTrace() }
    }
}