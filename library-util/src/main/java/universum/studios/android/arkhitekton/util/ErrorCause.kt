/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import androidx.annotation.NonNull

/**
 * [RuntimeException] implementation which may carry single [Error]. This cause acts like the carried
 * error by mirroring all its properties ([code], [description], [cause]).
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ErrorCause with the specified [error].
 * @param error The desired error to be propagated via the new error cause.
 */
class ErrorCause(@NonNull val error: Error) : RuntimeException(), Error {

    /*
     */
    override fun code(): String = error.code()

    /*
     */
    override fun description(): Description = error.description()

    /*
     */
    override fun cause(): Throwable? = error.cause()

    /*
     */
    override fun equals(other: Any?): Boolean {
        return when {
            other === this -> true
            other !is Error -> false
            else -> other.code() == error.code()
        }
    }

    /*
     */
    override fun hashCode(): Int = this.error.code().hashCode()

    /*
     */
    override fun toString(): String = "${javaClass.simpleName}(error: $error)"
}