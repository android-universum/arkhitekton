/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.util.Error.Contract.unknown

/**
 * A convenience factory class which may be used to create [Error] instances.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object Errors {

    /**
     * Creates a new Error with the specified [code], [description] and [cause].
     *
     * @param code The desired code for the new error.
     * @param description The desired description for the new error. Default is [EMPTY][Description.EMPTY].
     * @param cause The cause due to which the new error occurred. Default is `null`.
     * @return Error ready to be propagated further.
     */
    @JvmStatic fun create(
        @NonNull code: String,
        @NonNull description: Description = Description.EMPTY,
        @NonNull cause: Throwable? = null
    ): Error = ErrorException.create(code, description, cause)

    /**
     * Convenience method for casting a throwable into [Error].
     *
     * It is safe to call this method only if the specified [throwable] is actually an Error.
     *
     * @param throwable The desired throwable to cast into error.
     * @return The given throwable as Error or an exception is thrown.
     * @throws AssertionError If the given throwable is not of [Error] type.
     *
     * @see unwrap
     */
    @NonNull fun asError(@NonNull throwable: Throwable): Error {
        return throwable as? Error ?: throw AssertionError("Specified throwable($throwable) is not an Error!")
    }

    /**
     * Unwraps an error from the specified [throwable]. If the throwable is actually an [Error],
     * it will be returned directly as Error, if it is [ErrorCause], its `error` property will be
     * returned, otherwise an **unknown** error with the given `throwable` as **cause** will be returned.
     *
     * @param throwable The desired throwable from which to unwrap the desired error.
     * @return Found and unwrapped Error or an [unknown] error if the specified throwable is not
     * an Error or ErrorCause.
     *
     * @see asError
     */
    @NonNull fun unwrap(@NonNull throwable: Throwable): Error {
        return when (throwable) {
            is ErrorCause -> throwable.error
            is Error -> throwable
            else -> create(
                code = Error.UNKNOWN.code(),
                cause = throwable
            )
        }
    }
}

// EXTENSIONS ======================================================================================

/**
 * Convenience extension for [Error.unwrap] function.
 *
 * @return Found and unwrapped Error or an [Error.unknown] error if this throwable is not
 * an Error neither its causes (recursively) are not an Error.
 */
fun Throwable.unwrapError(): Error = Errors.unwrap(this)
