/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import universum.studios.android.arkhitekton.InstanceBuilder
import java.io.Serializable

/**
 * Simple class which may be used to describe desired element using *title* and *summary* texts.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of Description with the given `builder's` configuration.
 * @param builder The builder with configuration for the new description.
 */
class Description internal constructor(@NonNull builder: Builder) : Cloneable {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [Description] element.
     */
    companion object Contract {

        /**
         * Empty instance of [Description] without any descriptive properties.
         */
        @NonNull @JvmField
        val EMPTY: Description = Builder().build()

        /**
         * Returns a new instance of description [Builder].
         *
         * @return Builder ready to be used to build instance of a desired description.
         */
        @JvmStatic @NonNull
        fun builder(): Builder = Builder()
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Title text specified for this description.
     */
    private val title = builder.title

    /**
     * Summary text specified for this description.
     */
    private val summary = builder.summary

    /**
     * Map of details specified for this description mapped to their respective keys.
     */
    private val details = builder.details

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the title text specified for this description.
     *
     * Default value: **`""`**
     *
     * @return This description's title.
     */
    @NonNull fun getTitle(): String = title

    /**
     * Returns the summary text specified for this description.
     *
     * Default value: **`""`**
     *
     * @return This description's summary.
     */
    @NonNull fun getSummary(): String = summary

    /**
     * Returns the map of details specified for this description mapped to their respective keys.
     *
     * Default value: **[emptyMap]**
     *
     * @return This description's details.
     *
     * @see getDetail
     * @see requireDetail
     */
    @NonNull fun getDetails(): Map<String, Serializable> = details

    /**
     * Returns a single detail for the specified [key] casted to the requested type.
     *
     * @param key Key of the desired detail to obtain.
     * @param T Type of the requested detail.
     * @return The requested detail or exception is thrown.
     *
     * @throws IllegalStateException If there is no such detail present.
     *
     * @see getDetail
     * @see getDetails
     */
    @NonNull fun <T : Serializable> requireDetail(@NonNull key: String): T =
        checkNotNull(getDetail(key), { "No detail found for key: $key!" })

    /**
     * Returns a single detail for the specified [key] casted to the requested type.
     *
     * @param key Key of the desired detail to obtain.
     * @param T Type of the requested detail.
     * @return The requested detail or `null` if there is no such detail present.
     *
     * @see requireDetail
     * @see getDetails
     */
    @Suppress("UNCHECKED_CAST")
    @Nullable fun <T : Serializable> getDetail(@NonNull key: String): T? = details[key] as T?

    /*
     */
    @NonNull public override fun clone() = super.clone() as Description

    /*
     */
    @NonNull override fun toString(): String {
        return when(this) {
            EMPTY -> "EMPTY"
            else -> "Description(title: $title, summary: $summary, details: $details)"
        }
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * A builder which may be used to build instances of [Description].
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    class Builder internal constructor() : InstanceBuilder<Description> {

        /**
         * See [Description.getTitle].
         */
        var title: String = ""

        /**
         * See [Description.getSummary].
         */
        var summary: String = ""

        /**
         * See [Description.getDetails].
         */
        var details: Map<String, Serializable> = emptyMap()

        /**
         * Specifies a [title][Description.getTitle] text for description.
         *
         * @param title The desired title text for new description.
         * @return This builder to allow methods chaining.
         * @see summary
         */
        fun title(@NonNull title: String) = apply { this.title = title }

        /**
         * Specifies a [summary][Description.getSummary] text for description.
         *
         * @param summary The desired summary text for new description.
         * @return This builder to allow methods chaining.
         * @see title
         */
        fun summary(@NonNull summary: String) = apply { this.summary = summary }

        /**
         * Specifies a map of [details][Description.getDetails] for description.
         *
         * @param details The desired details for new description.
         * @return This builder to allow methods chaining.
         */
        fun details(@NonNull details: Map<String, Serializable>) = apply { this.details = details }

        /*
         */
        @NonNull override fun build() = Description(this)
    }
}