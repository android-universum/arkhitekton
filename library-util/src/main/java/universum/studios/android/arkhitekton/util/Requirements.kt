/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import androidx.annotation.NonNull
import androidx.annotation.Nullable

/**
 * Utility with group of methods which may be used to ensure specific requirements for argument
 * values, states and also return values.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object Requirements {

    /**
     * Checks whether the given [value] is **true**.
     *
     * **Requirement:**
     *
     * *value == true*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws AssertionError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun requireTrue(
        @Nullable value: Boolean,
        @NonNull message: String = "Required true!"
    ): Boolean = if (value) value else throw RequirementError(message)

    /**
     * Checks whether the given [value] is **false**.
     *
     * **Requirement:**
     *
     * *value == false*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws RequirementError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun requireFalse(
        @Nullable value: Boolean,
        @NonNull message: String = "Required false!"
    ): Boolean = if (!value) value else throw RequirementError(message)

    /**
     * Checks whether the given [value] is **not null**.
     *
     * **Requirement:**
     *
     * *value != null*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @param T The type of value to be checked.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws RequirementError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun <T> requireNotNull(
        @Nullable value: T?,
        @NonNull message: String = "Required not null!"
    ): T = value ?: throw RequirementError(message)

    /**
     * Checks whether the given [value] is **positive**.
     *
     * **Requirement:**
     *
     * *value > 0*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws RequirementError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun requirePositive(
        value: Long,
        @NonNull message: String = "Required positive!"
    ): Long = if (value > 0) value else throw RequirementError(message)

    /**
     * Checks whether the given [value] is **non positive**.
     *
     * **Requirement:**
     *
     * *value <= 0*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws RequirementError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun requireNonPositive(
        value: Long,
        @NonNull message: String = "Required non positive!"
    ): Long = if (value <= 0) value else throw RequirementError(message)

    /**
     * Checks whether the given [value] is **negative**.
     *
     * **Requirement:**
     *
     * *value < 0*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws RequirementError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun requireNegative(
        value: Long,
        @NonNull message: String = "Required negative!"
    ): Long = if (value < 0) value else throw RequirementError(message)

    /**
     * Checks whether the given [value] is **non negative**.
     *
     * **Requirement:**
     *
     * *value >= 0*
     *
     * @param value The desired value to be checked.
     * @param message The desired message for error to be thrown in case of not met requirement.
     * @return The given value if it meets this requirement, otherwise an error is thrown.
     * @throws RequirementError When the checked value does not meet this requirement.
     */
    @JvmStatic @JvmOverloads
    fun requireNonNegative(
        value: Long,
        @NonNull message: String = "Required non negative!"
    ): Long = if (value >= 0) value else throw RequirementError(message)
}