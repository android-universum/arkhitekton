/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import androidx.annotation.NonNull

/**
 * Basic error which may be identified by its [code][code] and described via [description][description]
 * along with its [cause][cause].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@Suppress("MemberVisibilityCanBePrivate")
interface Error {

    /**
     * Contract for [Error] element.
     */
    companion object Contract {

        /**
         * Instance which may be used to indicate a 'none' error.
         */
        @NonNull internal val NONE = object : Error {

            /*
             */
            override fun code(): String = "NONE"

            /*
             */
            override fun description(): Description = Description.EMPTY

            /*
             */
            override fun cause(): Throwable? = null

            /*
             */
            override fun toString(): String = code()
        }

        /**
         * Instance which may be used to indicate an 'unknown' error.
         */
        @NonNull internal val UNKNOWN = object : Error {

            /*
             */
            override fun code(): String = "UNKNOWN"

            /*
             */
            override fun description(): Description = Description.EMPTY

            /*
             */
            override fun cause(): Throwable? = null

            /*
             */
            override fun toString(): String = code()
        }

        /**
         * Returns an instance of Error with 'NONE' code and [EMPTY][Description.EMPTY] description
         * and `null` cause.
         *
         * May be used as empty object to indicate that no error has occurred.
         *
         * @return Error ready to be associated with a desired failure.
         */
        @NonNull fun none(): Error = NONE

        /**
         * Returns an instance of Error with 'UNKNOWN' code and [EMPTY][Description.EMPTY] description
         * and `null` cause.
         *
         * May be used to indicate that an unknown error has occurred.
         *
         * @return Error ready to be associated with a desired failure.
         */
        @NonNull fun unknown(): Error = UNKNOWN

        /**
         * **This function has been deprecated and will be removed in the next minor release.**
         *
         * Convenience method for casting a throwable into [Error].
         *
         * It is safe to call this method only if the specified [throwable] is actually an Error.
         *
         * @param throwable The desired throwable to cast into error.
         * @return The given throwable as Error or an exception is thrown.
         * @throws AssertionError If the given throwable is not of [Error] type.
         *
         * @see unwrap
         */
        @Deprecated(
            "Use Errors.asError(Throwable) instead.",
            ReplaceWith("Errors.asError(throwable)")
        )
        @NonNull fun asError(@NonNull throwable: Throwable): Error {
            return throwable as? Error ?: throw AssertionError("Specified throwable($throwable) is not an Error!")
        }

        /**
         * **This function has been deprecated and will be removed in the next minor release.**
         *
         * Unwraps an error from the specified [throwable]. If the throwable is actually an Error
         * it will be returned directly as Error otherwise its causes will be iterated recursively.
         *
         * @param throwable The desired throwable from which to unwrap the desired error.
         * @return Found and unwrapped Error or an [unknown] error if the specified throwable is not
         * an Error neither its causes (recursively) are not an Error.
         *
         * @see asError
         */
        @Deprecated(
            "Use Errors.unwrap(Throwable) instead.",
            ReplaceWith("Errors.unwrap(throwable)")
        )
        @NonNull fun unwrap(@NonNull throwable: Throwable): Error {
            if (throwable is Error) {
                return throwable
            }

            if (throwable.cause == null) {
                return UNKNOWN
            }

            return unwrap(throwable.cause!!)
        }
    }

    /**
     * Checks whether this error has the specified `code`.
     *
     * @param code The desired code to check.
     * @return `True` if this error has the code, `false` otherwise.
     */
    fun hasCode(@NonNull code: String): Boolean = this.code() == code

    /**
     * Returns the code of this error.
     *
     * @return This error's code.
     * @see description
     * @see cause
     */
    @NonNull fun code(): String

    /**
     * Returns the description specified for this error.
     *
     * @return This error's description.
     */
    @NonNull fun description(): Description

    /**
     * Returns the cause associated with this error.
     *
     * @return This error's cause or `null` if this error does not have any cause.
     */
    @NonNull fun cause(): Throwable?
}

// EXTENSIONS ======================================================================================

/**
 * Checks whether this throwable instance is actually an [Error].
 *
 * @return `True` if this throwable represents also [Error].
 */
fun Throwable.isError(): Boolean = this is Error
