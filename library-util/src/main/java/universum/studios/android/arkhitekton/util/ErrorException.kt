/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.util

import androidx.annotation.NonNull

/**
 * A [RuntimeException] which can be thrown or dispatched as [Error].
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ErrorException with the given [code], [description] and [cause].
 * @param code The desired code for the new error.
 * @param description The desired description for the new error.
 * @param cause The desired cause for the new error.
 */
class ErrorException private constructor(
    /**
     * Code identifying this error.
     */
    @NonNull private val code: String,
    /**
     * Description describing this error.
     */
    @NonNull private val description: Description,
    /**
     * Cause describing why this error occurred.
     */
    @NonNull cause: Throwable? = null
) : RuntimeException(cause), Error {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [ErrorException] element.
     */
    @Suppress("RemoveRedundantQualifierName")
    companion object Contract {

        /**
         * Instance of ErrorException with attributes of [NONE][Error.none] error.
         */
        @JvmField val NONE: ErrorException = ErrorException(
            Error.NONE.code(),
            Error.NONE.description(),
            Error.NONE.cause()
        )

        /**
         * Instance of ErrorException with attributes of [UNKNOWN][Error.none] error.
         */
        @JvmField val UNKNOWN: ErrorException = ErrorException(
            Error.UNKNOWN.code(),
            Error.UNKNOWN.description(),
            Error.UNKNOWN.cause()
        )

        /**
         * Creates a new Error with the specified [code] and [description].
         *
         * @param code The desired code for the new error.
         * @param description The desired description for the new error. Default is [EMPTY][Description.EMPTY].
         * @param cause The desired cause describing why the new error occurred. Default is `null`.
         * @return Error exception ready to be thrown or propagated further.
         */
        @JvmStatic @JvmOverloads @NonNull fun create(
            @NonNull code: String,
            @NonNull description: Description = Description.EMPTY,
            @NonNull cause: Throwable? = null
        ) = ErrorException(code, description, cause)

        /**
         * Creates a new Error with the specified [code] and [description].
         *
         * @param code The desired code for the new error.
         * @param cause The desired cause describing why the new error occurred.
         * @return Error exception ready to be thrown or propagated further.
         */
        @JvmStatic @NonNull fun create(
            @NonNull code: String,
            @NonNull cause: Throwable? = null
        ) = ErrorException(code, Description.EMPTY, cause)
    }

    /*
     * Functions ===================================================================================
     */

    /*
     */
    @NonNull override fun code(): String = this.code

    /*
     */
    @NonNull override fun description(): Description = this.description

    /*
     */
    @NonNull override fun cause(): Throwable? = this.cause

    /*
     */
    override fun equals(other: Any?): Boolean {
        return when {
            other === this -> true
            other !is ErrorException -> false
            else -> other.code() == this.code
        }
    }

    /*
     */
    override fun hashCode(): Int = this.code.hashCode()

    /*
     */
    @NonNull override fun toString(): String = "${javaClass.simpleName}(code: $code, description: $description, cause: $cause)"

    /*
	 * Interface ===================================================================================
	 */
}