/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.0
 */
@file:Suppress("NOTHING_TO_INLINE")
package universum.studios.android.arkhitekton.util

/**
 * Performs **logical AND operation** to check whether this Int value has the specified `flag`.
 *
 * @return `True` if the flag is contained in this value, `false` otherwise.
 */
inline fun Int.hasFlag(flag: Int): Boolean = (this and flag) == flag

/**
 * Changes presence of the specified [flag] in this flags value.
 *
 * @param flag The desired flag to be added or removed from this flags value.
 * @param add `True` to add the flag via [addFlag], otherwise to remove it via [removeFlag].
 *
 * @return New Int value with or without the flag provided.
 */
inline fun Int.changeFlag(flag: Int, add: Boolean): Int = if (add) addFlag(flag) else removeFlag(flag)

/**
 * Performs **logical OR operation** to combine this Int value with the specified `flag`.
 *
 * @return New Int value with the flag provided.
 */
inline fun Int.addFlag(flag: Int): Int = this or flag

/**
 * Performs **logical AND operation** with inverted value of the specified `flag` to remove the flag
 * from this Int value.
 *
 * @return New Int value without the flag provided.
 */
inline fun Int.removeFlag(flag: Int): Int = this and flag.inv()