/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import universum.studios.android.arkhitekton.view.presentation.Presenter

/**
 * Empty [Controller] implementation (NULL object) which doesn't perform any *controlling* logic.
 *
 * @author Martin Albedinsky
 * @since 2.2
 */
internal object EmptyController : Controller<Presenter<*>> {

    /*
     */
    override fun activate() {}

    /*
     */
    override fun isActive(): Boolean = false

    /*
     */
    override fun deactivate() {}

    /*
     */
    override fun setEnabled(enabled: Boolean) {}

    /*
     */
    override fun isEnabled(): Boolean = false

    /*
     */
    override fun getPresenter(): Presenter<*> = Presenter.empty()
}