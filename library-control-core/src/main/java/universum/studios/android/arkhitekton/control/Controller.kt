/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import androidx.annotation.CallSuper
import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.InstanceBuilder
import universum.studios.android.arkhitekton.control.Controller.Holder
import universum.studios.android.arkhitekton.interaction.Interactor
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewModel
import universum.studios.android.arkhitekton.view.presentation.Presenter

/**
 * Specifies an interface for architectural elements which should act as mediators between
 * [Views][View] and [Interactors][Interactor], that is, they should handle interaction events
 * produced by views, package them in appropriate requests, pass them to interaction layer to be
 * processed further and acknowledge presentation layer about responses to those requests or about
 * any appropriate interaction events, so the presentation layer may also accordingly modify state
 * of the view in order it to render graphics that correctly reflects the whole interaction process.
 *
 * In a simple way, controller handles events dispatched by view, processes them via interactor and
 * presents their results via presenter.
 *
 * Each controller instance should be activated via [activate] before it may be used and deactivated
 * via [deactivate] if it becomes no longer needed. It is recommended that controller instances lives
 * as long as its associated view. To preserve any controller's state across view's destruction and
 * recreation phases, a controller [Holder] may be used.
 *
 * If a specific controller is required to not handle interaction events for some desired time, it
 * may be temporarily disabled via [setEnabled] and later enabled. This may be useful in cases when
 * controller is allowed to handle only one specific interaction at a time and other interactions
 * may be handled after that particular one is finished.
 *
 * It is recommended that each controller implementation provides its specific builder that may be
 * used to build instances of that controller type and that such builders implement [Controller.Builder]
 * for convenience.
 *
 * **Note that it is also recommended to not directly implement this interface as its signature may
 * change over time, but rather to inherit from its [BaseController] implementation.**
 *
 * @param P Type of the presenter to which will this controller dispatch interaction events and/or responses.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Controller<out P : Presenter<*>> {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [Controller] element.
     */
    companion object Contract {

        /**
         * Returns an empty instance of [Controller] which may be used in cases where an instance of
         * concrete Controller implementation is not needed.
         *
         * The returned controller provides [EMPTY][Presenter.empty] presenter.
         *
         * **Note that the returned controller instance cannot be neither activated nor enabled,
         * that is, it will always remain deactivated and disabled.**
         *
         * @return Empty controller ready to be used.
         */
        @NonNull fun empty(): Controller<*> = EmptyController
    }

    /*
     * Interface ===================================================================================
     */

    /**
     * Interface for builders which may be used to build instances of [Controller] implementations.
     *
     * @param T Type of the controller of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : Controller<*>> : InstanceBuilder<T>

    /**
     * A [ViewModel] implementation used to hold instance of [Controller] in order to preserve
     * its ongoing interaction handling along with its associated state, for example across orientation
     * changes when the corresponding controller is used in context of lifecycle aware context.
     *
     * Once cleared via [onCleared], the attached controller is automatically detached via [Holder.detachController].
     *
     * **Remember that view model instances are created by the Android architecture components library
     * via reflection, so theirs classes need to be PUBLIC and also to have at least one PUBLIC constructor.**
     *
     * @param T Type of the controller of which instance the holder can hold.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    abstract class Holder<T : Controller<*>> : androidx.lifecycle.ViewModel() {

        /**
         * The controller instance attached to this holder. Should be valid between calls to
         * [attachController] and [detachController].
         */
        private var controller: T? = null

        /**
         * Attaches the given [controller] to this holder.
         *
         * @param controller The controller instance to be hold by this holder.
         * @see getController
         * @see detachController
         */
        fun attachController(@NonNull controller: T): T {
            check(this.controller == null) { "Cannot attach controller. Previous controller is still attached!" }

            controller.activate()
            this.controller = controller

            return controller
        }

        /**
         * Checks whether this holder has its corresponding controller instance attached or not.
         *
         * @return `True` if controller is attached, `false` otherwise.
         * @see getController
         */
        fun hasController(): Boolean = controller != null

        /**
         * Returns the controller attached to this holder.
         *
         * @return Controller instance attached or an exception is thrown.
         * @throws AssertionError If this holder has no controller attached.
         * @see hasController
         */
        @NonNull fun getController(): T = checkNotNull(controller, { "No controller attached!" })

        /*
         */
        @CallSuper override fun onCleared() {
            super.onCleared()

            detachController()
        }

        /**
         * Detaches the controller instance previously attached to this holder. This will also
         * deactivate the attached controller via [Controller.deactivate].
         *
         * If there is no controller attached, this method does nothing.
         *
         * @see attachController
         */
        fun detachController() {
            controller?.deactivate()
            controller = null
        }
    }

    /*
     * Functions ===================================================================================
     */

    /**
     * Activates this controller instance, if it is not active yet.
     *
     * Controller is considered active, and thus should be activated, when it is attached to a component
     * that produces interaction events, like view. Only active controller should accept interaction
     * events and pass them as appropriate requests to the associated interactor.
     *
     * Once activated, controller should be deactivated via [deactivate] when it is detached from
     * the interaction component due to fact that such component is about do be destroyed (permanently)
     * and there will be no more interaction events to be handled by this controller.
     *
     * @see isActive
     */
    fun activate()

    /**
     * Checks whether this controller instance is inactive. This is a convenience function which negates
     * result of [isActive] function.
     *
     * @return `True` if this controller is inactive, `false` otherwise.
     */
    fun isInactive(): Boolean = !isActive()

    /**
     * Checks whether this controller instance is active.
     *
     * Controller should be always active between calls to [activate] and [deactivate].
     *
     * @return `True` if this controller is active, `false` otherwise.
     * @see activate
     * @see deactivate
     */
    fun isActive(): Boolean

    /**
     * Deactivates this controller instance, if it is active.
     *
     * Once deactivated, controller is considered to be destroyed and cannot be activated again, that
     * is, it should not be used any further.
     *
     * Deactivated controller is allowed to throw an exception if some caller is trying to request
     * handling of a specific interaction event through such controller.
     *
     * @see isActive
     * @see activate
     */
    fun deactivate()

    /**
     * Enables/disables this controller instance.
     *
     * When controller is enabled, it should handle all interaction events and appropriately respond
     * to them, either directly or via some presentation layer. Disabled controller should ignore
     * all interaction events for the time it is disabled until it is enabled again.
     *
     * If controller is disabled and there are still pending some events to be handled or responded
     * to, those events should be processed and their result dispatched appropriately.
     *
     * @param enabled `True` to enable this controller, `false` to disable it.
     * @see isEnabled
     */
    fun setEnabled(enabled: Boolean)

    /**
     * Checks whether this controller instance is disabled. This is a convenience function which
     * negates result of [isEnabled] function.
     *
     * @return `True` if this controller is disabled, `false` otherwise.
     */
    fun isDisabled(): Boolean = !isEnabled()

    /**
     * Checks whether this controller instance is enabled or not.
     *
     * By default each controller should be initially **enabled**.
     *
     * @return `True` if this controller is enabled, `false` otherwise.
     * @see setEnabled
     */
    fun isEnabled(): Boolean

    /**
     * Returns the presenter to which is this controller dispatching responses produced by the
     * this controller's associated interactor or any appropriate interaction events.
     *
     * @return This controller's associated presenter.
     */
    @NonNull fun getPresenter(): P
}