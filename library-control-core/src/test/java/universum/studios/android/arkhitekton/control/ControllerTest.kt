/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.arkhitekton.view.presentation.Presenter
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ControllerTest : TestCase {

    @Test fun empty() {
        // Assert:
        assertThat(Controller.empty(), `is`(notNullValue()))
        assertThat(Controller.empty(), `is`(Controller.empty()))
    }

    @Test fun isInactive() {
        // Assert:
        assertFalse(TestController(active = true).isInactive())
        assertTrue(TestController(active = false).isInactive())
    }

    @Test fun isDisabled() {
        // Assert:
        assertFalse(TestController(enabled = true).isDisabled())
        assertTrue(TestController(enabled = false).isDisabled())
    }

    @Test fun holder() {
        // Assert:
        assertFalse(TestHolder().hasController())
    }

    @Test fun holderAttachController() {
        // Arrange:
        val holder = TestHolder()
        val controller = Controller.empty()

        // Act + Assert:
        assertThat(holder.attachController(controller), `is`(controller))
        assertTrue(holder.hasController())
        assertThat(holder.getController(), `is`(controller))
    }

    @Test(expected = IllegalStateException::class)
    fun holderAttachControllerWhenAlreadyAttached() {
        // Arrange:
        val holder = TestHolder()
        val controller = Controller.empty()
        holder.attachController(controller)

        // Act:
        holder.attachController(controller)
    }

    @Test(expected = IllegalStateException::class)
    fun holderGetControllerWhenNotAttached() {
        // Act:
        TestHolder().getController()
    }

    @Test fun holderCleared() {
        // Arrange:
        val holder = TestHolder()
        holder.attachController(Controller.empty())

        // Act:
        holder.onCleared()

        // Assert:
        assertFalse(holder.hasController())
    }

    @Test fun holderDetachController() {
        // Arrange:
        val holder = TestHolder()
        holder.attachController(Controller.empty())

        // Act:
        holder.detachController()

        // Assert:
        assertFalse(holder.hasController())
    }

    @Test fun holderDetachControllerWhenNotAttached() {
        // Only ensure that detaching of not attached controller does not cause any troubles.
        // Arrange:
        val holder = TestHolder()

        // Act:
        holder.detachController()
    }

    private class TestController(
        private val active: Boolean = false,
        private val enabled: Boolean = false,
    ) : Controller<Presenter<*>> {

        override fun activate() = Unit

        override fun isActive(): Boolean = active

        override fun deactivate() = Unit

        override fun setEnabled(enabled: Boolean) = Unit

        override fun isEnabled(): Boolean = enabled

        override fun getPresenter() = TODO("TEST")
    }

    class TestHolder : Controller.Holder<Controller<*>>() {

        public override fun onCleared() = super.onCleared()
    }
}