/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Test
import universum.studios.android.arkhitekton.view.presentation.Presenter
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class EmptyControllerTest : TestCase {

    @Test fun instance() {
        // Arrange:
        val controller = EmptyController

        // Act + Assert:
        assertFalse(controller.isActive())
        assertFalse(controller.isEnabled())
        assertThat(controller.getPresenter(), `is`<Presenter<*>>(Presenter.empty()))

        controller.activate()
        assertFalse(controller.isActive())

        controller.deactivate()
        assertFalse(controller.isActive())

        controller.setEnabled(true)
        assertFalse(controller.isEnabled())

        controller.setEnabled(false)
        assertFalse(controller.isEnabled())
    }
}