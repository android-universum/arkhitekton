Arkhitekton-Control-Core
===============

This module contains **core declaration** of `Controller` element which connects `Interactor` and `Presenter` together.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-control-core:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core),
[arkhitekton-presentation-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Controller](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-core/src/main/java/universum/studios/android/arkhitekton/control/Controller.kt)
