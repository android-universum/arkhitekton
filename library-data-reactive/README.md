Arkhitekton-Data-Reactive
===============

This module contains **reactive** data elements and utilities.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-data-reactive:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DataSchedulers](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-reactive/src/main/java/universum/studios/android/arkhitekton/data/DataSchedulers.kt)
