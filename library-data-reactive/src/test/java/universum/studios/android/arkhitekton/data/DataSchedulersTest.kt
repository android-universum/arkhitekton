/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data

import io.reactivex.rxjava3.schedulers.Schedulers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class DataSchedulersTest : TestCase {

    @After fun afterTest() {
        // Ensure default scheduler.
        DataSchedulers.setPrimaryScheduler(Schedulers.single())
    }

    @Test fun primaryScheduler() {
        // Act:
        val scheduler = DataSchedulers.primaryScheduler()

        // Assert:
        assertThat(scheduler, `is`(notNullValue()))
        assertThat(scheduler, `is`(Schedulers.single()))
        assertThat(scheduler, `is`(DataSchedulers.primaryScheduler()))
    }

    @Test fun setPrimaryScheduler() {
        // Arrange:
        val scheduler = Schedulers.trampoline()

        // Act:
        DataSchedulers.setPrimaryScheduler(scheduler)

        // Assert:
        assertThat(DataSchedulers.primaryScheduler(), `is`(scheduler))
    }
}