Arkhitekton-Control-Reactive
===============

This module contains **reactive implementation** of `Controller` element which may be used as base
for controllers that will be used along with **[Reactive Streams](https://www.reactive-streams.org/)**,
and contains also other **reactive** elements and utilities.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-control-reactive:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-data-reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-reactive),
[arkhitekton-control-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-core),
[arkhitekton-control-base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-base),
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core),
[arkhitekton-interaction-reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-reactive),
[arkhitekton-presentation-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core),
[arkhitekton-presentation-reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-reactive),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ReactiveController](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-reactive/src/main/java/universum/studios/android/arkhitekton/control/ReactiveController.kt)
