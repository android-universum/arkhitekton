/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.arkhitekton.data.DataSchedulers
import universum.studios.android.arkhitekton.interaction.InteractionSchedulers
import universum.studios.android.arkhitekton.interaction.Interactor
import universum.studios.android.arkhitekton.view.presentation.PresentationSchedulers
import universum.studios.android.arkhitekton.view.presentation.Presenter
import universum.studios.android.testing.AndroidTestCase

/**
 * @author Martin Albedinsky
 */
class ReactiveControllerTest : AndroidTestCase() {

    @Test fun instantiation() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)

        // Act:
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Assert:
        assertThat(controller, `is`(notNullValue()))
        assertThat(controller.accessDataScheduler(), `is`(DataSchedulers.primaryScheduler()))
        assertThat(controller.accessInteractionScheduler(), `is`(InteractionSchedulers.primaryScheduler()))
        assertThat(controller.accessPresentationScheduler(), `is`(PresentationSchedulers.primaryScheduler()))
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun instantiationWithSchedulers() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)

        // Act:
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
            this.dataScheduler = Schedulers.computation()
            this.interactionScheduler = Schedulers.trampoline()
            this.presentationScheduler = Schedulers.single()
        }.build()

        // Assert:
        assertThat(controller.accessDataScheduler(), `is`(Schedulers.computation()))
        assertThat(controller.accessInteractionScheduler(), `is`(Schedulers.trampoline()))
        assertThat(controller.accessPresentationScheduler(), `is`(Schedulers.single()))
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun hasSubscriptions() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Act + Assert:
        assertFalse(controller.hasSubscriptions())
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun keepSubscription() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        val mockSubscriptionFirst = mock(Disposable::class.java)
        val mockSubscriptionFirstSubsequent = mock(Disposable::class.java)
        val mockSubscriptionSecond = mock(Disposable::class.java)

        // Act + Assert:

        // 1)
        assertTrue(controller.callKeepSubscription("first", mockSubscriptionFirst))
        assertTrue(controller.callGetSubscriptionsRegistry().contains("first"))
        assertTrue(controller.callKeepSubscription("second", mockSubscriptionSecond))
        assertTrue(controller.callGetSubscriptionsRegistry().contains("second"))
        assertTrue(controller.hasSubscriptions())

        // 2)
        assertTrue(controller.callKeepSubscription("first", mockSubscriptionFirstSubsequent))
        assertTrue(controller.callGetSubscriptionsRegistry().contains("first"))
        verify(mockSubscriptionFirst).dispose()
        verifyNoMoreInteractions(mockSubscriptionFirst)

        verifyNoInteractions(
            mockInteractor,
            mockPresenter,
            mockSubscriptionFirstSubsequent,
            mockSubscriptionSecond
        )
    }

    @Test fun disposeSubscription() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        val mockSubscriptionFirst = mock(Disposable::class.java)
        val mockSubscriptionSecond = mock(Disposable::class.java)
        controller.callKeepSubscription("first", mockSubscriptionFirst)
        controller.callKeepSubscription("second", mockSubscriptionSecond)

        // Act + Assert:

        // 1)
        assertTrue(controller.callDisposeSubscription("first"))
        assertFalse(controller.callGetSubscriptionsRegistry().contains("first"))
        assertTrue(controller.callGetSubscriptionsRegistry().contains("second"))
        assertTrue(controller.hasSubscriptions())
        verify(mockSubscriptionFirst).dispose()

        // 2)
        assertTrue(controller.callDisposeSubscription("second"))
        assertFalse(controller.callGetSubscriptionsRegistry().contains("first"))
        assertFalse(controller.callGetSubscriptionsRegistry().contains("second"))
        assertFalse(controller.hasSubscriptions())
        verify(mockSubscriptionSecond).dispose()

        // 3)
        assertFalse(controller.callDisposeSubscription("first"))
        assertFalse(controller.callDisposeSubscription("second"))

        verifyNoMoreInteractions(mockSubscriptionFirst, mockSubscriptionSecond)
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun deactivateWithoutRegisteredSubscriptions() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        controller.activate()

        // Act:
        controller.deactivate()

        // Assert:
        verifyNoInteractions(mockInteractor)
    }

    @Test fun deactivateWithRegisteredSubscriptions() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        controller.activate()

        // Act + Assert:

        // 1)
        val mockSubscriptionFirst = mock(Disposable::class.java)
        val mockSubscriptionSecond = mock(Disposable::class.java)
        controller.callKeepSubscription("first", mockSubscriptionFirst)
        controller.callKeepSubscription("second", mockSubscriptionSecond)

        controller.deactivate()

        assertFalse(controller.hasSubscriptions())
        verify(mockSubscriptionFirst).dispose()
        verify(mockSubscriptionSecond).dispose()
        verifyNoMoreInteractions(mockSubscriptionFirst, mockSubscriptionSecond)

        // 2)
        val mockSubscriptionFirstSubsequent = mock(Disposable::class.java)
        val mockSubscriptionSecondSubsequent = mock(Disposable::class.java)
        assertFalse(controller.callKeepSubscription("first", mockSubscriptionFirstSubsequent))
        assertFalse(controller.callKeepSubscription("second", mockSubscriptionSecondSubsequent))
        verify(mockSubscriptionFirstSubsequent).dispose()
        verify(mockSubscriptionSecondSubsequent).dispose()
        verifyNoMoreInteractions(mockSubscriptionFirstSubsequent, mockSubscriptionSecondSubsequent)

        verifyNoInteractions(mockInteractor)
    }

    private class TestController(builder: TestBuilder) : ReactiveController<Interactor, Presenter<*>>(builder) {

        fun accessDataScheduler() = dataScheduler

        fun accessInteractionScheduler() = interactionScheduler

        fun accessPresentationScheduler() = presentationScheduler

        fun callKeepSubscription(key: String, subscription: Disposable) = keepSubscription(key, subscription)

        fun callDisposeSubscription(key: String) = disposeSubscription(key)

        fun callGetSubscriptionsRegistry() = getSubscriptionsRegistry()

        class TestBuilder : ReactiveController.BaseBuilder<TestBuilder, Interactor, Presenter<*>>() {

            override val self = this
            override fun build() = TestController(this)
        }
    }
}