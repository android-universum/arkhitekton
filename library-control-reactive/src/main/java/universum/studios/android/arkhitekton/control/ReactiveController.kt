/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import androidx.annotation.CallSuper
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.Disposable
import universum.studios.android.arkhitekton.data.DataSchedulers
import universum.studios.android.arkhitekton.interaction.InteractionSchedulers
import universum.studios.android.arkhitekton.interaction.Interactor
import universum.studios.android.arkhitekton.reactive.CompositeDisposableRegistry
import universum.studios.android.arkhitekton.reactive.DisposableRegistry
import universum.studios.android.arkhitekton.view.presentation.PresentationSchedulers
import universum.studios.android.arkhitekton.view.presentation.Presenter

/**
 * A [BaseController] implementation which provides a convenient api supporting **reactive approach**
 * using *Reactive Extensions* in order to implement controlling logic for a desired controller.
 *
 * @param I Type of the interactor to which may the controller pass interaction requests to be processed.
 * @param P Type of the presenter to which may the controller dispatch appropriate responses for
 * processed requests and/or other interaction events.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ReactiveController with the given `builder's` configuration.
 * @param builder The builder with configuration for the new controller.
 */
abstract class ReactiveController<out I : Interactor, out P : Presenter<*>>
protected constructor(builder: BaseBuilder<*, I, P>) : BaseController<I, P>(builder) {

    /*
	 * Companion ===================================================================================
	 */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        // internal const val TAG = "ReactiveController"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Scheduler which should be used by this controller for loading of data for its associated presenter.
     */
    @NonNull protected val dataScheduler: Scheduler = builder.dataScheduler

    /**
     * Scheduler which should be used by this controller for processing of interaction requests.
     */
    @NonNull protected val interactionScheduler: Scheduler = builder.interactionScheduler

    /**
     * Scheduler which should by used by this controller for presentation of received responses as
     * results of interaction requests.
     */
    @NonNull protected val presentationScheduler: Scheduler = builder.presentationScheduler

    /**
     * Registry containing subscriptions that have been kept/registered via [keepSubscription].
     * May be disposed, each respectively, via [disposeSubscription].
     *
     * Once this controller is [deactivated][onDeactivated] all kept/registered subscriptions are disposed.
     *
     * @since 2.2
     */
    private val subscriptions by lazy { CompositeDisposableRegistry() }

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Checks whether this controller has some subscriptions kept/registered or not.
     *
     * @return `True` if there are some subscriptions registered, `false` otherwise.
     */
    @VisibleForTesting fun hasSubscriptions(): Boolean = !subscriptions.isEmpty()

    /**
     * A convenient delegate function to keep the specified `subscription` in this controller's
     * [subscriptions registry][DisposableRegistry].
     *
     * All kept/registered subscriptions that are not manually disposed before this controller's
     * deactivation will be automatically disposed when [onDeactivated] is invoked for this controller
     * as part of its deactivation process.
     *
     * @param key Key of the desired subscription to keep. Previously kept subscription with the
     * same key will be disposed before the new one is kept.
     * @param subscription The desired subscription to keep.
     * @return `True` if subscription has been successfully registered, `false` if this controller
     * is already **deactivated**.
     *
     * @since 2.2
     *
     * @see DisposableRegistry.keep
     * @see disposeSubscription
     * @see hasSubscriptions
     */
    protected fun keepSubscription(@NonNull key: String, @NonNull subscription: Disposable): Boolean {
        return subscriptions.keep(key, subscription)
    }

    /**
     * A convenient delegate function to dispose a subscription previously kept/registered for the
     * specified `key` from this controller's [subscriptions registry][DisposableRegistry].
     *
     * @param key Key of the desired subscription to dispose.
     * @return `True` if subscription has been successfully unregistered and disposed, `false` if
     * the desired subscription was not found in the registry or this controller is already **deactivated**.
     *
     * @since 2.2
     *
     * @see DisposableRegistry.dispose
     * @see keepSubscription
     * @see hasSubscriptions
     */
    protected fun disposeSubscription(@NonNull key: String): Boolean {
        return subscriptions.dispose(key)
    }

    /**
     * Returns this controller's [subscriptions registry][DisposableRegistry].
     *
     * It is safe to call any function of the registry directly (in a thread safe fashion), however
     * this controller provides two convenient delegate functions for [keeping][keepSubscription]
     * and [disposing][disposeSubscription] of subscriptions acquired for reactive streams used in
     * context of this controller.
     *
     * Once this controller is **deactivated**, the registry will no longer accept any new subscriptions
     * and all subscriptions registered at the time of deactivation will be disposed.
     *
     * @return Registry containing subscriptions that have been kept/registered via [keepSubscription].
     *
     * @since 2.2
     */
    @NonNull protected fun getSubscriptionsRegistry(): DisposableRegistry = subscriptions

    /*
     */
    @CallSuper override fun onDeactivated() {
        super.onDeactivated()

        subscriptions.dispose()
    }

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Extended base implementation of [BaseController.BaseBuilder] which should be used by
     * implementations of [ReactiveController].
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     * @param I Type of the interactor of which instance may be associated with new controller.
     * @param P Type of the presenter of which instance may be associated with new controller.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with default [interaction scheduler][InteractionSchedulers.primaryScheduler]
     * and default [presentation scheduler][PresentationSchedulers.primaryScheduler].
     */
    abstract class BaseBuilder<B : BaseBuilder<B, I, P>, I : Interactor, P : Presenter<*>>
    protected constructor() : BaseController.BaseBuilder<B, I, P>() {

        /**
         * See [ReactiveController.dataScheduler].
         */
        var dataScheduler: Scheduler = DataSchedulers.primaryScheduler()

        /**
         * See [ReactiveController.interactionScheduler].
         */
        var interactionScheduler: Scheduler = InteractionSchedulers.primaryScheduler()

        /**
         * See [ReactiveController.presentationScheduler].
         */
        var presentationScheduler: Scheduler = PresentationSchedulers.primaryScheduler()

        /**
         * Specifies a scheduler which should be used by the new controller for loading of data
         * for its associated presenter.
         *
         * Default value: **[DataSchedulers.primaryScheduler]**
         *
         * @param scheduler The desired scheduler to be used.
         * @return This builder to allow methods chaining.
         */
        fun dataScheduler(@NonNull scheduler: Scheduler): B {
            this.dataScheduler = scheduler

            return self
        }

        /**
         * Specifies a scheduler which should be used by the new controller for processing of
         * interaction requests.
         *
         * Default value: **[InteractionSchedulers.primaryScheduler]**
         *
         * @param scheduler The desired scheduler to be used.
         * @return This builder to allow methods chaining.
         */
        fun interactionScheduler(@NonNull scheduler: Scheduler): B {
            this.interactionScheduler = scheduler

            return self
        }

        /**
         * Specifies a scheduler which should by used by the new controller for presentation of
         * received responses as results of interaction requests.
         *
         * Default value: **[PresentationSchedulers.primaryScheduler]**
         *
         * @param scheduler The desired scheduler to be used.
         * @return This builder to allow methods chaining.
         */
        fun presentationScheduler(@NonNull scheduler: Scheduler): B {
            this.presentationScheduler = scheduler

            return self
        }
    }
}