/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ResponsesTest : TestCase {

    @Test fun createEmptySuccess() {
        // Act:
        val response = Responses.createEmptySuccess()
        
        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response, `is`(not(Responses.createEmptySuccess())))
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`<Value>(Value.EMPTY))
        assertFalse(response.isFailure())
        assertThat(response.getError(), `is`(Error.none()))
    }

    @Test fun createEmptySuccessWithRequestSpecified() {
        // Arrange:
        val request = TestRequest()
        
        // Act:
        val response = Responses.createEmptySuccess(request)
        
        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response, `is`(not(Responses.createEmptySuccess())))
        assertThat(response.getRequest(), `is`(request))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`<Value>(Value.EMPTY))
        assertFalse(response.isFailure())
        assertThat(response.getError(), `is`(Error.none()))
    }

    @Test fun createUnknownFailure() {
        // Act:
        val response = Responses.createUnknownFailure<Value.Empty>()

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response, `is`(not(Responses.createUnknownFailure())))
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertFalse(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(Error.unknown()))
    }

    @Test fun createUnknownFailureWithRequestSpecified() {
        // Arrange:
        val request = TestRequest()

        // Act:
        val response = Responses.createUnknownFailure<Value.Empty>(request)

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response, `is`(not(Responses.createUnknownFailure())))
        assertThat(response.getRequest(), `is`(request))
        assertFalse(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(Error.unknown()))
    }

    @Test fun createSuccess() {
        // Arrange:
        val request = mock(Request::class.java)
        val resultValue = mock(Value::class.java)

        // Act:
        val response = Responses.createSuccess(request, resultValue)

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(request))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`(resultValue))
        assertFalse(response.isFailure())
        assertThat(response.getError(), `is`(Error.none()))
    }

    @Test fun createFailure() {
        // Arrange:
        val request = mock(Request::class.java)
        val error = mock(Error::class.java)

        // Act:
        val response = Responses.createFailure<Value.Empty>(request, error)

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(request))
        assertFalse(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(error))
    }

    private class TestRequest : Request
}