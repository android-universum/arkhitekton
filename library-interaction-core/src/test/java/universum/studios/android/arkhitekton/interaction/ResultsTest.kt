/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.ErrorException
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ResultsTest : TestCase {

    @Test fun createSuccess() {
        // Act:
        val result = Results.createSuccess(10)
        
        // Assert:
        assertThat(result, `is`(notNullValue()))
        assertTrue(result.isSuccess())
        assertThat(result.getValue(), `is`(10))
        assertFalse(result.isFailure())
        assertThat(result.getError(), `is`(Error.none()))
    }

    @Test fun createFailure() {
        // Act:
        val result = Results.createFailure<Int>(Error.unknown())
        
        // Assert:
        assertThat(result, `is`(notNullValue()))
        assertTrue(result.isFailure())
        assertThat(result.getError(), `is`(Error.unknown()))
        assertFalse(result.isSuccess())
    }

    @Test fun createFailureWithThrowableError() {
        // Arrange:
        val throwable: Throwable = ErrorException.create("fatal")
        
        // Act:
        val result = Results.createFailure<Int>(throwable)
        
        // Assert:
        assertThat(result, `is`(notNullValue()))
        assertTrue(result.isFailure())
        assertThat(result.getError(), `is`(throwable as Error))
        assertFalse(result.isSuccess())
    }
}