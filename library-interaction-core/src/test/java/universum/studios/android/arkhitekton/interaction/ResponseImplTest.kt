/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verifyNoInteractions
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ResponseImplTest : TestCase {

    @Test fun instantiationOfSuccess() {
        // Arrange:
        val mockRequest = mock(Request::class.java)
        val resultValue = Value.EMPTY

        // Act:
        val response = ResponseImpl(mockRequest, resultValue, Error.none())

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(mockRequest))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`(resultValue))
        assertFalse(response.isFailure())
        assertThat(response.getError(), `is`(Error.none()))
        assertThat(response.toString(), `is`("Response(request: ..., isSuccess: true, value: EMPTY, isFailure: false, error: NONE)"))
        verifyNoInteractions(mockRequest)
    }

    @Test fun instantiationOfFailure() {
        // Arrange:
        val mockRequest = mock(Request::class.java)
        val error = Error.unknown()

        // Act:
        val response = ResponseImpl(mockRequest, Value.EMPTY, error)

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(mockRequest))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(Error.unknown()))
        assertFalse(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
        assertThat(response.toString(), `is`("Response(request: ..., isSuccess: false, value: EMPTY, isFailure: true, error: UNKNOWN)"))
        verifyNoInteractions(mockRequest)
    }
}