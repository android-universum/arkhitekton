/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ParamRequestTest : TestCase {

    @Test fun instantiation() {
        // Act:
        val request = ParamRequest("test")

        // Assert:
        assertThat(request.param, `is`("test"))
    }

    @Test fun copy() {
        // Arrange + Act + Assert:
        assertThat(ParamRequest("test").copy().param, `is`("test"))
    }

    @Test fun equalsImplementation() {
        // Arrange + Act + Assert:
        assertTrue(ParamRequest("test").equals(ParamRequest("test")))
        assertTrue(ParamRequest(100).equals(ParamRequest(100)))
        assertFalse(ParamRequest("test").equals(ParamRequest("TEST")))
    }

    @Test fun hashCodeImplementation() {
        // Arrange + Act + Assert:
        assertThat(ParamRequest("test").hashCode(), `is`("test".hashCode()))
        assertThat(ParamRequest(100).hashCode(), `is`(100.hashCode()))
    }

    @Test fun toStringImplementation() {
        // Arrange + Act = Assert:
        assertThat(ParamRequest("test").toString(), `is`("ParamRequest(param=test)"))
        assertThat(ParamRequest(100).toString(), `is`("ParamRequest(param=100)"))
    }
}