/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ResultTest : TestCase {

    @Test fun success() {
        // Act:
        val result = Result.success()

        // Assert:
        assertThat(result, `is`(notNullValue()))
        assertThat(result, `is`(Result.success()))
        assertThat(result, `is`(not(Result.failure())))
    }

    @Test fun successInstance() {
        // Arrange:
        val result = Result.success()

        // Assert:
        assertTrue(result.isSuccess())
        assertThat(result.getValue(), `is`<Any>(Value.EMPTY))
        assertFalse(result.isFailure())
        assertThat(result.getError(), `is`(Error.none()))
    }

    @Test fun failure() {
        // Act:
        val result = Result.failure()

        // Assert:
        assertThat(result, `is`(notNullValue()))
        assertThat(result, `is`(Result.failure()))
        assertThat(result, `is`(not(Result.success())))
    }

    @Test fun failureInstance() {
        // Arrange:
        val result = Result.failure()

        // Assert:
        assertTrue(result.isFailure())
        assertThat(result.getError(), `is`(Error.unknown()))
        assertFalse(result.isSuccess())
        assertThat(result.getValue(), `is`<Any>(Value.EMPTY))
    }
}