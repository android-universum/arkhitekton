/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class ResponseTest : TestCase {

    @Test fun success() {
        // Act:
        val response = Response.success()
        
        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response, `is`(Response.success()))
        assertThat(response, `is`(not(Response.failure())))
    }

    @Test fun successInstance() {
        // Arrange:
        val response = Response.success()
        
        // Assert:
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`<Any>(Value.EMPTY))
        assertFalse(response.isFailure())
        assertThat(response.getError(), `is`(Error.none()))
    }

    @Test fun failure() {
        // Act:
        val response = Response.failure()
        
        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response, `is`(Response.failure()))
        assertThat(response, `is`(not(Response.success())))
    }

    @Test fun failureInstance() {
        // Arrange:
        val response = Response.failure()
        
        // Assert:
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(Error.unknown()))
        assertFalse(response.isSuccess())
        assertThat(response.getValue(), `is`<Any>(Value.EMPTY))
    }

    @Suppress("unused")
    class TestResponse : Response<Value> {

        @Suppress("UNCHECKED_CAST")
        override fun <R : Request> getRequest(): R = Request.EMPTY as R

        override fun isSuccess() = true

        override fun getValue() = Value.EMPTY

        override fun isFailure() = false

        override fun getError() = Error.unknown()
    }
}