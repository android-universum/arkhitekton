/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value

/**
 * A convenience factory class which may be used to create [Response] instances.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Responses.createSuccess
 * @see Responses.createFailure
 */
object Responses {

    /**
     * Convenience method which creates instance of [Response] with [EMPTY][Value.EMPTY] value.
     *
     * @param request The desired request for which is the new response being created.
     * Default is [EMPTY][Request.empty].
     * @return Success response ready to be dispatched.
     *
     * @see createSuccess
     */
    @JvmStatic @JvmOverloads @NonNull
    fun createEmptySuccess(@NonNull request: Request = Request.empty()): Response<Value.Empty> =
        createSuccess(request, Value.EMPTY)

    /**
     * Creates a new instance of successful [Response] with the specified [resultValue].
     *
     * The created response will have specified [NONE][Error.none] error.
     *
     * @param request The desired request for which is the new response being created.
     * @param resultValue The desired result value of the processed request.
     * @param T Type of the result value.
     * @return Success response ready to be dispatched.
     *
     * @see createEmptySuccess
     * @see createFailure
     */
    @JvmStatic @NonNull
    fun <T> createSuccess(@NonNull request: Request, @NonNull resultValue: T): Response<T> =
        ResponseImpl(request, resultValue, Error.none())

    /**
     * Convenience method which creates instance of [Response] with [UNKNOWN][Error.unknown] error.
     *
     * @param request The desired request for which is the new response being created.
     * Default is [EMPTY][Request.empty].
     * @param T Type of the result value.
     * @return Failure response ready to be dispatched.
     *
     * @see createFailure
     */
    @JvmStatic @JvmOverloads @NonNull
    fun <T> createUnknownFailure(@NonNull request: Request = Request.empty()): Response<T> =
        createFailure(request, Error.unknown())

    /**
     * Creates a new instance of [Response] with the specified [error].
     *
     * The created response will have specified [EMPTY][Value.EMPTY] value.
     *
     * @param request The desired request for which is the new response being created.
     * @param error The desired error due to which the request has failed.
     * @param T Type of the result value.
     * @return Failure response ready to be dispatched.
     *
     * @see createUnknownFailure
     * @see createSuccess
     */
    @JvmStatic @NonNull
    @Suppress("UNCHECKED_CAST")
    fun <T> createFailure(@NonNull request: Request, @NonNull error: Error): Response<T> =
        ResponseImpl(request, Value.EMPTY, error) as Response<T>
}