/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value

/**
 * Basic interface describing an object which may represent either **[success][isSuccess]** with a
 * result [value][getValue] or **[failure][isFailure]** with an [error][getError].
 *
 * @param T Type of the result value.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Response.getValue
 */
interface Result<out T> {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [Result] element.
     */
    companion object Contract {

        /**
         * Empty instance of successful result.
         */
        private val SUCCESS: Result<Value.Empty> = object : Result<Value.Empty> {

            /*
             */
            override fun isSuccess(): Boolean = true

            /*
             */
            override fun getValue(): Value.Empty = Value.EMPTY

            /*
             */
            override fun isFailure(): Boolean = false

            /*
             */
            override fun getError(): Error = Error.none()
        }

        /**
         * Empty instance of failed result.
         */
        private val FAILURE: Result<Value.Empty> = object : Result<Value.Empty> {

            /*
             */
            override fun isSuccess(): Boolean = false

            /*
             */
            override fun getValue(): Value.Empty = Value.EMPTY

            /*
             */
            override fun isFailure(): Boolean = true

            /*
             */
            override fun getError(): Error = Error.unknown()
        }

        /**
         * Returns an empty instance of **successful** result with [EMPTY][Value.EMPTY] value
         * and [NONE][Error.none] error.
         *
         * @return Empty result ready to be dispatched.
         */
        @NonNull fun success(): Result<Value.Empty> = SUCCESS

        /**
         * Returns an empty instance of **failed** result with [EMPTY][Value.EMPTY] value
         * and [UNKNOWN][Error.unknown] error.
         *
         * @return Empty result ready to be dispatched.
         */
        @NonNull fun failure(): Result<Value.Empty> = FAILURE
    }

    /*
     * Interface ===================================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Checks whether this result is a success or not.
     *
     * @return `True` if this result represents a success of which result value may be obtained via
     * [getValue], `false` if it is actually a failure.
     *
     * @see isFailure
     */
    fun isSuccess(): Boolean

    /**
     * Returns the value of this successful result.
     *
     * If this result is a failure, the returned value should be a simple `EMPTY` value.
     *
     * @return Value associated with this result.
     *
     * @see isSuccess
     */
    fun getValue(): T

    /**
     * Checks whether this result is a failure or not.
     *
     * @return `True` if this result represents a failure of which error may be obtained via [getError],
     * `false` if it is actually a success.
     *
     * @see isSuccess
     */
    fun isFailure(): Boolean

    /**
     * Returns the error due to which value of this result could not be produced.
     *
     * If this result is a successful one, the returned error should be a simple `NONE` error.
     *
     * @return Error associated with this result.
     *
     * @see isFailure
     */
    fun getError(): Error
}