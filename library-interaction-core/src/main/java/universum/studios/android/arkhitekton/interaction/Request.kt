/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.InstanceBuilder

/**
 * Basic interface for requests which may be used to pass input arguments for execution.
 *
 * **Java language**
 *
 * It is recommended that each request implementation provides its specific builder that may be used
 * to build instances of that request and that such builders implement [Request.Builder] for
 * convenience.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Response
 */
interface Request {

    /*
     * Companion ===================================================================================
     */

    /**
     * Contract for [Request] element.
     */
    companion object Contract {

        /**
         * Empty request instance without any arguments.
         */
        @NonNull internal val EMPTY = object : Empty {}

        /**
         * Returns an instance of empty request which has no arguments for use case.
         *
         * @return Empty request ready to be passed to a desired use case.
         */
        @NonNull fun empty(): Empty = EMPTY
    }

    /*
     * Interface ===================================================================================
     */

    /**
     * A [Request] extension which may be used to indicate that execution function of a particular
     * client interface accepts empty request, that is, it does not require any input arguments for
     * its execution.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @see empty
     */
    interface Empty : Request

    /**
     * Basic interface for builders which may be used to build instances of [Request] implementations.
     *
     * @param T Type of the request of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : Request> : InstanceBuilder<T>

    /*
     * Functions ===================================================================================
     */
}