/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction.usecase

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.InstanceBuilder
import universum.studios.android.arkhitekton.interaction.Response

/**
 * Use case represents a group of actions or event steps which define interaction between actor and
 * the system in order to achieve a specific goal.
 *
 * A single use case may be executed via [execute] passing in an instance of [Request] specific
 * for that use case with desired input/execution arguments. Use case processes that request and
 * should respond with a [Response] indicating whether the execution has been successful or
 * has failed. If **execution** of a use case is **successful** the returned response should contain
 * a result of that execution which may be obtained via [Response.getValue], on the other side,
 * if execution has **failed**, the response should contain a failure describing what did go wrong
 * which may be then obtained via [Response.getError].
 *
 * It is recommended that each use case implementation provides its specific builder that may be
 * used to build instances of that use case type and that such builder implement [UseCase.Builder]
 * for convenience.
 *
 * **Note that it is also recommended to not directly implement this interface as its signature may
 * change over time, but rather to inherit from its [BaseUseCase] implementation.**
 *
 * @param Request Type of the request that this use case accepts.
 * @param ResultValue Type of the result value that this use case will deliver via [Response] in case
 * of successful processing of the passed request.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface UseCase<in Request : universum.studios.android.arkhitekton.interaction.Request, out ResultValue> {

    /*
	 * Companion ===================================================================================
	 */

    /*
     * Interface ===================================================================================
     */

    /**
     * Basic interface for builders which may be used to build instances of [UseCase] implementations.
     *
     * @param T Type of the use case of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : UseCase<*, *>> : InstanceBuilder<T>

    /*
     * Functions ===================================================================================
     */

    /**
     * Executes this use case with input arguments specified via the given [request].
     *
     * @param request The desired request with input/execution arguments.
     * @return Response either with successful result of processing of the specified request or with
     * a failure if processing fails.
     */
    fun execute(@NonNull request: Request): Response<ResultValue>
}