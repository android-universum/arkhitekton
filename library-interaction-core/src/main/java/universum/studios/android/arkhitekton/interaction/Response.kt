/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value

/**
 * Interface describing a response which may be returned as response to execution of a specific [Request].
 * Each response is associated with its request which may be obtained via [getRequest]. If response
 * represents a [success][isSuccess], result value of executed request may be obtained via [getValue],
 * otherwise if response represents a [failure][isFailure], the occurred error may be obtained via [getError].
 *
 * @param T Type of the result value that this response can provide via [getValue] in case of success.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Request
 */
interface Response<out T> : Result<T> {

    /*
     * Companion ===================================================================================
     */

    /**
     * Contract for [Response] element.
     */
    companion object Contract {

        /**
         * Empty instance of successful response.
         */
        @Suppress("UNCHECKED_CAST")
        private val SUCCESS: Response<Value.Empty> = object : Response<Value.Empty> {

            /*
             */
            override fun <R : Request> getRequest(): R = Request.empty() as R

            /*
             */
            override fun isSuccess(): Boolean = true

            /*
             */
            override fun getValue(): Value.Empty = Value.EMPTY

            /*
             */
            override fun isFailure(): Boolean = false

            /*
             */
            override fun getError(): Error = Error.none()
        }

        /**
         * Empty instance of failed response.
         */
        @Suppress("UNCHECKED_CAST")
        private val FAILURE: Response<Value.Empty> = object : Response<Value.Empty> {

            /*
             */
            override fun <R : Request> getRequest(): R = Request.empty() as R

            /*
             */
            override fun isSuccess(): Boolean = false

            /*
             */
            override fun getValue(): Value.Empty = Value.EMPTY

            /*
             */
            override fun isFailure(): Boolean = true

            /*
             */
            override fun getError(): Error = Error.unknown()
        }

        /**
         * Returns an empty instance of **successful** response with [EMPTY][Value.EMPTY] value.
         *
         * @return Empty response ready to be dispatched.
         */
        @NonNull fun success(): Response<Value.Empty> = SUCCESS

        /**
         * Returns an empty instance of **failed** response with [EMPTY][Value.EMPTY] value.
         *
         * @return Empty response ready to be dispatched.
         */
        @NonNull fun failure(): Response<Value.Empty> = FAILURE
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the request this response is associated with.
     *
     * @param R Type of the expected request used to cast the request of this response to.
     * @return Request this response has been created for.
     */
    @NonNull fun <R : Request> getRequest(): R
}