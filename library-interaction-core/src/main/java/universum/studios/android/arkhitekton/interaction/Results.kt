/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Errors
import universum.studios.android.arkhitekton.util.Value

/**
 * A convenience factory class which may be used to create [Result] instances.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see Results.createSuccess
 * @see Results.createFailure
 */
object Results {

    /**
     * Creates a new instance of successful [Result] with the specified [value].
     *
     * The created result will have associated [NONE][Error.none] error.
     *
     * @param value The desired value for the new result.
     * @return Successful result ready to be delivered.
     *
     * @see createFailure
     */
    @JvmStatic @NonNull
    fun <T> createSuccess(@NonNull value: T): Result<T> = BasicResult(value = value)

    /**
     * Creates a new instance of failure [Result] with the specified [error].
     *
     * The created result will have associated [EMPTY][Value.EMPTY] value.
     *
     * @param error The desired error for the new result.
     * @return Failure result ready to be delivered.
     *
     * @see createSuccess
     */
    @JvmStatic @NonNull
    fun <T> createFailure(@NonNull error: Error): Result<T> = BasicResult(error = error)

    /**
     * Creates a new instance of failure [Result] with error unwrapped from the specified [throwable].
     *
     * The created result will have associated [EMPTY][Value.EMPTY] value.
     *
     * @param throwable Throwable with the desired error for the new result.
     * @return Failure result ready to be delivered.
     *
     * @see createFailure
     * @see Error.unwrap
     */
    @JvmStatic @NonNull
    fun <T> createFailure(@NonNull throwable: Throwable): Result<T> = BasicResult(error = Errors.unwrap(throwable))
}