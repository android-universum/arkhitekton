/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.InstanceBuilder
import universum.studios.android.arkhitekton.interaction.usecase.UseCase

/**
 * Interface for architectural elements that provide interaction facade for set of [UseCases][UseCase].
 *
 * It is recommended that each interactor implementation provides its specific builder that may be
 * used to build instances of that interactor type and that such builder implement [Interactor.Builder]
 * for convenience.
 *
 * **Note that it is also recommended to not directly implement this interface as its signature may
 * change over time, but rather to inherit from its [BaseInteractor] implementation.**
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Interactor {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [Interactor] element.
     */
    companion object Contract {

        /**
         * Empty implementation (NULL object) which doesn't perform any *interaction* logic.
         */
        private val EMPTY = object : Interactor {}

        /**
         * Returns an empty instance of [Interactor] which may be used in cases where an instance of
         * concrete Interactor implementation is not needed.
         *
         * @return Empty interactor ready to be used.
         */
        @NonNull fun empty(): Interactor = EMPTY
    }

    /*
     * Interface ===================================================================================
     */

    /**
     * Interface for builders which may be used to build instances of [Interactor] implementations.
     *
     * @param T Type of the interactor of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : Interactor> : InstanceBuilder<T>

    /*
     * Functions ===================================================================================
     */
}