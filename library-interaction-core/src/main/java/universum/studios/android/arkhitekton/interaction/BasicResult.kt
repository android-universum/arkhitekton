/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value

/**
 * Basic implementation of [Result].
 *
 * @param T Type of the value that this result can provide via [getValue] in case of success.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BasicResult with the given [value] and [error].
 * @param value The desired value for the new result. Default is [Value.EMPTY].
 * @param error The desired error for the new result. Default is [Error.none].
 */
@Suppress("UNCHECKED_CAST")
open class BasicResult<out T>(
    /**
     * Result value which has been successfully produced.
     *
     * If this result is a failure, this should be an EMPTY value.
     */
    private val value: T = Value.EMPTY as T,
    /**
     * Error due to which a result value could not be produced.
     *
     * If this result is a success this should be [Error.none].
     */
    private val error: Error = Error.none()
) : Result<T> {

    /*
	 * Companion ===================================================================================
	 */

    /*
	 * Interface ===================================================================================
	 */

    /*
	 * Members =====================================================================================
	 */

    /*
     * Initialization ==============================================================================
     */

    /*
	 * Functions ===================================================================================
	 */

    /*
     */
    override fun isSuccess(): Boolean = !isFailure()

    /*
     */
    override fun getValue(): T = value

    /*
     */
    override fun isFailure(): Boolean = error != Error.none()

    /*
     */
    override fun getError(): Error = error

    /*
     */
    override fun toString(): String {
        val builder = StringBuilder(64)
        builder.append("${Result::class.java.simpleName}(")
        builder.append("isSuccess: ${isSuccess()}, ")
        builder.append("value: $value, ")
        builder.append("isFailure: ${isFailure()}, ")
        builder.append("error: $error")
        return builder.append(")").toString()
    }

    /*
	 * Inner classes ===============================================================================
	 */
}