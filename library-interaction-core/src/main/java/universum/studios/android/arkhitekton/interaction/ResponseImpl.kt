/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import universum.studios.android.arkhitekton.util.Error

/**
 * An implementation of [Response].
 *
 * @param T Type of the result value that this response can provide via [getValue] in case of success.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of ResponseImpl with the given `builder's` configuration.
 * @param request The request the new response to be associated with.
 * @param value The desired result value for the new successful response.
 * @param error The desired error for the new failure response.
 */
internal class ResponseImpl<out T> internal constructor(
    /**
     * The request associated with this response.
     */
    private val request: Request,
    /**
     * Result value which has been produced by successful execution of the associated [request].
     *
     * If this response is a failure, this should be an EMPTY value.
     */
    value: T,
    /**
     * Error which occurred during execution of the associated request.
     *
     * If this response is a success this should be [Error.none].
     */
    error: Error
) : BasicResult<T>(value, error), Response<T> {

    /*
     * Companion ===================================================================================
     */

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /*
     */
    @Suppress("UNCHECKED_CAST")
    override fun <R : Request> getRequest(): R = request as R

    /*
     */
    override fun toString(): String {
        val builder = StringBuilder(64)
        builder.append("${Response::class.java.simpleName}(")
        builder.append("request: ..., ")
        builder.append("isSuccess: ${isSuccess()}, ")
        builder.append("value: ${getValue()}, ")
        builder.append("isFailure: ${isFailure()}, ")
        builder.append("error: ${getError()}")
        return builder.append(")").toString()
    }

    /*
     * Inner classes ===============================================================================
     */
}