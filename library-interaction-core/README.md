Arkhitekton-Interaction-Core
===============

This module contains **core declaration** of `Interactor` element which acts as gateway to
**use cases** and **data sources** and also some core interaction elements like `UseCase`, `Request`, `Response`.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-interaction-core:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-util](https://bitbucket.org/android-universum/arkhitekton/src/main/library-util)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Interactor](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core/src/main/java/universum/studios/android/arkhitekton/interaction/Interactor.kt)
- [Result](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core/src/main/java/universum/studios/android/arkhitekton/interaction/Result.kt)
- [Request](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core/src/main/java/universum/studios/android/arkhitekton/interaction/Request.kt)
- [Response](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core/src/main/java/universum/studios/android/arkhitekton/interaction/Response.kt)
- [UseCase](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core/src/main/java/universum/studios/android/arkhitekton/interaction/usecase/UseCase.kt)
