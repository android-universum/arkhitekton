/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view.presentation

import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.InstanceBuilder
import universum.studios.android.arkhitekton.view.View

/**
 * Interface for presenters which may be used to handle presentation logic across application.
 *
 * **Note that it is recommended to not directly implement this interface as its signature may
 * change over time, but rather to inherit from its [BasePresenter] implementation.**
 *
 * @param V  Type of the view that observers the view model attached to this presenter.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface Presenter<in V : View<*>> {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [Presenter] element.
     */
    companion object Contract {

        /**
         * Returns an empty instance of [Presenter] which may be used in cases where an instance of
         * concrete Presenter implementation is not needed.
         *
         * @return Empty presenter ready to be used.
         */
        @NonNull fun empty(): Presenter<View<*>> = EmptyPresenter
    }

    /*
     * Interface ===================================================================================
     */

    /**
     * Interface for builders which may be used to build instances of [Presenter] implementations.
     *
     * @param T Type of the presenter of which instance this builder can build.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface Builder<out T : Presenter<*>> : InstanceBuilder<T>

    /*
     * Functions ===================================================================================
     */

    /**
     * Attaches the given view to this presenter.
     *
     * Generally, view should be attached to presenter as soon as such view becomes live/created.
     *
     * @param view The desired view to be attached to this presenter.
     */
    fun attachView(@NonNull view: V)

    /**
     * Detaches the given view from this presenter.
     *
     * Generally, view should be detached from presenter as soon as such view becomes death/destroyed.
     *
     * @param view The view that has been attached to this presenter via [attachView].
     * @throws IllegalArgumentException If the given view is not of same reference as the current attached view.
     */
    fun detachView(@NonNull view: V)

    /**
     * Destroys this presenter instance.
     *
     * Once destroyed, such presenter should not be used any further.
     *
     * Destroyed presenter is allowed to throw an exception if some dispatcher is trying to dispatch
     * a specific event to such presenter.
     */
    fun destroy()
}