Arkhitekton-Presentation-Core
===============

This module contains **core declaration** of `Presenter` which acts as handler of changes in
interaction state and presents them to `View` either via `ViewModel` or directly.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-presentation-core:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [Presenter](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core/src/main/java/universum/studios/android/arkhitekton/view/presentation/Presenter.kt)
