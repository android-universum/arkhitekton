Arkhitekton-Control-Base
===============

This module contains **base implementation** of `Controller` element which may be used as base for specific controller implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-control-base:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-control-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-core),
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core),
[arkhitekton-presentation-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BaseController](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-base/src/main/java/universum/studios/android/arkhitekton/control/BaseController.kt)
