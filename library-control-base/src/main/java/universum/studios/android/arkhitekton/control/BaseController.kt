/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import androidx.annotation.CallSuper
import androidx.annotation.NonNull
import universum.studios.android.arkhitekton.ArkhitektonLogging
import universum.studios.android.arkhitekton.interaction.Interactor
import universum.studios.android.arkhitekton.view.presentation.Presenter
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A [Controller] base implementation which is recommended to be inherited by concrete controller
 * implementations in order to take advantage of already implemented stubs of [Controller] interface
 * and also to be 'resistant' against future changes in that interface.
 *
 * Inheritance hierarchies should provide their specific builder which implements [BaseController.BaseBuilder]
 * for convenience.
 *
 * @param I Type of the interactor to which should controller pass interaction requests to be processed.
 * @param P Type of the presenter to which should controller dispatch appropriate responses for&nbsp;
 * processed requests and/or other interaction events.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BaseController with the given `builder's` configuration.
 * @param builder The builder with configuration for the new controller.
 */
abstract class BaseController<out I : Interactor, out P : Presenter<*>>
protected constructor(@NonNull builder: BaseBuilder<*, I, P>) : Controller<P> {

    /*
	 * Companion ===================================================================================
	 */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        // internal const val TAG = "BaseController"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Interactor instance associated with this controller. Controller should use this interactor
     * to process interaction requests and deliver responses for those requests to the [presenter].
     */
    private val interactor: I = builder.interactor

    /**
     * Presenter instance associated with this controller. Controller should dispatch to this
     * presenter any appropriate responses for requests processed by the [interactor] so the presenter
     * may present their results by means that are relevant to it.
     */
    private val presenter: P = builder.presenter

    /**
     * Boolean flag indicating whether this controller is active or not. If controller is active, it
     * should handle all interaction events that are dispatched to it and respond to them accordingly.
     */
    private val active = AtomicBoolean(false)

    /**
     * Boolean flag indicating whether this controller is enabled or not. If controller is temporarily
     * disabled, it should not handle any interaction events until it is enabled again.
     */
    private val enabled = AtomicBoolean(true)

    /**
     * Boolean flag indicating whether this controller is already destroyed or not. `True` if
     * [deactivate] has been already called for this controller instance.
     */
    private val destroyed = AtomicBoolean(false)

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the name of this controller.
     *
     * @return This controller's name.
     */
    @NonNull open fun name(): String = javaClass.simpleName

    /*
     */
    override fun activate() {
        check(!destroyed.get()) { "Deactivated controller cannot be activated again!" }

        if (active.compareAndSet(false, true)) {
            onActivated()
            ArkhitektonLogging.d(name(), "Activated.")
        }
    }

    /**
     * Invoked whenever this controller instance is activated due to call to [activate] when it was
     * in inactive state.
     *
     * From now on, this controller is considered active and should handle all interaction events
     * and respond to them accordingly until it is deactivated via [deactivate] and [onDeactivated]
     * is invoked.
     *
     * **Also note that controller may be disabled via [setEnabled].
     */
    protected open fun onActivated() {
        // Inheritance hierarchies may start here subscriptions if appropriate.
    }

    /*
     */
    override fun isActive(): Boolean = active.get()

    /**
     * Asserts that this controller is activated. If not an exception is thrown.
     *
     * @throws IllegalStateException If this controller instance is not activated.
     * @see activate
     */
    protected fun assertActivated() {
        check(active.get()) { "Not activated!" }
    }

    /*
     */
    override fun deactivate() {
        if (destroyed.get()) {
            return
        }

        if (active.compareAndSet(true, false)) {
            onDeactivated()
            ArkhitektonLogging.d(name(), "Deactivated.")
        }

        destroyed.set(true)
    }

    /**
     * Invoked whenever this controller instance is deactivated due to call to [deactivate] when it
     * was in active state.
     *
     * From now on, this controller is considered inactive and should not handle any interaction
     * events further as its associated interaction component is about to be destroyed (permanently).
     *
     * This implementation destroys the attached presenter via [Presenter.destroy].
     */
    @CallSuper protected open fun onDeactivated() {
        // Inheritance hierarchies should cancel here all subscriptions started in onActivated().
        presenter.destroy()
    }

    /*
     */
    override fun setEnabled(enabled: Boolean) {
        if (this.enabled.compareAndSet(!enabled, enabled)) {
            onEnabledChanged(enabled)
        }
    }

    /**
     * Invoked whenever enabled state of this controller changes as result of call to [setEnabled].
     *
     * @param enabled The current enabled state of this controller instance.
     */
    protected open fun onEnabledChanged(enabled: Boolean) {
        // Inheritance hierarchies may use enabled flag for example to change presentation of a view
        // via the associated presenter.
    }

    /*
     */
    override fun isEnabled(): Boolean = enabled.get()

    /**
     * Returns the interactor to which is this controller propagating interaction requests to be
     * processed by its specific use cases. Responses from those use cases are then appropriately
     * dispatched to this controller's associated presenter.
     *
     * @return This controller's associated interactor.
     * @see getPresenter
     */
    @NonNull fun getInteractor(): I = interactor

    /*
     */
    @NonNull override fun getPresenter(): P = presenter

    /*
     */
    @NonNull override fun toString(): String = "${name()}(active: $active, enabled: $enabled)"

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Base implementation of [Controller.Builder] which should be used by [BaseController] inheritance
     * hierarchies.
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     * @param I Type of the interactor of which instance may be associated with new controller.
     * @param P Type of the presenter of which instance may be associated with new controller.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with empty state.
     */
    abstract class BaseBuilder<B : BaseBuilder<B, I, P>, I : Interactor, P : Presenter<*>>
    protected constructor() : Controller.Builder<Controller<P>> {

        /**
         * Obtains reference to this builder instance which is used for purpose of methods chaining.
         *
         * @return This builder instance.
         */
        protected abstract val self: B

        /**
         * See [BaseController.interactor].
         */
        lateinit var interactor: I

        /**
         * See [BaseController.presenter].
         */
        lateinit var presenter: P

        /**
         * Specifies an interactor to be associated with the new controller.
         *
         * @param interactor The desired interactor for the controller.
         * @return This builder to allow methods chaining.
         */
        fun interactor(@NonNull interactor: I): B {
            this.interactor = interactor
            return self
        }

        /**
         * Specifies a presenter to be associated with the new controller.
         *
         * @param presenter The desired presenter for the controller.
         * @return This builder to allow methods chaining.
         */
        fun presenter(@NonNull presenter: P): B {
            this.presenter = presenter
            return self
        }
    }

    /**
     * A [BaseBuilder] simple implementation which may be used in cases when a concrete implementation
     * of [BaseController] does not require any additional dependencies except its interactor and
     * presenter. Such controllers may be instantiated simply by passing instance of this builder
     * created via [SimpleBuilder.create] to such controller's constructor.
     *
     * @param I Type of the interactor of which instance may be associated with new controller.
     * @param P Type of the presenter of which instance may be associated with new controller.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    class SimpleBuilder<I : Interactor, P : Presenter<*>> private constructor() : BaseBuilder<SimpleBuilder<I, P>, I, P>() {

        /**
         */
        companion object {

            /**
             * Creates a new SimpleBuilder with the specified [interactor] and [presenter].
             *
             * @param interactor The desired interactor for the new controller.
             * @param presenter The desired presenter for the new controller.
             * @param I Type of the interactor of which instance may be associated with the new controller.
             * @param P Type of the presenter of which instance may be associated with the new controller.
             * @return Builder instance ready to be passed to the controller's constructor.
             */
            @JvmStatic @NonNull
            fun <I : Interactor, P : Presenter<*>> create(
                @NonNull interactor: I,
                @NonNull presenter: P
            ): SimpleBuilder<I, P> = SimpleBuilder<I, P>().apply {
                this.interactor = interactor
                this.presenter = presenter
            }
        }

        /*
         */
        override val self = this

        /*
         */
        override fun build(): Controller<P> = throw UnsupportedOperationException("Pass instance of SimpleBuilder to the appropriate constructor!")
    }
}