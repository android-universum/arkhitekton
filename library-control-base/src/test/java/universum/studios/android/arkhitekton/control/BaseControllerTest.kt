/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.control

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.arkhitekton.interaction.Interactor
import universum.studios.android.arkhitekton.view.presentation.Presenter
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class BaseControllerTest : TestCase {

    @Test fun instantiation() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)

        // Act:
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Assert:
        assertThat(controller.getInteractor(), `is`(mockInteractor))
        assertThat(controller.getPresenter(), `is`(mockPresenter))
        assertFalse(controller.isActive())
        assertTrue(controller.isEnabled())
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun instantiationViaSimpleBuilder() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)

        // Act:
        val controller = TestController.create(mockInteractor, mockPresenter)

        // Assert:
        assertThat(controller.getInteractor(), `is`(mockInteractor))
        assertThat(controller.getPresenter(), `is`(mockPresenter))
        assertFalse(controller.isActive())
        assertTrue(controller.isEnabled())
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun name() {
        // Arrange:
        val controller = TestController.TestBuilder().apply {
            this.interactor = Interactor.empty()
            this.presenter = Presenter.empty()
        }.build()

        // Act + Assert:
        assertThat(controller.name(), `is`("TestController"))
    }

    @Test fun activate() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Act:
        controller.activate()

        // Assert:
        assertTrue(controller.isActive())
        assertThat(controller.onActivatedInvocationsCount, `is`(1))
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun activateWhenAlreadyActivated() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        controller.activate()

        // Act:
        controller.activate()

        // Assert:
        assertTrue(controller.isActive())
        assertThat(controller.onActivatedInvocationsCount, `is`(1))
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test(expected = IllegalStateException::class)
    fun activateWhenAlreadyDeactivated() {
        // Arrange:
        val controller = TestController.TestBuilder().apply {
            this.interactor = Interactor.empty()
            this.presenter = Presenter.empty()
        }.build()
        controller.activate()
        controller.deactivate()

        // Act:
        controller.activate()
    }

    @Test fun assertActivatedWhenActive() {
        // Arrange:
        val controller = TestController.TestBuilder().apply {
            this.interactor = Interactor.empty()
            this.presenter = Presenter.empty()
        }.build()
        controller.activate()

        // Act:
        controller.callAssertActivated()
    }

    @Test(expected = IllegalStateException::class)
    fun assertActivatedWhenNotActive() {
        // Arrange:
        val controller = TestController.TestBuilder().apply {
            this.interactor = Interactor.empty()
            this.presenter = Presenter.empty()
        }.build()

        // Act:
        controller.callAssertActivated()
    }

    @Test fun deactivate() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        controller.activate()

        // Act:
        controller.deactivate()

        // Assert:
        assertFalse(controller.isActive())
        assertThat(controller.onDeactivatedInvocationsCount, `is`(1))
        verify(mockPresenter).destroy()
        verifyNoMoreInteractions(mockPresenter)
        verifyNoInteractions(mockInteractor)
    }

    @Test fun deactivateWhenNotActivated() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Act:
        controller.deactivate()

        // Assert:
        assertFalse(controller.isActive())
        assertThat(controller.onDeactivatedInvocationsCount, `is`(0))
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun deactivateWhenAlreadyDeactivated() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        controller.activate()
        controller.deactivate()

        // Act:
        controller.deactivate()

        // Assert:
        assertFalse(controller.isActive())
        assertThat(controller.onDeactivatedInvocationsCount, `is`(1))
        verify(mockPresenter).destroy()
        verifyNoMoreInteractions(mockPresenter)
        verifyNoInteractions(mockInteractor)
    }

    @Test fun setEnabledDisabled() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Act + Assert:

        // 1)
        controller.setEnabled(false)
        assertFalse(controller.isEnabled())
        assertThat(controller.onEnabledChangedInvocationsCount, `is`(1))
        assertFalse(controller.onEnabledChangedInvokedValue)

        // 2)
        controller.setEnabled(true)
        assertTrue(controller.isEnabled())
        assertThat(controller.onEnabledChangedInvocationsCount, `is`(2))
        assertTrue(controller.onEnabledChangedInvokedValue)
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun setEnabledWhenAlreadyEnabled() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()

        // Act:
        controller.setEnabled(true)

        // Assert:
        assertTrue(controller.isEnabled())
        assertThat(controller.onEnabledChangedInvocationsCount, `is`(0))
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun setDisabledWhenAlreadyDisabled() {
        // Arrange:
        val mockInteractor = mock(Interactor::class.java)
        val mockPresenter = mock(Presenter::class.java)
        val controller = TestController.TestBuilder().apply {
            this.interactor = mockInteractor
            this.presenter = mockPresenter
        }.build()
        controller.setEnabled(false)

        // Act:
        controller.setEnabled(false)

        // Assert:
        assertFalse(controller.isEnabled())
        assertThat(controller.onEnabledChangedInvocationsCount, `is`(1))
        assertFalse(controller.onEnabledChangedInvokedValue)
        verifyNoInteractions(mockInteractor, mockPresenter)
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val controller = TestController.TestBuilder().apply {
            this.interactor = Interactor.empty()
            this.presenter = Presenter.empty()
        }.build()

        // Assert:
        assertThat(controller.toString(), `is`("TestController(active: false, enabled: true)"))
    }

    private class TestController(builder: BaseBuilder<*, Interactor, Presenter<*>>) :
        BaseController<Interactor, Presenter<*>>(builder) {

        companion object {

            fun create(interactor: Interactor, presenter: Presenter<*>) =
                TestController(SimpleBuilder.create(interactor, presenter))
        }

        var onActivatedInvocationsCount = 0
        var onDeactivatedInvocationsCount = 0
        var onEnabledChangedInvocationsCount = 0
        var onEnabledChangedInvokedValue = false

        override fun onActivated() {
            super.onActivated()

            onActivatedInvocationsCount++
        }

        override fun onDeactivated() {
            super.onDeactivated()

            onDeactivatedInvocationsCount++
        }

        override fun onEnabledChanged(enabled: Boolean) {
            super.onEnabledChanged(enabled)

            onEnabledChangedInvocationsCount++
            onEnabledChangedInvokedValue = enabled
        }

        fun callAssertActivated() = assertActivated()

        class TestBuilder : BaseController.BaseBuilder<TestBuilder, Interactor, Presenter<*>>() {

            override val self = this
            override fun build() = TestController(this)
        }
    }
}