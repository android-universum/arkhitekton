@Arkhitekton-Control
===============

This module groups the following modules into one **single group**:

- [Control-Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-core)
- [Control-Base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-base)
- [Control-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-reactive)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-interaction:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-data-reactive](https://bitbucket.org/android-universum/arkhitekton/src/master/library-data-reactive),
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction-core),
[arkhitekton-interaction-reactive](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction-reactive),
[arkhitekton-presentation-core](https://bitbucket.org/android-universum/arkhitekton/src/master/library-presentation-core),
[arkhitekton-presentation-reactive](https://bitbucket.org/android-universum/arkhitekton/src/master/library-presentation-reactive),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/master/library-view)