@Arkhitekton-Data
===============

This module groups the following modules into one **single group**:

- [Data-Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-core)
- [Data-Base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-base)
- [Data-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-reactive)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-data:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core),
[arkhitekton-util](https://bitbucket.org/android-universum/arkhitekton/src/master/library-util)