/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.source

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class BaseRepositoryTest : TestCase {

    @Test fun name() {
        // Arrange:
        val repository = TestRepository()

        // Assert:
        assertThat(repository.name(), `is`("TestRepository"))
    }

    @Test fun isEmpty() {
        // Arrange:
        val repository = TestRepository()

        // Assert:
        assertTrue(repository.isEmpty())
    }

    @Test fun clear() {
        // Arrange:
        val repository = TestRepository()

        // Act + Assert:
        repository.clear()
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val repository = TestRepository()

        // Assert:
        assertThat(repository.toString(), `is`("TestRepository(isEmpty: true)"))
    }
    
    private class TestRepository : BaseRepository()
}