/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.source

/**
 * Repository is a [data source][DataSource] which encapsulates [local data source][LocalDataSource]
 * and [remote data source][RemoteDataSource], that is, it hides these data sources from its user
 * and thus acts as a facade for them.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
abstract class BaseRepository : DataSource {

    /**
     * Returns simple name of this repository.
     *
     * Default implementation returns simple name of this repository's class.
     *
     * @return Name of this repository.
     */
    open fun name(): String = javaClass.simpleName

    /*
     */
    override fun isEmpty(): Boolean = true

    /*
     */
    override fun clear() {}

    /*
     */
    override fun toString(): String = "${name()}(isEmpty: ${isEmpty()})"
}