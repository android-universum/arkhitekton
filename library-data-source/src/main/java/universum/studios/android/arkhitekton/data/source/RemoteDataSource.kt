/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.source

/**
 * [DataSource] which provides access to entities stored in a remote location.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface RemoteDataSource : DataSource {

    /*
     */
    override fun isEmpty(): Boolean {
        throw UnsupportedOperationException("Cannot check if remote data source is empty by default!")
    }

    /*
     */
    override fun clear() {
        throw UnsupportedOperationException("Cannot clear remote data source by default!")
    }
}