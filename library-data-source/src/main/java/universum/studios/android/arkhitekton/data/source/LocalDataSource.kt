/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.data.source

import universum.studios.android.arkhitekton.interaction.Result

/**
 * [DataSource] which provides access to entities stored locally on the device.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface LocalDataSource<T> : DataSource {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [LocalDataSource] element.
     */
    companion object Contract {

        /**
         * Constant that identifies no entities count.
         */
        const val NO_ENTITIES = -1
    }

    /*
     * Interface ===================================================================================
     */

    /**
     * Lock which may be used to lock and unlock data notifications.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @see [LocalDataSource.lockNotifications]
     */
    interface NotificationsLock {

        /**
         * Contract for [NotificationsLock] element.
         */
        companion object Contract {

            /**
             *  Empty implementation (NULL object) which doesn't perform any *locking* logic.
             */
            val EMPTY: NotificationsLock = object : NotificationsLock {

                /*
                 */
                override fun release() {}
            }
        }

        /**
         * Releases this notifications lock.
         *
         * If this lock is already released, this function does nothing.
         */
        fun release()
    }

    /*
     * Functions ===================================================================================
     */

    /**
     * Locks further notifications of changes requested via [notifyChanges] until the acquired lock
     * is released via [NotificationsLock.release]. Each notifications lock should be released when
     * it is no longer needed.
     *
     * **Note that notifications are lock until all acquired locks are released.**
     *
     * @return  Acquired notifications lock.
     */
    fun lockNotifications(): NotificationsLock

    /**
     * Queries all entities stored in this data source.
     *
     * @return All entities of this data source or empty list if there are no entities stored yet.
     */
    fun query(): List<T>

    /**
     * Queries a single entity stored in this data source.
     *
     * @param entityId Unique id of the desired entity to be queried.
     * @return The requested entity or `null` if there is no such entity stored.
     */
    fun query(entityId: Long): T?

    /**
     * Inserts the specified [entity] as a new entry into this data source.
     *
     * @param entity The desired entity to be inserted.
     * @return Result of this operation.
     */
    fun insert(entity: T): Result<Long>

    /**
     * Inserts all [entities] specified within the given list into this data source.
     *
     * @param entities The desired entities to be inserted.
     * @return Result of this operation.
     */
    fun insert(entities: List<T>): Result<Array<Long>>

    /**
     * Updates the specified [entity] in this data source.
     *
     * @param entity The desired entity to be updated.
     * @return Result of this operation.
     */
    fun update(entity: T): Result<Int>

    /**
     * Updates all [entities] specified within the given list in this data source.
     *
     * @param entities The desired entities to be updated.
     * @return Result of this operation.
     */
    fun update(entities: List<T>): Result<Int>

    /**
     * Deletes the specified [entity] from this data source.
     *
     * @param entity The desired entity to be deleted.
     * @return Result of this operation.
     */
    fun delete(entity: T): Result<Int>

    /**
     * Deletes all entities specified in the given list from this data source.
     *
     * @param entities The desired entities to be deleted.
     * @return Result of this operation.
     */
    fun delete(entities: List<T>): Result<Int>

    /**
     * Notifies all observers that are registered for changes made in data of this data source.
     */
    fun notifyChanges()
}