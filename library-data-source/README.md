Arkhitekton-Data-Source
===============

This module contains **base declaration** of `DataSource` which acts as gateway to data.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-data-source:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DataSource](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-source/src/main/java/universum/studios/android/arkhitekton/data/source/DataSource.kt)
- [LocalDataSource](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-source/src/main/java/universum/studios/android/arkhitekton/data/source/LocalDataSource.kt)
- [RemoteDataSource](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-source/src/main/java/universum/studios/android/arkhitekton/data/source/RemoteDataSource.kt)
- [BaseRepository](https://bitbucket.org/android-universum/arkhitekton/src/main/library-data-source/src/main/java/universum/studios/android/arkhitekton/data/source/BaseRepository.kt)
