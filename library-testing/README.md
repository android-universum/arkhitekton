Arkhitekton-Testing
===============

This module contains **test matchers** and **mock factories** for architecture elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-testing:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [SuccessMatcher](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/matcher/SuccessMatcher.kt)
- [FailureMatcher](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/matcher/FailureMatcher.kt)
- [SuccessResponseMatcher](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/matcher/SuccessResponseMatcher.kt)
- [FailureResponseMatcher](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/matcher/FailureResponseMatcher.kt)

## Mock factories ##

- [control](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/control.kt)
- [interaction](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/interaction.kt)
- [presentation](https://bitbucket.org/android-universum/arkhitekton/src/main/library-testing/src/main/java/universum/studios/android/arkhitekton/test/presentation.kt)
- [view](https://bitbucket.org/android-universum/arkhitekton/src/master/library-testing/src/main/java/universum/studios/android/arkhitekton/test/view.kt)
