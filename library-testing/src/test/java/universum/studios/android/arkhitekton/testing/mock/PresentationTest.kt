/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.mock

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.presentation.Presenter
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
internal class PresentationTest : TestCase {

    @Test fun mockPresenter() {
        // Act:
        val presenter = mockPresenter(TestPresenter::class.java)

        // Assert:
        assertThat(presenter, `is`(notNullValue()))
    }

    private interface TestPresenter : Presenter<View<*>>
}