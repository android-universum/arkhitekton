/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.arkhitekton.interaction.Request
import universum.studios.android.arkhitekton.interaction.Responses
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class FailureResponseMatcherTest : TestCase {

    @Test fun failure() {
        // Act:
        val matcher = FailureResponseMatcher.failure<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(FailureResponseMatcher.failure<Any>())))
    }

    @Test fun matches() {
        // Arrange + Act + Assert:
        assertThat(Responses.createFailure(Request.empty(), Error.unknown()), `is`(FailureResponseMatcher.failure<Any>()))
        assertThat(Responses.createSuccess(Request.empty(), Value.EMPTY), `is`(not(FailureResponseMatcher.failure<Any>())))
    }
}