/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.mock

import androidx.lifecycle.Lifecycle
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewModel
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
internal class ViewTest : TestCase {

    @Test fun mockResumedView() {
        // Act:
        val view = mockResumedView(TestView::class.java)

        // Assert:
        assertThat(view, `is`(notNullValue()))
        assertThat(view.getViewModel(), `is`(nullValue()))
        assertThat(view.lifecycle, `is`(notNullValue()))
        assertThat(view.lifecycle.currentState, `is`(Lifecycle.State.RESUMED))
    }

    @Test fun mockView() {
        // Act:
        val view = mockView(TestView::class.java)

        // Assert:
        assertThat(view, `is`(notNullValue()))
        assertThat(view.getViewModel(), `is`(nullValue()))
        assertThat(view.lifecycle, `is`(notNullValue()))
        assertThat(view.lifecycle.currentState, `is`(Lifecycle.State.INITIALIZED))
    }

    @Test fun mockViewWithViewModel() {
        // Arrange:
        val mockViewModel = mock(TestViewModel::class.java)

        // Act:
        val view = mockView(TestView::class.java, mockViewModel)

        // Assert:
        assertThat(view, `is`(notNullValue()))
        assertThat(view.getViewModel(), `is`(mockViewModel))
        assertThat(view.lifecycle, `is`(notNullValue()))
        assertThat(view.lifecycle.currentState, `is`(Lifecycle.State.INITIALIZED))
    }

    @Test fun mockViewWithState() {
        // Act:
        val view = mockView(TestView::class.java, state = Lifecycle.State.STARTED)

        // Assert:
        assertThat(view, `is`(notNullValue()))
        assertThat(view.getViewModel(), `is`(nullValue()))
        assertThat(view.lifecycle, `is`(notNullValue()))
        assertThat(view.lifecycle.currentState, `is`(Lifecycle.State.STARTED))
    }

    @Test fun mockViewWithViewModelAndState() {
        // Arrange:
        val mockViewModel = mock(TestViewModel::class.java)

        // Act:
        val view = mockView(TestView::class.java, mockViewModel, state = Lifecycle.State.STARTED)

        // Assert:
        assertThat(view, `is`(notNullValue()))
        assertThat(view.getViewModel(), `is`(mockViewModel))
        assertThat(view.lifecycle, `is`(notNullValue()))
        assertThat(view.lifecycle.currentState, `is`(Lifecycle.State.STARTED))
    }

    private interface TestViewModel : ViewModel
    private interface TestView : View<TestViewModel>
}