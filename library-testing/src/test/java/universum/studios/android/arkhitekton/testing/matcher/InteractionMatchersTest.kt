/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class InteractionMatchersTest : TestCase {

    @Test fun success() {
        // Act:
        val matcher = InteractionMatchers.success<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(InteractionMatchers.success<Any>())))
    }

    @Test fun failure() {
        // Act:
        val matcher = InteractionMatchers.failure<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(InteractionMatchers.failure<Any>())))
    }

    @Test fun successResponse() {
        // Act:
        val matcher = InteractionMatchers.successResponse<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(InteractionMatchers.successResponse<Any>())))
    }

    @Test fun failureResponse() {
        // Act:
        val matcher = InteractionMatchers.failureResponse<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(InteractionMatchers.failureResponse<Any>())))
    }
}