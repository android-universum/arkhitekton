/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.mock

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import universum.studios.android.arkhitekton.interaction.ParamRequest
import universum.studios.android.arkhitekton.interaction.Request
import universum.studios.android.arkhitekton.interaction.Responses
import universum.studios.android.arkhitekton.interaction.Result
import universum.studios.android.arkhitekton.interaction.Results
import universum.studios.android.arkhitekton.interaction.usecase.UseCase
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Errors
import universum.studios.android.testing.TestCase
import universum.studios.android.testing.kotlin.capture

/**
 * @author Martin Albedinsky
 */
internal class InteractionTest : TestCase {

    @Test fun paramRequestCaptor() {
        // Arrange:
        val request = ParamRequest("test")
        val mockInteractor = mock(TestInteractor::class.java)
        val captor = paramRequestCaptor<String>()
        
        // Act:
        mockInteractor.request(request)
        
        // Assert:
        verify(mockInteractor).request(capture(captor))
        assertThat(captor.value, `is`(request))
    }

    @Test fun resultCaptor() {
        // Arrange:
        val result = Results.createSuccess("test")
        val mockPresenter = mock(TestPresenter::class.java)
        val captor = resultCaptor<String>()
        
        // Act:
        mockPresenter.onResult(result)
        
        // Assert:
        verify(mockPresenter).onResult(capture(captor))
        assertThat(captor.value, `is`(result))
    }

    @Test fun mockUseCaseWithSuccess() {
        // Act:
        val useCase = mockUseCase("data")
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        val response = useCase.execute(Request.empty())
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`<Any>("data"))
    }

    @Test fun mockUseCaseWithFailure() {
        // Arrange:
        val error = Errors.create("test")
        
        // Act:
        val useCase = mockUseCase<Any>(error)
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        val response = useCase.execute(Request.empty())
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(error))
    }

    @Test fun mockUseCaseWithResponse() {
        // Arrange:
        val response = Responses.createSuccess(Request.empty(), "test")
        
        // Act:
        val useCase = mockUseCase<Request, String>(response)
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        assertThat(useCase.execute(Request.empty()), `is`(response))
    }

    @Test(expected = UnsupportedOperationException::class)
    fun mockUseCaseEmpty() {
        // Act:
        val useCase = mockUseCase<Request, String>()
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        useCase.execute(Request.empty())
    }

    @Test fun mockUseCaseCustomWithSuccess() {
        // Act:
        val useCase = mockUseCase(TestUseCase::class.java, true)
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        val response = useCase.execute(Request.empty())
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`<Any>(true))
    }

    @Test fun mockUseCaseCustomWithFailure() {
        // Arrange:
        val error = Errors.create("test")
        
        // Act:
        val useCase = mockUseCase(TestUseCase::class.java, error)
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        val response = useCase.execute(Request.empty())
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(error))
    }

    @Test fun mockUseCaseCustomWithResponse() {
        // Arrange:
        val response = Responses.createSuccess(Request.empty(), false)
        
        // Act:
        val useCase = mockUseCase(TestUseCase::class.java, response)
        
        // Assert:
        assertThat(useCase, `is`(notNullValue()))
        assertThat(useCase.execute(Request.empty()), `is`(response))
    }

    private interface TestUseCase : UseCase<Request, Boolean>

    private interface TestInteractor {

        fun request(request: ParamRequest<String>)
    }

    private interface TestPresenter {

        fun onResult(result: Result<String>)
    }
}