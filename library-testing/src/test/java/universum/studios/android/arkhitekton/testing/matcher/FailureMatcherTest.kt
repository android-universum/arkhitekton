/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.arkhitekton.interaction.Results
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class FailureMatcherTest : TestCase {

    @Test fun failure() {
        // Act:
        val matcher = FailureMatcher.failure<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(FailureMatcher.failure<Any>())))
    }

    @Test fun matches() {
        // Assert:
        assertThat(Results.createFailure<Any>(Error.unknown()), `is`(FailureMatcher.failure<Any>()))
        assertThat(Results.createSuccess<Any>(Value.EMPTY), `is`(not(FailureMatcher.failure<Any>())))
    }
}