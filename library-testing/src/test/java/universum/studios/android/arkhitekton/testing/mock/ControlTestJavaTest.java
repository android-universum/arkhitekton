/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.mock;

import org.junit.Test;

import universum.studios.android.arkhitekton.control.Controller;
import universum.studios.android.arkhitekton.view.View;
import universum.studios.android.arkhitekton.view.presentation.Presenter;
import universum.studios.android.testing.TestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static universum.studios.android.arkhitekton.testing.mock.ControlKt.mockController;
import static universum.studios.android.arkhitekton.testing.mock.PresentationKt.mockPresenter;

/**
 * @author Martin Albedinsky
 */
public class ControlTestJavaTest implements TestCase {

    @Test public void testMockController() {
        // Act:
        final Controller controller = mockController(TestSimpleController.class);

        // Assert:
        assertThat(controller, is(notNullValue()));
        assertThat(controller.getPresenter(), is(Presenter.Contract.empty()));
    }

    @Test public void testMockControllerWithPresenterClass() {
        // Act:
        final Controller controller = mockController(TestController.class, TestPresenter.class);

        // Assert:
        assertThat(controller, is(notNullValue()));
        assertThat(controller.getPresenter(), is(notNullValue()));
    }

    @Test public void testMockControllerWithPresenterInstance() {
        // Arrange:
        final Presenter presenter = mockPresenter(TestPresenter.class);

        // Act:
        final Controller controller = mockController(TestController.class, presenter);

        // Assert:
        assertThat(controller, is(notNullValue()));
        assertThat(controller.getPresenter(), is(presenter));
    }

    private interface TestPresenter extends Presenter<View<?>> {}
    private interface TestController extends Controller<TestPresenter> {}

    private interface TestSimpleController extends Controller {}
}