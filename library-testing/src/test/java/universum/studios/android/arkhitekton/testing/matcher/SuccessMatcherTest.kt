/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.arkhitekton.interaction.Results
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class SuccessMatcherTest : TestCase {

    @Test fun success() {
        // Act:
        val matcher = SuccessMatcher.success<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(SuccessMatcher.success<Any>())))
    }

    @Test fun matches() {
        // Arrange + Act + Assert:
        assertThat(Results.createSuccess(Value.EMPTY), `is`(SuccessMatcher.success<Any>()))
        assertThat(Results.createFailure(Error.unknown()), `is`(not(SuccessMatcher.success<Any>())))
    }

    @Test fun matchesWithEmptyValue() {
        // Arrange + Act + Assert:
        assertThat(Results.createSuccess(Value.EMPTY), `is`(SuccessMatcher.success<Any>().withEmptyValue()))
        assertThat(Results.createSuccess("test"), `is`(not(SuccessMatcher.success<Any>().withEmptyValue())))
    }

    @Test fun matchesWithValue() {
        // Arrange + Act + Assert:
        assertThat(Results.createSuccess("test"), `is`(SuccessMatcher.success<Any>().withValue("test")))
        assertThat(Results.createSuccess(Value.EMPTY), `is`(not(SuccessMatcher.success<Any>().withValue("test"))))
    }
}