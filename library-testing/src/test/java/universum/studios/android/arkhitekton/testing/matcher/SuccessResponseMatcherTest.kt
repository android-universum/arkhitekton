/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.interaction.Request
import universum.studios.android.arkhitekton.interaction.Responses
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class SuccessResponseMatcherTest : TestCase {

    @Test fun success() {
        // Act:
        val matcher = SuccessResponseMatcher.success<Any>()

        // Assert:
        assertThat(matcher, `is`(notNullValue()))
        assertThat(matcher, `is`(not<Any>(SuccessResponseMatcher.success<Any>())))
    }

    @Test fun matches() {
        // Arrange + Act + Assert:
        assertThat(Responses.createSuccess(Request.empty(), Value.EMPTY), `is`(SuccessResponseMatcher.success<Any>()))
        assertThat(Responses.createFailure(Request.empty(), Error.unknown()), `is`(not(SuccessResponseMatcher.success<Any>())))
    }

    @Test fun matchesForRequest() {
        // Arrange:
        val requestFirst = Request.empty()
        val requestSecond = mock(TestRequest::class.java)

        // Act + Assert:
        assertThat(Responses.createSuccess(requestFirst, Value.EMPTY), `is`(SuccessResponseMatcher.success<Any>().forRequest(requestFirst)))
        assertThat(Responses.createSuccess(requestFirst, Value.EMPTY), `is`(not(SuccessResponseMatcher.success<Any>().forRequest(requestSecond))))
    }

    @Test fun matchesWithEmptyValue() {
        // Arrange + Act + Assert:
        assertThat(Responses.createSuccess(Request.empty(), Value.EMPTY), `is`(SuccessResponseMatcher.success<Any>().withEmptyValue()))
        assertThat(Responses.createSuccess(Request.empty(), "test"), `is`(not(SuccessResponseMatcher.success<Any>().withEmptyValue())))
    }

    @Test fun matchesWithValue() {
        // Arrange + Act + Assert:
        assertThat(Responses.createSuccess(Request.empty(), "test"), `is`(SuccessResponseMatcher.success<Any>().withValue("test")))
        assertThat(Responses.createSuccess(Request.empty(), Value.EMPTY), `is`(not(SuccessResponseMatcher.success<Any>().withValue("test"))))
    }

    private interface TestRequest : Request
}