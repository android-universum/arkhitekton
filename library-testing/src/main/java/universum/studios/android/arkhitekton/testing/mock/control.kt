/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.arkhitekton.testing.mock

import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.control.Controller
import universum.studios.android.arkhitekton.view.presentation.Presenter

/**
 * Creates a new [Controller] mock object of the given [controllerClass] with [empty][Presenter.empty]
 * presenter associated that will be available via [Controller.getPresenter].
 *
 * @param controllerClass The desired controller class to create mock of.
 * @param C Type of the controller.
 * @return New mock instance ready to be used.
 */
fun <C : Controller<*>> mockController(controllerClass: Class<C>): C =
    mockController(controllerClass, Presenter.empty())

/**
 * Creates a new [Controller] mock object of the given [controllerClass] with a new [Presenter] mock
 * object of the given [presenterClass] associated.
 *
 * @param controllerClass The desired controller class to create mock of.
 * @param presenterClass The desired presenter class to create mock of and to be available via [Controller.getPresenter].
 * @param C Type of the controller.
 * @param P Type of the presenter to be associated with the controller.
 * @return New mock instance ready to be used.
 */
fun <C : Controller<P>, P : Presenter<*>> mockController(controllerClass: Class<C>, presenterClass: Class<P>): C =
    mockController(controllerClass, mockPresenter(presenterClass))

/**
 * Creates a new [Controller] mock object of the given [controllerClass] with the given [presenter]
 * associated.
 *
 * @param controllerClass The desired controller class to create mock of.
 * @param presenter The desired presenter instance to be available via [Controller.getPresenter].
 * @param C Type of the controller.
 * @param P Type of the presenter to be associated with the controller.
 * @return New mock instance ready to be used.
 */
fun <C : Controller<P>, P : Presenter<*>> mockController(controllerClass: Class<C>, presenter: P): C {
    val controller = mock(controllerClass)

    `when`(controller.getPresenter()).thenReturn(presenter)

    return controller
}
