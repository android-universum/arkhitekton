/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.arkhitekton.testing.mock

import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.Factory
import universum.studios.android.arkhitekton.Provider

/**
 * Creates a new [Factory] mock object that provides the given [element].
 *
 * @param element The desired element to be provided.
 * @param T Type of the element to be provided.
 * @return New mock instance ready to be used.
 */
@Suppress("UNCHECKED_CAST")
fun <T> mockFactory(element: T): Factory<T> {
    val factory = mock(Factory::class.java)
    `when`(factory.create()).thenReturn(element)
    return factory as Factory<T>
}

/**
 * Creates a new [Provider] mock object that provides the given [element].
 *
 * @param element The desired element to be provided.
 * @param T Type of the element to be provided.
 * @return New mock instance ready to be used.
 */
@Suppress("UNCHECKED_CAST")
fun <T> mockProvider(element: T): Provider<T> {
    val provider = mock(Provider::class.java)
    `when`(provider.get()).thenReturn(element)
    return provider as Provider<T>
}