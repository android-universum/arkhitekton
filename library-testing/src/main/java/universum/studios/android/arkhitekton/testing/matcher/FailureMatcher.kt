/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import universum.studios.android.arkhitekton.interaction.Result
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.addFlag
import universum.studios.android.arkhitekton.util.hasFlag
import universum.studios.android.arkhitekton.util.removeFlag

/**
 * A [Matcher] that tests whether the examined object is [failure][Result.isFailure] result.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see SuccessMatcher
 */
class FailureMatcher<T> : TypeSafeMatcher<Result<T>>(Result::class.java) {

    /**
     */
    companion object {

        /**
         * Flag indicating that whole error should be checked as part of [matches] implementation.
         */
        private const val CHECK_ERROR = 1

        /**
         * Flag indicating that error code should be checked as part of [matches] implementation.
         */
        private const val CHECK_ERROR_CODE = 1 shl 1

        /**
         * Flag indicating that error cause should be checked as part of [matches] implementation.
         */
        private const val CHECK_ERROR_CAUSE = 1 shl 2

        /**
         * Creates a matcher that matches if examined object is [failure][Result.isFailure] result.
         *
         * For example:
         *
         * `assertThat(result, is(failure())`
         */
        @JvmStatic fun <T> failure(): FailureMatcher<T> = FailureMatcher()
    }

    /**
     * Flags determining what checks should be performed as part of [matches] implementation.
     */
    private var expectedChecks: Int = 0

    /**
     * Error that the examined result is expected to have.
     */
    private var expectedError: Error? = null

    /**
     * Code that the examined result's error is expected to have.
     */
    private var expectedErrorCode: String? = null

    /**
     * Cause that the examined result's error is expected to have.
     */
    private var expectedErrorCause: Throwable? = null

    /**
     * Adds check requesting this matcher to match if examined result has the specified [error].
     *
     * @param error The desired error to expect.
     * @return This matcher to allow methods chaining.
     *
     * @see withErrorCode
     * @see withErrorCause
     * @see Result.getError
     */
    fun withError(error: Error): FailureMatcher<T> {
        this.expectedChecks = expectedChecks.addFlag(CHECK_ERROR).removeFlag(CHECK_ERROR_CODE or CHECK_ERROR_CAUSE)
        this.expectedError = error
        return this
    }

    /**
     * Adds check requesting this matcher to match if examined result has error with the specified [code].
     *
     * @param code The desired error code to expect.
     * @return This matcher to allow methods chaining.
     *
     * @see withError
     * @see Result.getError
     */
    fun withErrorCode(code: String): FailureMatcher<T> {
        this.expectedChecks = expectedChecks.addFlag(CHECK_ERROR_CODE).removeFlag(CHECK_ERROR)
        this.expectedErrorCode = code
        return this
    }

    /**
     * Same as [withErrorCause] available for more fluent methods chaining.
     */
    fun andErrorCause(cause: Throwable): FailureMatcher<T> = withErrorCause(cause)

    /**
     * Adds check requesting this matcher to match if examined result has error with the specified [cause].
     *
     * @param cause The desired error cause to expect.
     * @return This matcher to allow methods chaining.
     *
     * @see withError
     * @see Result.getError
     */
    fun withErrorCause(cause: Throwable): FailureMatcher<T> {
        this.expectedChecks = expectedChecks.addFlag(CHECK_ERROR_CAUSE).removeFlag(CHECK_ERROR)
        this.expectedErrorCause = cause
        return this
    }

    /*
     */
    override fun matchesSafely(item: Result<T>): Boolean {
        var matches = item.isFailure()
        val error = item.getError()

        if (expectedChecks.hasFlag(CHECK_ERROR) && error != expectedError) {
            matches = false
        } else {
            if (expectedChecks.hasFlag(CHECK_ERROR_CODE) && error.code() != expectedErrorCode) {
                matches = false
            }

            if (expectedChecks.hasFlag(CHECK_ERROR_CAUSE) && error.cause() != expectedErrorCause) {
                matches = false
            }
        }

        return matches
    }

    /*
     */
    override fun describeTo(description: Description) {
        description.appendText("failure")

        if (expectedChecks.hasFlag(CHECK_ERROR)) {
            description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
            description.appendText("- with error ").appendValue(expectedError)
        }

        if (expectedChecks.hasFlag(CHECK_ERROR_CODE)) {
            description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
            description.appendText("- with error.code ").appendValue(expectedErrorCode)
        }

        if (expectedChecks.hasFlag(CHECK_ERROR_CAUSE)) {
            description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
            description.appendText("- with error.cause ").appendValue(expectedErrorCause)
        }
    }

    /*
     */
    override fun describeMismatchSafely(item: Result<T>, description: Description) {
        if (item.isSuccess()) {
            description.appendText("was success")
        } else {
            description.appendText("was failure")

            val error = item.getError()

            if (expectedChecks.hasFlag(CHECK_ERROR) && error != expectedError) {
                description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
                description.appendText("- had error ").appendValue(item.getError())
            }

            if (expectedChecks.hasFlag(CHECK_ERROR_CODE) && error.code() != expectedErrorCode) {
                description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
                description.appendText("- had error.code ").appendValue(error.code())
            }

            if (expectedChecks.hasFlag(CHECK_ERROR_CAUSE) && error.cause() != expectedErrorCause) {
                description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
                description.appendText("- had error.cause ").appendValue(error.cause())
            }
        }
    }
}