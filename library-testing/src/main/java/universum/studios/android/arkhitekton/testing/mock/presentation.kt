/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.arkhitekton.testing.mock

import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.view.presentation.Presenter

/**
 * Creates a new [Presenter] mock object of the given [presenterClass].
 *
 * @param presenterClass The desired presenter class to create mock of.
 * @param P Type of the presenter.
 * @return New mock instance ready to be used.
 */
fun <P : Presenter<*>> mockPresenter(presenterClass: Class<P>): P =
    mock(presenterClass)