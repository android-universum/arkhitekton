/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.mock.kotlin

import org.mockito.Mockito

/**
 * Utility class providing *Kotlin* specific, or rather compatible, argument matchers.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
internal object KotlinArgumentMatchers {

    /**
     * Delegates to [Mockito.any] and returns uninitialized instance of the desired type.
     *
     * @param T Accepted type.
     * @return Uninitialized instance of the type.
     */
    fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    /**
     * Delegates to [Mockito.any] and returns uninitialized instance of the desired type.
     *
     * @param type Class of the accepted type.
     * @param T Accepted type.
     * @return Uninitialized instance of the type.
     */
    fun <T> any(type: Class<T>): T {
        Mockito.any<T>(type)
        return uninitialized()
    }

    /**
     * Returns uninitialized instance of the desired type.
     *
     * @return Uninitialized instance of the type.
     */
    @Suppress("UNCHECKED_CAST")
    private fun <T> uninitialized(): T = null as T
}