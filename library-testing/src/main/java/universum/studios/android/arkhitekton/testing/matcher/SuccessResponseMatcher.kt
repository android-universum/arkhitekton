/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import universum.studios.android.arkhitekton.interaction.Request
import universum.studios.android.arkhitekton.interaction.Response
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.arkhitekton.util.addFlag
import universum.studios.android.arkhitekton.util.hasFlag

/**
 * A [Matcher] that tests whether the examined object is [success][Response.isSuccess] response.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see FailureResponseMatcher
 */
class SuccessResponseMatcher<T> : TypeSafeMatcher<Response<T>>(Response::class.java) {

    /**
     */
    companion object {

        /**
         * Flag indicating that request should be checked as part of [matches] implementation.
         */
        private const val CHECK_REQUEST = 1

        /**
         * Flag indicating that response value should be checked as part of [matches] implementation.
         */
        private const val CHECK_VALUE = 1 shl 1

        /**
         * Creates a matcher that matches if examined object is [success][Response.isSuccess] response.
         *
         * For example:
         *
         * `assertThat(response, is(successResponse())`
         */
        @JvmStatic fun <T> success(): SuccessResponseMatcher<T> = SuccessResponseMatcher()
    }

    /**
     * Flags determining what checks should be performed as part of [matches] implementation.
     */
    private var expectedChecks: Int = 0

    /**
     * Request that the examined response is expected to have.
     */
    private var expectedRequest: Request? = null

    /**
     * Result value that the examined response is expected to have.
     */
    private var expectedValue: Any? = null

    /**
     * Adds check requesting this matcher to match if examined response has the specified [request].
     *
     * @param request The desired request to expect.
     * @return This matcher to allow methods chaining.
     *
     * @see Response.getRequest
     */
    fun forRequest(request: Request): SuccessResponseMatcher<T> {
        this.expectedChecks = expectedChecks.addFlag(CHECK_REQUEST)
        this.expectedRequest = request
        return this
    }

    /**
     * Adds check requesting this matcher to match if examined response has [empty][Value.EMPTY] value.
     *
     * @return This matcher to allow methods chaining.
     *
     * @see withValue
     * @see Response.getValue
     */
    fun withEmptyValue(): SuccessResponseMatcher<T> = withValue(Value.EMPTY)

    /**
     * Adds check requesting this matcher to match if examined response has the specified [value].
     *
     * @param value The desired value to expect.
     * @return This matcher to allow methods chaining.
     *
     * @see Response.getValue
     */
    fun withValue(value: Any): SuccessResponseMatcher<T> {
        this.expectedChecks = expectedChecks.addFlag(CHECK_VALUE)
        this.expectedValue = value
        return this
    }

    /*
     */
    override fun matchesSafely(item: Response<T>): Boolean {
        var matches = item.isSuccess()

        if (expectedChecks.hasFlag(CHECK_REQUEST) && item.getRequest<Request>() != expectedRequest) {
            matches = false
        }

        if (expectedChecks.hasFlag(CHECK_VALUE) && item.getValue() != expectedValue) {
            matches = false
        }

        return matches
    }

    /*
     */
    override fun describeTo(description: Description) {
        description.appendText("success")

        if (expectedChecks.hasFlag(CHECK_REQUEST)) {
            description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
            description.appendText("- for request ").appendValue(expectedRequest)
        }

        if (expectedChecks.hasFlag(CHECK_VALUE)) {
            description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
            description.appendText("- with value ").appendValue(expectedValue)
        }
    }

    /*
     */
    override fun describeMismatchSafely(item: Response<T>, description: Description) {
        if (item.isFailure()) {
            description.appendText("was failure")
        } else {
            description.appendText("was success")

            if (expectedChecks.hasFlag(CHECK_REQUEST) && item.getRequest<Request>() != expectedRequest) {
                description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
                description.appendText("- for different request ").appendValue(item.getRequest())
            }

            val value = item.getValue()

            if (expectedChecks.hasFlag(CHECK_VALUE) && value != expectedValue) {
                description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
                description.appendText("- had value ").appendValue(value)
            }
        }
    }
}