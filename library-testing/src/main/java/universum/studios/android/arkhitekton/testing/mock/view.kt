/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.arkhitekton.testing.mock

import androidx.lifecycle.Lifecycle
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewModel

/**
 * Creates a new [View] mock object of the given [viewClass] in a [RESUMED][Lifecycle.State.RESUMED]
 * lifecycle state with the given [viewModel] associated.
 *
 * @param viewClass The desired view class to create mock of.
 * @param viewModel The desired view model to be available via [View.getViewModel] (if `not null`).
 * @param V Type of the view.
 * @param VM Type of the view model to be associated with the view.
 * @return New mock instance ready to be used.
 */
fun <V : View<VM>, VM : ViewModel> mockResumedView(viewClass: Class<V>, viewModel: VM? = null): V =
    mockView(viewClass, viewModel, Lifecycle.State.RESUMED)

/**
 * Creates a new [View] mock object of the given [viewClass] in the given lifecycle [state] and with
 * the given [viewModel] associated.
 *
 * @param viewClass The desired view class to create mock of.
 * @param viewModel The desired view model to be available via [View.getViewModel] (if `not null`).
 * @param V Type of the view.
 * @param VM Type of the view model to be associated with the view.
 * @return New mock instance ready to be used.
 */
fun <V : View<VM>, VM : ViewModel> mockView(
    viewClass: Class<V>,
    viewModel: VM? = null,
    state: Lifecycle.State = Lifecycle.State.INITIALIZED
): V {
    val view = mock(viewClass)
    viewModel?.let { `when`(view.getViewModel()).thenReturn(it) }
    val lifecycle = mock(Lifecycle::class.java)
    `when`(lifecycle.currentState).thenReturn(state)
    `when`(view.lifecycle).thenReturn(lifecycle)
    return view
}