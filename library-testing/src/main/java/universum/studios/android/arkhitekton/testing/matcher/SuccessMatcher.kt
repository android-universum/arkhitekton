/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import universum.studios.android.arkhitekton.interaction.Result
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.arkhitekton.util.addFlag
import universum.studios.android.arkhitekton.util.hasFlag

/**
 * A [Matcher] that tests whether the examined object is [success][Result.isSuccess] result.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @see FailureMatcher
 */
class SuccessMatcher<T> : TypeSafeMatcher<Result<T>>(Result::class.java) {

    /**
     */
    companion object {

        /**
         * Flag indicating that result value should be checked as part of [matches] implementation.
         */
        private const val CHECK_VALUE = 1

        /**
         * Creates a matcher that matches if examined object is [success][Result.isSuccess] result.
         *
         * For example:
         *
         * `assertThat(result, is(success())`
         */
        @JvmStatic fun <T> success(): SuccessMatcher<T> = SuccessMatcher()
    }

    /**
     * Flags determining what checks should be performed as part of [matches] implementation.
     */
    private var expectedChecks: Int = 0

    /**
     * Result value that the examined result is expected to have.
     */
    private var expectedValue: Any? = null

    /**
     * Adds check requesting this matcher to match if examined result has [empty][Value.EMPTY] value.
     *
     * @return This matcher to allow methods chaining.
     *
     * @see withValue
     * @see Result.getValue
     */
    fun withEmptyValue(): SuccessMatcher<T> = withValue(Value.EMPTY)

    /**
     * Adds check requesting this matcher to match if examined result has the specified [value].
     *
     * @param value The desired value to expect.
     * @return This matcher to allow methods chaining.
     *
     * @see Result.getValue
     */
    fun withValue(value: Any): SuccessMatcher<T> {
        this.expectedChecks = expectedChecks.addFlag(CHECK_VALUE)
        this.expectedValue = value
        return this
    }

    /*
     */
    override fun matchesSafely(item: Result<T>): Boolean {
        var matches = item.isSuccess()
        if (expectedChecks.hasFlag(CHECK_VALUE) && item.getValue() != expectedValue) {
            matches = false
        }
        return matches
    }

    /*
     */
    override fun describeTo(description: Description) {
        description.appendText("success")

        if (expectedChecks.hasFlag(CHECK_VALUE)) {
            description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
            description.appendText("- with value ").appendValue(expectedValue)
        }
    }

    /*
     */
    override fun describeMismatchSafely(item: Result<T>, description: Description) {
        if (item.isFailure()) {
            description.appendText("was failure")
        } else {
            description.appendText("was success")

            val value = item.getValue()

            if (expectedChecks.hasFlag(CHECK_VALUE) && value != expectedValue) {
                description.appendText(MatcherPolicies.DESCRIPTION_NEW_LINE_ALIGNMENT_CHARS)
                description.appendText("- had value ").appendValue(value)
            }
        }
    }
}