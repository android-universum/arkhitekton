/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
/**
 * @author Martin Albedinsky
 * @since 1.0
 */
package universum.studios.android.arkhitekton.testing.mock

import org.mockito.ArgumentCaptor
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import universum.studios.android.arkhitekton.interaction.ParamRequest
import universum.studios.android.arkhitekton.interaction.Request
import universum.studios.android.arkhitekton.interaction.Response
import universum.studios.android.arkhitekton.interaction.Responses
import universum.studios.android.arkhitekton.interaction.Result
import universum.studios.android.arkhitekton.interaction.usecase.UseCase
import universum.studios.android.arkhitekton.testing.mock.kotlin.KotlinArgumentCaptor
import universum.studios.android.arkhitekton.testing.mock.kotlin.KotlinArgumentMatchers
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.Value

/**
 * Builds a new [ArgumentCaptor] that may be used to capture [ParamRequest] argument.
 *
 * @param <T> Type of param associated with the request captured by the newly built captor.
 * @return New [ParamRequest] argument captor ready to be used.
 */
@Suppress("UNCHECKED_CAST")
fun <T> paramRequestCaptor(): ArgumentCaptor<ParamRequest<T>> =
    KotlinArgumentCaptor.captor(ParamRequest::class.java) as ArgumentCaptor<ParamRequest<T>>

/**
 * Builds a new [ArgumentCaptor] that may be used to capture [Result] argument.
 *
 * @param <T> Type of value associated with the result captured by the newly built captor.
 * @return New [Result] argument captor ready to be used.
 */
@Suppress("UNCHECKED_CAST")
fun <T> resultCaptor(): ArgumentCaptor<Result<T>> =
    KotlinArgumentCaptor.captor(Result::class.java) as ArgumentCaptor<Result<T>>

/**
 * Creates a new [UseCase] mock object which when executed returns **success** [Response]
 * with the given [resultValue].
 *
 * @param resultValue The desired result value to be returned via response.
 * @param T Type of the response result value.
 * @return New mock instance ready to be used.
 */
fun <T> mockUseCase(resultValue: T): UseCase<Request, T> =
    mockUseCase(Responses.createSuccess(Request.empty(), resultValue))

/**
 * Creates a new [UseCase] mock object which when executed returns **failure** [Response]
 * with the given [error].
 *
 * @param error The desired error to be returned via response.
 * @param T Type of the response result value.
 * @return New mock instance ready to be used.
 */
fun <T> mockUseCase(error: Error): UseCase<Request, T> =
    mockUseCase(Responses.createFailure(Request.empty(), error))

/**
 * Creates a new [UseCase] mock object which when executed returns the given [response].
 *
 * @param response The desired response.
 * @param R Type of the use case request.
 * @param T Type of the response result value.
 * @return New mock instance ready to be used.
 */
@Suppress("UNCHECKED_CAST")
fun <R : Request, T> mockUseCase(response: Response<T>): UseCase<R, T> {
    val useCase = mock(UseCase::class.java) as UseCase<R, T>
    `when`(useCase.execute(KotlinArgumentMatchers.any())).thenReturn(response)
    return useCase
}

/**
 * Creates a new [UseCase] empty mock object with `execute(...)` function not being mocked.
 * That being said, execution of this use case will actually throw [UnsupportedOperationException].
 *
 * @param R Type of the use case request.
 * @param T Type of the response result value.
 * @return New empty mock instance.
 */
@Suppress("UNCHECKED_CAST")
fun <R : Request, T> mockUseCase(): UseCase<R, T> {
    val useCase = mock(UseCase::class.java) as UseCase<R, T>
    `when`(useCase.execute(KotlinArgumentMatchers.any())).thenThrow(UnsupportedOperationException("Cannot execute empty mock use case!"))
    return useCase
}

/**
 * Creates a new [UseCase] mock object of the given [useCaseClass] which when executed returns
 * **success** [Response] with the given [resultValue].
 *
 * @param useCaseClass The desired use case class to create mock of.
 * @param resultValue The desired result value to be returned via response.
 * @param U Type of the use case.
 * @param R Type of the use case request.
 * @param T Type of the response result value.
 * @return New mock instance ready to be used.
 */
fun <U : UseCase<R, T>, R : Request, T> mockUseCase(useCaseClass: Class<U>, resultValue: T): U = mockUseCase(
    useCaseClass,
    Responses.createSuccess(Request.empty(), resultValue)
)

/**
 * Creates a new [UseCase] mock object of the given [useCaseClass] which when executed returns
 * **failure** [Response] with the given [error].
 *
 * @param useCaseClass The desired use case class to create mock of.
 * @param error The desired error to be returned via response.
 * @param U Type of the use case.
 * @param R Type of the use case request.
 * @param T Type of the response result value.
 * @return New mock instance ready to be used.
 */
fun <U : UseCase<R, T>, R : Request, T> mockUseCase(useCaseClass: Class<U>, error: Error): U = mockUseCase(
    useCaseClass,
    Responses.createFailure(Request.empty(), error)
)

/**
 * Creates a new [UseCase] mock object of the given [useCaseClass] which when executed returns
 * the given [response].
 *
 * @param useCaseClass The desired use case class to create mock of.
 * @param response The desired response.
 * @param U Type of the use case.
 * @param R Type of the use case request.
 * @param T Type of the response result value.
 * @return New mock instance ready to be used.
 */
fun <U : UseCase<R, T>, R : Request, T> mockUseCase(useCaseClass: Class<U>, response: Response<T>): U {
    val useCase = mock(useCaseClass)
    `when`(useCase.execute(KotlinArgumentMatchers.any())).thenReturn(response)
    return useCase
}