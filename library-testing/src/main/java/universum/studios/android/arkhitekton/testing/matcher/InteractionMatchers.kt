/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.testing.matcher

import universum.studios.android.arkhitekton.interaction.Response
import universum.studios.android.arkhitekton.interaction.Result

/**
 * Convenience factory that may be used to create common interaction matchers.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
object InteractionMatchers {

    /**
     * Creates a matcher that matches if examined object is [success][Result.isSuccess] result.
     *
     * For example:
     *
     * `assertThat(result, is(success())`
     *
     * @see failure
     */
    fun <T> success(): SuccessMatcher<T> = SuccessMatcher.success()

    /**
     * Creates a matcher that matches if examined object is [failure][Result.isFailure] result.
     *
     * For example:
     *
     * `assertThat(result, is(failure())`
     *
     * @see success
     */
    fun <T> failure(): FailureMatcher<T> = FailureMatcher.failure()

    /**
     * Creates a matcher that matches if examined object is [success][Response.isSuccess] response.
     *
     * For example:
     *
     * `assertThat(response, is(successResponse())`
     *
     * @see failureResponse
     */
    fun <T> successResponse(): SuccessResponseMatcher<T> = SuccessResponseMatcher.success()

    /**
     * Creates a matcher that matches if examined object is [failure][Response.isFailure] response.
     *
     * For example:
     *
     * `assertThat(response, is(failureResponse())`
     *
     * @see successResponse
     */
    fun <T> failureResponse(): FailureResponseMatcher<T> = FailureResponseMatcher.failure()
}