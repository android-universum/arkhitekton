Arkhitekton-View
===============

This module contains **core declaration** of `View` and `ViewModel` elements where view acts as gateway
to user interface and view model represent state of a single view.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-view:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [View](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view/src/main/java/universum/studios/android/arkhitekton/view/View.kt)
- [ViewModel](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view/src/main/java/universum/studios/android/arkhitekton/view/ViewModel.kt)
