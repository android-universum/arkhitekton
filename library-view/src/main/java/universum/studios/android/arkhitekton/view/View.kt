/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view

import androidx.annotation.NonNull
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

/**
 * Interface that specifies base layer for views. In its basic sense a view is responsible for
 * rendering a data available via [ViewModel] to a screen and also for navigating between different
 * screens.
 *
 * Each view implementation should provide access to its current [Lifecycle] via [getLifecycle] and
 * to its associated [ViewModel] via [getViewModel].
 *
 * @param VM View model of which data will be rendered by the view.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface View<out VM : ViewModel> : LifecycleOwner {

    /*
	 * Companion ===================================================================================
	 */

    /**
     * Contract for [View] element.
     */
    companion object Contract {

        /**
         * Empty implementation (NULL object) which doesn't perform any logic.
         */
        private val EMPTY: View<*> = object : View<ViewModel> {

            /**
             * Lifecycle registry of this dummy view. Its state will never by updated.
             */
            override val lifecycle: Lifecycle = LifecycleRegistry(this)

            /*
             */
            override fun getViewModel(): ViewModel = ViewModel.empty()
        }

        /**
         * Returns an empty instance of [View] which may be used in cases where an instance of
         * concrete View implementation is not needed.
         *
         * The returned view provides [Lifecycle] which is always in [INITIALIZED][Lifecycle.State.INITIALIZED]
         * state and also [empty ViewModel][ViewModel.empty].
         *
         * @return Empty view ready to be used.
         */
        @NonNull fun empty(): View<*> = EMPTY
    }

    /*
     * Interface ===================================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the view model of which data this view renders to a screen.
     *
     * @return View model associated with this view.
     */
    @NonNull fun getViewModel(): VM
}