/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view

import androidx.annotation.NonNull

/**
 * Interface declaring layer for view models that are used across application in order to persist
 * state for [Views][View].
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
interface ViewModel {

    /**
     * Contract for [ViewModel] element.
     */
    companion object Contract {

        /**
         * Empty implementation (NULL object) which doesn't *persist* any view data.
         */
        private val EMPTY = object : ViewModel {}

        /**
         * Returns an empty instance of view model which may be used in cases when a concrete [View]
         * does not need to persist its state.
         *
         * @return Empty view model ready to be used.
         */
        @NonNull fun empty() = EMPTY
    }
}