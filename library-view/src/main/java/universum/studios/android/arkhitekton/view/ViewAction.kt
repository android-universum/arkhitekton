/*
 * *************************************************************************************************
 *                                 Copyright 2019 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view

import androidx.annotation.NonNull

/**
 * Simple action that may be performed for a single [View].
 *
 * @param V Type of the view for which will be action performed.
 *
 * @author Martin Albedinsky
 * @since 1.0
 */
@FunctionalInterface
interface ViewAction<in V : View<*>> {

    /**
     * Performs this action for the specified [view].
     *
     * @param view The view.
     */
    fun perform(@NonNull view: V)
}