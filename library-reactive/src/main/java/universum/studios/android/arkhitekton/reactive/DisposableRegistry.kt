/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * 
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.reactive

import io.reactivex.rxjava3.disposables.Disposable

/**
 * Interface for registry that can hold onto multiple [Disposable]s. Each disposable is registered
 * for its associated key via [keep] and may be later unregistered/disposed via [dispose].
 * All registered disposables may be disposed at once via [clear].
 *
 * @author Martin Albedinsky
 * @since 2.2
 */
interface DisposableRegistry {

    /**
     * Checks whether this registry holds some [Disposable]s or not.
     *
     * @return `True` if there are some disposables held, `false` otherwise.
     *
     * @see size
     */
    fun isEmpty(): Boolean

    /**
     * Returns the number of currently held [Disposable]s by this registry.
     *
     * @return The number of currently held disposables.
     *
     * @see isEmpty
     */
    fun size(): Int

    /**
     * Registers and keeps the given `disposable` in this registry for the specified `key` or disposes
     * it if this registry has been disposed.
     *
     * If this registry holds previous disposable for the same key, such disposable will be disposed
     * first before the new one is registered.
     *
     * @param key Key for which to hold the disposable.
     * @param disposable The desired disposable to hold until a new one with the same key is registered.
     * @return `True` if successful, `false` if this registry has been disposed.
     *
     * @see dispose
     * @see contains
     */
    fun keep(key: String, disposable: Disposable): Boolean

    /**
     * Checks whether the registry holds a disposable for the specified `key`.
     *
     * @param key Key of the desired disposable to check.
     * @return `True` if this registry holds such disposable, `false` if not or if this registry
     * has been disposed.
     *
     * @see keep
     * @see dispose
     */
    fun contains(key: String): Boolean

    /**
     * Same as [find] but requires that the desired disposable is held by this registry.
     *
     * @param key Key of the desired disposable to require.
     * @return The desired disposable or an exception is thrown if this registry does not hold such
     * disposable or this registry as been disposed.
     *
     * @throws IllegalStateException If this registry does not hold such disposable.
     */
    fun require(key: String): Disposable

    /**
     * Searches for a disposable held by this registry for the specified `key`.
     *
     * @param key Key of the desired disposable to find.
     * @return The desired disposable or `null` if this registry does not hold such disposable or
     * this registry has been disposed.
     *
     * @see require
     */
    fun find(key: String): Disposable?

    /**
     * Unregisters and disposes a disposable for the specified `key` from this registry.
     *
     * @param key Key of the desired disposable to unregister and dispose.
     * @return `True` if the desired disposable has been unregistered and disposed, `false` if such
     * disposable is not registered in this registry or this registry has been disposed.
     *
     * @see keep
     * @see contains
     * @see clear
     */
    fun dispose(key: String): Boolean

    /**
     * Atomically clears this registry, then disposes all the previously held [Disposable]s.
     *
     * **Note that invocation of this function may actually rise some runtime exception if some of
     * disposables fails to be disposed.**
     *
     * @return `True` if all disposables have been successfully disposed, `false` if this registry
     * is empty or it has been disposed.
     *
     * @see dispose
     */
    fun clear(): Boolean
}