/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.reactive

import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.exceptions.CompositeException
import io.reactivex.rxjava3.exceptions.Exceptions
import io.reactivex.rxjava3.internal.util.ExceptionHelper

/**
 * A disposable registry that can hold onto multiple other [Disposable]s and offers `O(1)` time
 * complexity for [keep(String, Disposable)][keep] and [dispose(String)] operations.
 *
 * @author Martin Albedinsky
 * @since 2.2
 */
class CompositeDisposableRegistry : Disposable, DisposableRegistry {

    /**
     * Map containing all disposable resources registered in this registry via [keep]. Each disposable
     * resource may be identified and found in the map by its associated key.
     */
    private var resources: MutableMap<String, Disposable>? = null

    /**
     * Flag indicating whether this composite registry is disposed or not. If disposed, this registry
     * no longer accepts any new disposables.
     *
     * @see dispose
     */
    @Volatile private var disposed = false

    /*
     */
    override fun dispose() {
        if (disposed) {
            return
        }

        val map: Map<String, Disposable>?

        synchronized(this) {
            if (disposed) {
                return
            }

            disposed = true
            map = resources
            resources = null
        }

        dispose(map)
    }

    /*
     */
    override fun isDisposed(): Boolean = disposed

    /*
     */
    override fun isEmpty(): Boolean = size() == 0

    /*
     */
    override fun size(): Int {
        if (disposed) {
            return 0
        }

        synchronized(this) {
            if (disposed) {
                return 0
            }

            val map = resources

            return map?.size ?: 0
        }
    }

    /*
     */
    override fun keep(key: String, disposable: Disposable): Boolean {
        if (!disposed) {
            dispose(key)

            synchronized(this) {
                if (!disposed) {
                    var map = resources
                    if (map == null) {
                        map = mutableMapOf()
                        resources = map
                    }

                    map[key] = disposable

                    return true
                }
            }
        }

        disposable.dispose()

        return false
    }

    /*
     */
    override fun contains(key: String): Boolean {
        if (disposed) {
            return false
        }

        synchronized(this) {
            if (disposed) {
                return false
            }

            val map = resources

            return map?.contains(key) == true
        }
    }

    /*
     */
    override fun require(key: String): Disposable = checkNotNull(find(key)) { "No disposable found for the given key '$key'." }

    /*
     */
    override fun find(key: String): Disposable? {
        if (disposed) {
            return null
        }

        synchronized(this) {
            if (disposed) {
                return null
            }

            val map = resources

            return map?.get(key)
        }
    }

    /*
     */
    override fun dispose(key: String): Boolean {
        if (disposed) {
            return false
        }

        synchronized(this) {
            if (disposed) {
                return false
            }

            val map = resources

            val disposable = map?.remove(key) ?: return false
            disposable.dispose()
        }

        return true
    }

    /*
     */
    override fun clear(): Boolean {
        if (disposed) {
            return false
        }

        val map: Map<String, Disposable>?

        synchronized(this) {
            if (disposed) {
                return false
            }

            map = resources
            resources = null
        }

        return dispose(map)
    }

    /**
     * Disposes all disposable resources contained in the given `map`.
     *
     * If the given map is `null` or **empty**, this function does nothing and returns `false`.
     *
     * @param map The map with disposable resources to dispose. May be `null`.
     * @return `True` if all disposable resources in the map have been successfully disposed, `false`
     * if the given map is `null` or **empty**.
     *
     * @throws RuntimeException If exactly one disposable resource failed to be disposed
     * (raised exception while its [Disposable.dispose] function has been invoked).
     * @throws CompositeException If at least one disposable resource failed to be disposed
     * (raised exception while its [Disposable.dispose] function has been invoked).
     */
    private fun dispose(map: Map<String, Disposable>?): Boolean {
        if (map.isNullOrEmpty()) {
            return false
        }

        val errors = mutableListOf<Throwable>()
        val disposables = map.values

        disposables.forEach { disposable ->
            try {
                disposable.dispose()
            } catch (e: Throwable) {
                Exceptions.throwIfFatal(e)
                errors.add(e)
            }
        }

        if (errors.isNotEmpty()) {
            if (errors.size == 1) {
                throw ExceptionHelper.wrapOrThrow(errors.first())
            }

            throw CompositeException(errors)
        }

        return true
    }
}