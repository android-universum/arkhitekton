/*
 * *************************************************************************************************
 *                                 Copyright 2020 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.reactive

import io.reactivex.rxjava3.disposables.Disposable
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class CompositeDisposableRegistryTest : TestCase {

    @Test fun defaultInstance() {
        // Arrange:
        val registry = CompositeDisposableRegistry()

        // Assert:
        assertFalse(registry.isDisposed)
        assertTrue(registry.isEmpty())
        assertThat(registry.size(), `is`(0))
    }

    @Test fun dispose() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)
        registry.keep("first", mockDisposableFirst)
        registry.keep("second", mockDisposableSecond)

        // Act:
        registry.dispose()

        // Assert:
        assertTrue(registry.isDisposed)
        assertTrue(registry.isEmpty())
        assertThat(registry.size(), `is`(0))
        verify(mockDisposableFirst).dispose()
        verify(mockDisposableSecond).dispose()
    }

    @Test fun disposeWhenEmpty() {
        // Arrange:
        val registry = CompositeDisposableRegistry()

        // Act:
        registry.dispose()

        // Assert:
        assertTrue(registry.isDisposed)
    }

    @Test fun disposeWhenAlreadyDisposed() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        registry.dispose()

        // Act:
        registry.dispose()

        // Assert:
        assertTrue(registry.isDisposed)
    }

    @Test fun keep() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)

        // Act + Assert:

        // 1)
        assertTrue(registry.keep("first", mockDisposableFirst))
        assertFalse(registry.isEmpty())
        assertThat(registry.size(), `is`(1))
        assertTrue(registry.contains("first"))

        // 2)
        assertTrue(registry.keep("second", mockDisposableSecond))
        assertFalse(registry.isEmpty())
        assertThat(registry.size(), `is`(2))
        assertTrue(registry.contains("first"))
        assertTrue(registry.contains("second"))

        verifyNoInteractions(mockDisposableFirst, mockDisposableSecond)
    }

    @Test fun keepSubsequent() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)
        registry.keep("first", mockDisposableFirst)
        registry.keep("second", mockDisposableSecond)
        val mockDisposableFirstSubsequent = mock(Disposable::class.java)
        val mockDisposableSecondSubsequent = mock(Disposable::class.java)

        // Act + Assert:

        // 1)
        assertTrue(registry.keep("first", mockDisposableFirstSubsequent))
        assertFalse(registry.isEmpty())
        assertThat(registry.size(), `is`(2))
        assertTrue(registry.contains("first"))
        assertTrue(registry.contains("second"))
        verify(mockDisposableFirst).dispose()

        // 2)
        assertTrue(registry.keep("second", mockDisposableSecondSubsequent))
        assertFalse(registry.isEmpty())
        assertThat(registry.size(), `is`(2))
        assertTrue(registry.contains("first"))
        assertTrue(registry.contains("second"))
        verify(mockDisposableSecond).dispose()

        verifyNoMoreInteractions(mockDisposableFirst, mockDisposableSecond)
        verifyNoInteractions(mockDisposableFirstSubsequent, mockDisposableSecondSubsequent)
    }

    @Test fun keepWhenDisposed() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)
        registry.dispose()

        // Act + Assert:
        assertFalse(registry.keep("first", mockDisposableFirst))
        assertFalse(registry.keep("second", mockDisposableSecond))
        assertTrue(registry.isEmpty())
        verify(mockDisposableFirst).dispose()
        verify(mockDisposableSecond).dispose()
        verifyNoMoreInteractions(mockDisposableFirst, mockDisposableSecond)
    }

    @Test fun disposeResource() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)
        registry.keep("first", mockDisposableFirst)
        registry.keep("second", mockDisposableSecond)

        // Act + Assert:

        // 1)
        assertTrue(registry.dispose("first"))
        assertFalse(registry.isEmpty())
        assertThat(registry.size(), `is`(1))
        assertFalse(registry.contains("first"))
        assertTrue(registry.contains("second"))
        verify(mockDisposableFirst).dispose()

        // 2)
        assertTrue(registry.dispose("second"))
        assertTrue(registry.isEmpty())
        assertThat(registry.size(), `is`(0))
        assertFalse(registry.contains("second"))
        verify(mockDisposableSecond).dispose()

        // 3)
        assertFalse(registry.dispose("third"))

        verifyNoMoreInteractions(mockDisposableFirst, mockDisposableSecond)
    }

    @Test fun disposeResourceWhenDisposed() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        registry.keep("first", mock(Disposable::class.java))
        registry.keep("second", mock(Disposable::class.java))
        registry.dispose()

        // Act + Assert:
        assertFalse(registry.dispose("first"))
        assertFalse(registry.dispose("second"))
    }

    @Test fun find() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)

        // Act + Assert:

        // 1)
        assertThat(registry.find("first"), `is`(nullValue()))
        assertThat(registry.find("second"), `is`(nullValue()))

        // 2)
        registry.keep("first", mockDisposableFirst)
        assertThat(registry.find("first"), `is`(mockDisposableFirst))
        assertThat(registry.require("first"), `is`(mockDisposableFirst))
        assertThat(registry.find("second"), `is`(nullValue()))

        // 3)
        registry.keep("second", mockDisposableSecond)
        assertThat(registry.find("first"), `is`(mockDisposableFirst))
        assertThat(registry.require("first"), `is`(mockDisposableFirst))
        assertThat(registry.find("second"), `is`(mockDisposableSecond))
        assertThat(registry.require("second"), `is`(mockDisposableSecond))
    }

    @Test fun findWhenDisposed() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)
        registry.keep("first", mockDisposableFirst)
        registry.keep("second", mockDisposableSecond)
        registry.dispose()

        // Act + Assert:
        assertThat(registry.find("first"), `is`(nullValue()))
        assertThat(registry.find("second"), `is`(nullValue()))
    }

    @Test(expected = IllegalStateException::class)
    fun requireWhenEmpty() {
        // Arrange:
        val registry = CompositeDisposableRegistry()

        // Act:
        registry.require("first")
    }

    @Test fun clear() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        val mockDisposableFirst = mock(Disposable::class.java)
        val mockDisposableSecond = mock(Disposable::class.java)

        // Act + Assert:

        // 1)
        assertTrue(registry.keep("first", mockDisposableFirst))
        assertTrue(registry.clear())
        assertFalse(registry.contains("first"))
        assertTrue(registry.isEmpty())
        assertFalse(registry.isDisposed)
        verify(mockDisposableFirst).dispose()

        // 2)
        assertTrue(registry.keep("second", mockDisposableSecond))
        assertTrue(registry.clear())
        assertFalse(registry.contains("first"))
        assertFalse(registry.contains("second"))
        assertTrue(registry.isEmpty())
        assertFalse(registry.isDisposed)
        verify(mockDisposableSecond).dispose()

        // 3)
        assertFalse(registry.clear())
        assertFalse(registry.isDisposed)

        verifyNoMoreInteractions(mockDisposableFirst, mockDisposableSecond)
    }

    @Test fun clearWhenEmpty() {
        // Arrange:
        val registry = CompositeDisposableRegistry()

        // Act + Assert:
        assertFalse(registry.clear())
    }

    @Test fun clearWhenDisposed() {
        // Arrange:
        val registry = CompositeDisposableRegistry()
        registry.dispose()

        // Act + Assert:
        assertFalse(registry.clear())
        assertFalse(registry.clear())
        assertTrue(registry.isDisposed)
    }
}