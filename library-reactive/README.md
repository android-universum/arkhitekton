Arkhitekton-Reactive
===============

This module contains common **reactive** architecture elements used in reactive modules.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-reactive:${DESIRED_VERSION}@aar"

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [DisposableRegistry](https://bitbucket.org/android-universum/arkhitekton/src/main/library-reactive/src/main/java/universum/studios/android/arkhitekton/reactive/DisposableRegistry.kt)
- [CompositeDisposableRegistry](https://bitbucket.org/android-universum/arkhitekton/src/main/library-reactive/src/main/java/universum/studios/android/arkhitekton/reactive/CompositeDisposableRegistry.kt)
