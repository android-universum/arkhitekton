Modules
===============

Library is also distributed via **separate modules** which may be downloaded as standalone parts of
the library in order to decrease dependencies count in Android projects, so only dependencies really
needed in an Android project are included. **However** some modules may depend on another modules
from this library or on modules from other libraries.

## Download ##

### Gradle ###

For **successful resolving** of artifacts for separate modules via **Gradle** add the following snippet
into **build.gradle** script of your desired Android project and use `implementation '...'` declaration
as usually.

    repositories {
        maven {
            url  "http://dl.bintray.com/universum-studios/android"
        }
    }

## Available modules ##
> Following modules are available in the [latest](https://bitbucket.org/android-universum/arkhitekton/downloads "Downloads page") stable release.

- **[Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core)**
- **[Reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-reactive)**
- **[View](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)**
- **[@Presentation](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation_group)**
- **[Presentation-Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-core)**
- **[Presentation-Base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-base)**
- **[Presentation-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-reactive)**
- **[@Control](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control_group)**
- **[Control-Core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-core)**
- **[Control-Base](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-base)**
- **[Control-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/main/library-control-reactive)**
- **[@Interaction](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction_group)**
- **[Interaction-Core](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction-core)**
- **[Interaction-Base](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction-base)**
- **[Interaction-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/master/library-interaction-reactive)**
- **[@Data](https://bitbucket.org/android-universum/arkhitekton/src/master/library-data_group)**
- **[Data-Core](https://bitbucket.org/android-universum/arkhitekton/src/master/library-data-core)**
- **[Data-Source](https://bitbucket.org/android-universum/arkhitekton/src/master/library-data-source)**
- **[Data-Reactive](https://bitbucket.org/android-universum/arkhitekton/src/master/library-data-reactive)**
- **[Handler](https://bitbucket.org/android-universum/arkhitekton/src/master/library-handler)**
- **[Util](https://bitbucket.org/android-universum/arkhitekton/src/master/library-util)**
- **[Testing](https://bitbucket.org/android-universum/arkhitekton/src/master/library-testing)**
