Android Arkhitekton
===============

[![CircleCI](https://circleci.com/bb/android-universum/arkhitekton.svg?style=shield)](https://circleci.com/bb/android-universum/arkhitekton)
[![Codecov](https://codecov.io/bb/android-universum/arkhitekton/graph/badge.svg?token=QoD4acM4mT)](https://codecov.io/bb/android-universum/arkhitekton)
[![Codacy](https://app.codacy.com/project/badge/Grade/5b57ac90696b48afa607188cd97cc511)](https://app.codacy.com/bb/android-universum/arkhitekton/dashboard?utm_source=bb&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Android](https://img.shields.io/badge/android-14.0-blue.svg)](https://developer.android.com/about/versions/14)
[![Kotlin](https://img.shields.io/badge/kotlin-1.9.10-blue.svg)](http://kotlinlang.org)
[![RxJava](https://img.shields.io/badge/rxjava-3.1.8-blue.svg)](https://github.com/ReactiveX/RxJava)
[![Robolectric](https://img.shields.io/badge/robolectric-4.5.1-blue.svg)](http://robolectric.org)
[![Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

Architectural components designed in accordance to Clean Architecture principles for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/arkhitekton/wiki)**.

## Download ##
[![Maven](https://img.shields.io/maven-central/v/io.bitbucket.android-universum/arkhitekton)](https://search.maven.org/artifact/io.bitbucket.android-universum/arkhitekton)

Download the latest **[artifacts](https://bitbucket.org/android-universum/arkhitekton/downloads "Downloads page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "io.bitbucket.android-universum:arkhitekton:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/arkhitekton/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.lifecycle:lifecycle-runtime`](https://developer.android.com/jetpack/androidx)
- [`androidx.lifecycle:lifecycle-viewmodel`](https://developer.android.com/jetpack/androidx)
- [`io.reactivex.rxjava2:rxjava`](https://github.com/ReactiveX/RxJava)
- [`io.reactivex.rxjava2:rxandroid`](https://github.com/ReactiveX/RxAndroid)
- [`io.bitbucket.android-universum:logger`](https://bitbucket.org/android-universum/logger)

## [License](https://bitbucket.org/android-universum/arkhitekton/src/main/LICENSE.md) ##

**Copyright 2024 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.

See the License for the specific language governing permissions and limitations under the License.
