Arkhitekton-Presentation-Base
===============

This module contains **base implementation** of `Presenter` which may be used as base for specific presenter implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-presentation-base:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-view](https://bitbucket.org/android-universum/arkhitekton/src/main/library-view)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [BasePresenter](https://bitbucket.org/android-universum/arkhitekton/src/main/library-presentation-base/src/main/java/universum/studios/android/arkhitekton/view/presentation/BasePresenter.kt)
