/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view.presentation

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.clearInvocations
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoInteractions
import org.mockito.Mockito.verifyNoMoreInteractions
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewAction
import universum.studios.android.arkhitekton.view.ViewModel
import universum.studios.android.testing.AndroidTestCase
import universum.studios.android.testing.kotlin.KotlinArgumentMatchers

/**
 * @author Martin Albedinsky
 */
class BasePresenterTest : AndroidTestCase() {

    @Test fun instantiation() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)

        // Act:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()

        // Assert:
        assertThat(presenter.callGetViewModel(), `is`(mockViewModel))
        assertFalse(presenter.callIsViewAttached())
        assertFalse(presenter.callIsDestroyed())
        verifyNoInteractions(mockViewModel)
    }

    @Test fun instantiationViaSimpleBuilder() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)

        // Act:
        val presenter = TestPresenter.create(mockViewModel)

        // Assert:
        assertThat(presenter.callGetViewModel(), `is`(mockViewModel))
        assertFalse(presenter.callIsViewAttached())
        assertFalse(presenter.callIsDestroyed())
        verifyNoInteractions(mockViewModel)
    }

    @Test fun name() {
        // Arrange:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()

        // Act + Assert:
        assertThat(presenter.name(), `is`("TestPresenter"))
    }

    @Test fun attachView() {
        // Arrange:
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()

        // Act + Assert:
        presenter.attachView(mockView)
        assertTrue(presenter.callIsViewAttached())
        assertThat(presenter.callGetView(), `is`(mockView))
        assertThat(presenter.callGetViewLifecycleCurrentState(), `is`(Lifecycle.State.RESUMED))
        verify(mockView).lifecycle
        verify(mockLifecycle).currentState
        verifyNoMoreInteractions(mockView, mockLifecycle)
    }

    @Test fun attachViewForLifecycleObserverPresenter() {
        // Arrange:
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()

        // Act + Assert:
        presenter.attachView(mockView)
        assertTrue(presenter.callIsViewAttached())
        assertThat(presenter.callGetView(), `is`(mockView))
        verify(mockView).lifecycle
        verify(mockLifecycle, times(2)).addObserver(KotlinArgumentMatchers.any(LifecycleObserver::class.java))
        verify(mockLifecycle).addObserver(presenter)
        verifyNoMoreInteractions(mockView, mockLifecycle)
    }

    @Test(expected = IllegalStateException::class)
    fun viewWhenNotAttached() {
        // Arrange:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()

        // Act:
        presenter.callGetView()
    }

    @Test fun performInteractiveViewRunnableActionForViewThatIsInteractive() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformInteractiveViewAction(viewActionFirst)
        presenter.callPerformInteractiveViewAction(viewActionSecond)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionSecond.wasPerformedOnce())
    }

    @Test fun performInteractiveViewRunnableActionForViewThatIsNotInteractive() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.STARTED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformInteractiveViewAction(viewActionFirst)
        presenter.callPerformInteractiveViewAction(viewActionSecond)

        // Assert:
        assertFalse(viewActionFirst.wasPerformed())
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun performVisibleViewRunnableActionForViewThatIsVisible() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.STARTED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformVisibleViewAction(viewActionFirst)
        presenter.callPerformVisibleViewAction(viewActionSecond)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionSecond.wasPerformedOnce())
    }

    @Test fun performVisibleViewRunnableActionForViewThatIsNotVisible() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.CREATED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformVisibleViewAction(viewActionFirst)
        presenter.callPerformVisibleViewAction(viewActionSecond)

        // Assert:
        assertFalse(viewActionFirst.wasPerformed())
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun performViewRunnableActionWithoutStateCondition() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst)
        presenter.callPerformViewAction(viewActionSecond)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionSecond.wasPerformedOnce())
    }

    @Test fun performViewRunnableActionForViewThatIsCreated() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.CREATED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst, stateCondition = Lifecycle.State.CREATED)
        presenter.callPerformViewAction(viewActionSecond, stateCondition = Lifecycle.State.RESUMED)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun performViewActionWithoutStateCondition() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst)
        presenter.callPerformViewAction(viewActionSecond)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertTrue(viewActionSecond.wasPerformedOnce())
        assertTrue(viewActionSecond.wasPerformedForView(mockView))
    }

    @Test fun performViewActionForViewThatIsCreated() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.CREATED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst, stateCondition = Lifecycle.State.CREATED)
        presenter.callPerformViewAction(viewActionSecond, stateCondition = Lifecycle.State.RESUMED)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun performViewActionForViewThatIsStarted() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.STARTED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst, stateCondition = Lifecycle.State.STARTED)
        presenter.callPerformViewAction(viewActionSecond, stateCondition = Lifecycle.State.RESUMED)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun performViewActionForViewThatIsResumed() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst, stateCondition = Lifecycle.State.RESUMED)
        presenter.callPerformViewAction(viewActionSecond, stateCondition = Lifecycle.State.RESUMED)

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertTrue(viewActionSecond.wasPerformedOnce())
        assertTrue(viewActionSecond.wasPerformedForView(mockView))
    }

    @Test fun performViewActionForViewThatIsDestroyed() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.DESTROYED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act:
        presenter.callPerformViewAction(viewActionFirst, stateCondition = Lifecycle.State.RESUMED)
        presenter.callPerformViewAction(viewActionSecond, stateCondition = Lifecycle.State.RESUMED)

        // Assert:
        assertFalse(viewActionFirst.wasPerformed())
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewActionForEventOnCreate() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_CREATE, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.CREATED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.CREATED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewRunnableActionForEventOnCreate() {
        // Arrange:
        val viewActionFirst = TestRunnableAction()
        val viewActionSecond = TestRunnableAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_CREATE, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.CREATED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.CREATED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewActionForEventOnStart() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        lifecycle.currentState = Lifecycle.State.CREATED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_START, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.STARTED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.STARTED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewActionForEventOnResume() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        lifecycle.currentState = Lifecycle.State.CREATED
        lifecycle.currentState = Lifecycle.State.STARTED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.RESUMED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.RESUMED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertTrue(viewActionSecond.wasPerformedOnce())
        assertTrue(viewActionSecond.wasPerformedForView(mockView))
    }

    @Test fun registerViewActionForEventOnPause() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        lifecycle.currentState = Lifecycle.State.CREATED
        lifecycle.currentState = Lifecycle.State.STARTED
        lifecycle.currentState = Lifecycle.State.RESUMED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_PAUSE, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.STARTED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.STARTED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewActionForEventOnStop() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        lifecycle.currentState = Lifecycle.State.CREATED
        lifecycle.currentState = Lifecycle.State.STARTED
        lifecycle.currentState = Lifecycle.State.RESUMED
        lifecycle.currentState = Lifecycle.State.STARTED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_STOP, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.CREATED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.CREATED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewActionForEventOnDestroy() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        lifecycle.currentState = Lifecycle.State.CREATED
        lifecycle.currentState = Lifecycle.State.STARTED
        lifecycle.currentState = Lifecycle.State.RESUMED
        lifecycle.currentState = Lifecycle.State.STARTED
        lifecycle.currentState = Lifecycle.State.CREATED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_DESTROY, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.DESTROYED

        // Ensure that entering the same state again does not trigger actions again because they should be already cleared.
        lifecycle.currentState = Lifecycle.State.DESTROYED
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun registerViewActionMultipleTimes() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act + Assert:
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_CREATE, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_CREATE, viewActionFirst), `is`(notNullValue()))
        assertThat(presenter.callRegisterViewAction(Lifecycle.Event.ON_RESUME, viewActionSecond), `is`(notNullValue()))
        lifecycle.currentState = Lifecycle.State.CREATED

        // Assert:
        assertThat(viewActionFirst.performedTimes(), `is`(2))
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun cancelRegisteredViewAction() {
        // Arrange:
        val viewActionFirst = TestViewAction()
        val viewActionSecond = TestViewAction()
        val mockView = mock(TestView::class.java)
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.currentState = Lifecycle.State.INITIALIZED
        `when`(mockView.lifecycle).thenReturn(lifecycle)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView)

        // Act:
        presenter.callRegisterViewAction(Lifecycle.Event.ON_CREATE, viewActionFirst)
        presenter.callRegisterViewAction(Lifecycle.Event.ON_CREATE, viewActionSecond).cancel()
        lifecycle.currentState = Lifecycle.State.CREATED

        // Assert:
        assertTrue(viewActionFirst.wasPerformedOnce())
        assertTrue(viewActionFirst.wasPerformedForView(mockView))
        assertFalse(viewActionSecond.wasPerformed())
    }

    @Test fun detachView() {
        // Arrange:
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act + Assert:
        presenter.detachView(mockView)
        verifyNoInteractions(mockView)
    }

    @Test fun detachViewForLifecycleObserverPresenter() {
        // Arrange:
        val mockView = mock(TestView::class.java)
        val mockLifecycle = mock(Lifecycle::class.java)
        `when`(mockView.lifecycle).thenReturn(mockLifecycle)
        `when`(mockLifecycle.currentState).thenReturn(Lifecycle.State.RESUMED)
        val presenter = TestLifecycleObserverPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()
        presenter.attachView(mockView)
        clearInvocations(mockView, mockLifecycle)

        // Act + Assert:
        presenter.detachView(mockView)
        verify(mockView).lifecycle
        verify(mockLifecycle, times(2)).removeObserver(KotlinArgumentMatchers.any(LifecycleObserver::class.java))
        verify(mockLifecycle).removeObserver(presenter)
        verifyNoMoreInteractions(mockView, mockLifecycle)
    }

    @Test fun destroy() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()

        // Act:
        presenter.destroy()

        // Assert:
        assertTrue(presenter.callIsDestroyed())
    }

    @Test fun destroySubsequent() {
        // Arrange:
        val mockViewModel = mock(ViewModel::class.java)
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = mockViewModel
        }.build()

        // Act:
        presenter.destroy()
        presenter.destroy()

        // Assert:
        assertTrue(presenter.callIsDestroyed())
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val presenter = TestPresenter.TestBuilder().apply {
            this.viewModel = ViewModel.empty()
        }.build()

        // Act + Assert:
        assertThat(presenter.toString(), `is`("TestPresenter(viewAttached: false, destroyed: false)"))
    }

    private interface TestView : View<ViewModel>

    private class TestViewAction : ViewAction<TestView> {

        private var performedTimes = 0
        private var performCalledWithView: TestView? = null

        override fun perform(view: TestView) {
            performedTimes++
            performCalledWithView = view
        }

        fun performedTimes(): Int = performedTimes

        fun wasPerformed(): Boolean = performedTimes > 0

        fun wasPerformedOnce(): Boolean = performedTimes == 1

        fun wasPerformedForView(view: TestView): Boolean = performCalledWithView == view
    }

    private class TestRunnableAction : Runnable {

        private var performedTimes = 0

        override fun run() {
            performedTimes++
        }

        fun wasPerformed(): Boolean = performedTimes > 0

        fun wasPerformedOnce(): Boolean = performedTimes == 1
    }

    private abstract class TestBasePresenter(builder: BaseBuilder<*, TestView, ViewModel>) :
        BasePresenter<TestView, ViewModel>(builder) {

        fun callGetViewModel() = super.getViewModel()

        fun callGetView() = super.getView()

        fun callGetViewLifecycleCurrentState() = super.getViewLifecycleCurrentState()

        fun callIsViewAttached() = isViewAttached()

        fun callIsDestroyed(): Boolean = isDestroyed()
    }

    private class TestPresenter(builder: BaseBuilder<*, TestView, ViewModel>) : TestBasePresenter(builder) {

        companion object {

            fun create(viewModel: ViewModel) = TestPresenter(SimpleBuilder.create(viewModel))
        }

        fun callPerformInteractiveViewAction(action: Runnable) = performInteractiveViewAction(action)

        fun callPerformVisibleViewAction(action: Runnable) = performVisibleViewAction(action)

        fun callPerformViewAction(action: Runnable) = performViewAction(action)

        fun callPerformViewAction(action: Runnable, stateCondition: Lifecycle.State) = performViewAction(action, stateCondition)

        fun callPerformViewAction(action: ViewAction<TestView>) = performViewAction(action)

        fun callPerformViewAction(action: ViewAction<TestView>, stateCondition: Lifecycle.State) =
            performViewAction(action, stateCondition)

        class TestBuilder : BasePresenter.BaseBuilder<TestBuilder, TestView, ViewModel>() {

            override val self = this
            override fun build() = TestPresenter(this)
        }
    }

    private class TestLifecycleObserverPresenter(builder: BaseBuilder<*, TestView, ViewModel>) : TestBasePresenter(builder),
        LifecycleObserver {

        fun callRegisterViewAction(event: Lifecycle.Event, action: Runnable) = registerViewAction(event, action)
        
        fun callRegisterViewAction(event: Lifecycle.Event, action: ViewAction<TestView>) = registerViewAction(event, action)

        class TestBuilder : BasePresenter.BaseBuilder<TestBuilder, TestView, ViewModel>() {

            override val self = this
            override fun build() = TestLifecycleObserverPresenter(this)
        }
    }
}