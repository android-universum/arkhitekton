/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.view.presentation

import androidx.annotation.NonNull
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import universum.studios.android.arkhitekton.ArkhitektonLogging
import universum.studios.android.arkhitekton.view.View
import universum.studios.android.arkhitekton.view.ViewAction
import universum.studios.android.arkhitekton.view.ViewModel
import java.util.concurrent.atomic.AtomicBoolean

/**
 * A [Presenter] base implementation which is recommended to be inherited by concrete presenter
 * implementations in order to take advantage of already implemented stubs of [Presenter] interface
 * and also to be 'resistant' against future changes in that interface.
 *
 * Inheritance hierarchies should provide their specific builder which implements [BasePresenter.BaseBuilder]
 * for convenience.
 *
 * @param V  Type of the view that may be attached to the presenter a then used by the presenter for
 * example to perform navigation actions.
 * @param VM Type of the view model of which state may the presenter update according to responses
 * for interaction events.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BasePresenter with the given `builder's` configuration.
 * @param builder The builder with configuration for the new presenter.
 */
abstract class BasePresenter<V : View<*>, out VM : ViewModel>
protected constructor(@NonNull builder: BaseBuilder<*, V, VM>) : Presenter<V> {

    /*
	 * Companion ===================================================================================
	 */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        // internal const val TAG = "BasePresenter"
    }

    /*
	 * Interface ===================================================================================
	 */

    /**
     * Represents a state of registered [ViewAction] and allows canceling of view action before it
     * is performed via [cancel].
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    interface ViewActionRegistration {

        /**
         * Cancels this view action registration.
         *
         * When canceled, the associated view action is removed from registered actions so it will
         * be no longer performed when the corresponding lifecycle event occurs.
         *
         * @see isCanceled
         */
        fun cancel()

        /**
         * Checks whether this registration has been canceled.
         *
         * @return `True` if this registration has been canceled, `false` otherwise.
         *
         * @see cancel
         */
        fun isCanceled(): Boolean
    }

    /*
     * Members =====================================================================================
     */

    /**
     * View model instance associated with this presenter.
     */
    private val viewModel: VM = builder.viewModel

    /**
     * View instance attached to this presenter (if any).
     *
     * @see [attachView]
     * @see [detachView]
     */
    private var view: V? = null

    /**
     * Registry with lifecycle actions registered via [registerViewAction].
     */
    @Suppress("LeakingThis")
    private val lifecycleActionRegistry by lazy { LifecycleActionRegistry(this) }

    /**
     * Boolean flag indicating whether this presenter instance is already destroyed or not.
     *
     * @see [onDestroyed]
     */
    private val destroyed = AtomicBoolean(false)

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the name of this presenter.
     *
     * @return This presenter's name.
     */
    @NonNull fun name(): String = javaClass.simpleName

    /**
     * Returns the view model associated with this presenter.
     *
     * @return View model which this presenter uses to persist view's state.
     */
    protected fun getViewModel(): VM = viewModel

    /*
     */
    override fun attachView(view: V) {
        check(this.view == null) { "Already has view attached!" }

        this.view = view

        if (this is LifecycleObserver) {
            val lifecycle = view.lifecycle

            // Add 'lifecycleActionRegistry' as observer so all view actions registered via [registerViewAction]
            // may be properly performed when their corresponding lifecycle event occurs.
            lifecycle.addObserver(lifecycleActionRegistry)

            lifecycle.addObserver(this)
        }

        onViewAttached(view)
    }

    /**
     * Invoked whenever the specified [view] has been just attached to this presenter.
     *
     * At this point, the view is already attached to this presenter, that is, [isViewAttached]
     * returns `true`.
     *
     * @param view The attached view.
     *
     * @see attachView
     * @see onViewDetached
     */
    protected open fun onViewAttached(view: V) {
        // Inheritance hierarchies may for example register here as lifecycle observers on the attached view.
    }

    /**
     * Checks whether this presenter has view attached at this time or not.
     *
     * @return `True` if there is view attached, `false` otherwise.
     */
    protected fun isViewAttached(): Boolean = view != null

    /**
     * Asserts that this presenter has view attached at this time. If not [IllegalStateException] is thrown.
     */
    protected fun assertViewAttached() {
        view ?: throw IllegalStateException("No view attached!")
    }

    /**
     * Returns the current lifecycle state of the view attached to this presenter.
     *
     * @return Current lifecycle state.
     *
     * @see View.getLifecycle
     * @see Lifecycle.getCurrentState
     */
    protected fun getViewLifecycleCurrentState(): Lifecycle.State = getView().lifecycle.currentState

    /**
     * This is the same as calling [performViewAction(Runnable, Lifecycle.State)][performViewAction]
     * with the given [action] and [Lifecycle.State.RESUMED] as value for `stateCondition` parameter.
     *
     * @param action The desired runnable action to be performed.
     *
     * @see performVisibleViewAction
     */
    protected fun performInteractiveViewAction(action: Runnable): Boolean =
        performViewAction(action, stateCondition = Lifecycle.State.RESUMED)

    /**
     * This is the same as calling [performViewAction(Runnable, Lifecycle.State)][performViewAction]
     * with the given [action] and [Lifecycle.State.STARTED] as value for `stateCondition` parameter.
     *
     * @param action The desired runnable action to be performed.
     *
     * @see performInteractiveViewAction
     */
    protected fun performVisibleViewAction(action: Runnable): Boolean =
        performViewAction(action, stateCondition = Lifecycle.State.STARTED)

    /**
     * A convenience function for performing of simple view runnable action. This is the same as
     * calling [performViewAction(ViewAction, Lifecycle.State)][performViewAction] where the given
     * runnable action is performed when here constructed [ViewAction] is performed via [ViewAction.perform].
     *
     * @param action The desired runnable action to be performed or registered until the desired lifecycle
     * event occurs.
     * @param stateCondition The desired condition determining lifecycle state in which the associated
     * view needs to be in order to perform the action. Default value is [Lifecycle.State.INITIALIZED].
     * @return `True` if action has been performed immediately, `false` if it has been registered for later.
     */
    protected fun performViewAction(
        action: Runnable,
        stateCondition: Lifecycle.State = Lifecycle.State.INITIALIZED
    ): Boolean = performViewAction(object : ViewAction<V> {
        override fun perform(view: V) = action.run()
    }, stateCondition)

    /**
     * Performs the specified [action] immediately if these conditions are met:
     * - view is **attached**,
     * - view's current lifecycle state is **at least** the specified **state condition**
     *
     * otherwise the action is registered for lifecycle event corresponding with that state condition
     * via [registerViewAction].
     *
     * @param action The desired action to be performed or registered until the desired lifecycle
     * event occurs.
     * @param stateCondition The desired condition determining lifecycle state in which the associated
     * view needs to be in order to perform the action. Default value is [Lifecycle.State.INITIALIZED].
     * @return `True` if action has been performed immediately, `false` if it has been registered for later.
     */
    protected fun performViewAction(
        action: ViewAction<V>,
        stateCondition: Lifecycle.State = Lifecycle.State.INITIALIZED
    ): Boolean {
        if (isViewAttached() && getViewLifecycleCurrentState().isAtLeast(stateCondition)) {
            action.perform(getView())

            return true
        }

        registerViewAction(LifecycleActionRegistry.resolveEventForState(stateCondition), action)

        return false
    }

    /**
     * Same as [registerViewAction(Lifecycle.Event, ViewAction)][registerViewAction] where the
     * specified runnable [action] will be transformed into [ViewAction].
     *
     * @param event The lifecycle event for which to register the desired action. If this event occurs,
     * the action will be performed.
     * @param action The desired action to perform as view action.
     * @return Registration associated with the registered action. May be used to cancel the action.
     *
     * @see performViewAction
     */
    protected fun registerViewAction(event: Lifecycle.Event, action: Runnable): ViewActionRegistration =
        registerViewAction(event, object : ViewAction<V> {

            override fun perform(view: V) = action.run()
        })

    /**
     * Registers the specified view [action] to be performed when lifecycle callback is received for
     * the specified lifecycle [event].
     *
     * @param event The lifecycle event for which to register the desired action. If this event occurs,
     * the action will be performed.
     * @param action The desired view action to perform.
     * @return Registration associated with the registered action. May be used to cancel the action.
     *
     * @see performViewAction
     */
    protected fun registerViewAction(event: Lifecycle.Event, action: ViewAction<V>): ViewActionRegistration =
        lifecycleActionRegistry.registerEventAction(event, action)

    /**
     * Returns the view that is currently attached to this presenter.
     *
     * @return View attached.
     * @throws IllegalStateException If there is no view attached to this presenter at this time.
     *
     * @see isViewAttached
     * @see attachView
     * @see detachView
     */
    protected fun getView(): V {
        assertViewAttached()
        return view!!
    }

    /*
     */
    override fun detachView(view: V) {
        if (this.view == null) {
            return
        }

        require(!(this.view !== view)) { "Cannot detach current view. The view passed to detachView(...) is of different reference." }

        this.view = null

        if (this is LifecycleObserver) {
            val lifecycle = view.lifecycle

            lifecycle.removeObserver(lifecycleActionRegistry)
            lifecycle.removeObserver(this)
        }

        onViewDetached(view)
    }

    /**
     * Invoked whenever the specified [view] has been just detached from this presenter.
     *
     * At this point, the view is no longer attached to this presenter, that is, [isViewAttached]
     * returns `false`.
     *
     * @param view The detached view.
     *
     * @see detachView
     * @see onViewAttached
     */
    protected open fun onViewDetached(view: V) {
        // Inheritance hierarchies should for example unregister here as lifecycle observers on the detached view.
    }

    /*
     */
    override fun destroy() {
        if (destroyed.compareAndSet(false, true)) {
            lifecycleActionRegistry.clear()
            onDestroyed()
        }
    }

    /**
     * Invoked when this presenter instance is being destroyed due to call to [destroy].
     *
     * At this point are all lifecycle actions previously registered via [registerViewAction]
     * cleared.
     */
    protected open fun onDestroyed() {
        // Inheritance hierarchies may for example clear here resources that will be no longer used.
    }

    /**
     * Checks whether this presenter instance is destroyed.
     *
     * @return `True` if this presenter has been already destroyed, `false` otherwise.
     *
     * @see destroy
     */
    protected fun isDestroyed(): Boolean = destroyed.get()

    /*
     */
    @NonNull override fun toString(): String = "${name()}(viewAttached: ${view != null}, destroyed: $destroyed)"

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Base implementation of [Presenter.Builder] which should be used by [BasePresenter] inheritance
     * hierarchies.
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     * @param V Type of the view of which instance may be associated with new presenter.
     * @param VM Type of the view model of which instance may be associated with new presenter.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with empty state.
     */
    abstract class BaseBuilder<B : BaseBuilder<B, V, VM>, V : View<*>, VM : ViewModel>
    protected constructor() : Presenter.Builder<Presenter<V>> {

        /**
         * Obtains reference to this builder instance which is used for purpose of methods chaining.
         *
         * @return This builder instance.
         */
        protected abstract val self: B

        /**
         * See [BaseBuilder.viewModel].
         */
        lateinit var viewModel: VM

        /**
         * Specifies a view model to be associated with the new presenter.
         *
         * @param viewModel The desired view model for the presenter.
         * @return This builder to allow methods chaining.
         */
        fun viewModel(@NonNull viewModel: VM): B {
            this.viewModel = viewModel
            return self
        }
    }

    /**
     * A [BaseBuilder] simple implementation which may be used in cases when a concrete implementation
     * of [BasePresenter] does not require any additional dependencies except its view model.
     * Such presenters may be instantiated simply by passing instance of this builder created via
     * [SimpleBuilder.create] to such presenters's constructor.
     *
     * @param V Type of the view of which instance may be associated with new presenter.
     * @param VM Type of the view model of which instance may be associated with new presenter.
     *
     * @author Martin Albedinsky
     * @since 1.0
     */
    class SimpleBuilder<V : View<*>, VM : ViewModel> private constructor() : BaseBuilder<SimpleBuilder<V, VM>, V, VM>() {

        /**
         */
        companion object {

            /**
             * Creates a new SimpleBuilder with the specified [viewModel].
             *
             * @param viewModel The desired view model for the new presenter.
             * @param V Type of the view of which instance may be associated with the new presenter.
             * @param VM Type of the view model of which instance may be associated with the new presenter.
             * @return Builder instance ready to be passed to the presenter's constructor.
             */
            @JvmStatic @NonNull
            fun <V : View<*>, VM : ViewModel> create(viewModel: VM): SimpleBuilder<V, VM> =
                SimpleBuilder<V, VM>().apply { this.viewModel = viewModel }
        }

        /*
         */
        override val self = this

        /*
         */
        override fun build(): Presenter<V> =
            throw UnsupportedOperationException("Pass instance of SimpleBuilder to the appropriate constructor!")
    }

    /**
     * [ViewActionRegistration] implementation.
     *
     * @constructor Creates a new instance of ViewActionRegistrationImpl with the specified attributes.
     * @param actions List of actions where is the specified [action] registered.
     * @param action The desired action for which is the new registration being created.
     * @param V Type of the view for which are actions registered.
     */
    private open class ViewActionRegistrationImpl<V : View<*>>(
        /**
         * List of actions where is the given [action] registered.
         */
        private val actions: MutableList<ViewAction<V>>,
        /**
         * The registered action for which is this registration created. When this registration
         * is canceled via [cancel][ViewActionRegistrationImpl.cancel] this action will be
         * removed from the registered [actions] so it will be no longer performed.
         */
        private val action: ViewAction<V>
    ) : ViewActionRegistration {

        /**
         * Boolean flag indicating whether this subscription is already canceled or not.
         */
        private val canceled = AtomicBoolean()

        /*
         */
        override fun cancel() {
            if (canceled.compareAndSet(false, true)) {
                actions.remove(action)
            }
        }

        /*
         */
        override fun isCanceled(): Boolean = canceled.get()
    }

    /**
     * Registry for [ViewActions][ViewAction] registered for a specific [Lifecycle.Event].
     * This registry also acts as [LifecycleObserver] in order to receive lifecycle event related
     * callbacks so the actions may be run according to their associated lifecycle event.
     *
     * @param presenter The presenter for which this registry manages lifecycle actions.
     */
    @Suppress("unused")
    internal class LifecycleActionRegistry<V : View<*>>(private val presenter: BasePresenter<V, *>) : LifecycleObserver {

        /**
         */
        companion object {

            /**
             * Resolves proper lifecycle event for the specified lifecycle [state].
             *
             * @param state The desired state for which to resolve its associated event.
             * @return Resolved lifecycle event.
             */
            fun resolveEventForState(state: Lifecycle.State): Lifecycle.Event {
                return when (state) {
                    Lifecycle.State.INITIALIZED -> Lifecycle.Event.ON_ANY
                    Lifecycle.State.CREATED -> Lifecycle.Event.ON_CREATE
                    Lifecycle.State.STARTED -> Lifecycle.Event.ON_START
                    Lifecycle.State.RESUMED -> Lifecycle.Event.ON_RESUME
                    Lifecycle.State.DESTROYED -> Lifecycle.Event.ON_DESTROY
                    else -> Lifecycle.Event.ON_ANY
                }
            }
        }

        /**
         * List of registered actions for [ON_DESTROY][Lifecycle.Event.ON_CREATE] event.
         */
        private val createActions: MutableList<ViewAction<V>> by lazy { mutableListOf() }

        /**
         * List of registered actions for [ON_DESTROY][Lifecycle.Event.ON_START] event.
         */
        private val startActions: MutableList<ViewAction<V>> by lazy { mutableListOf() }

        /**
         * List of registered actions for [ON_DESTROY][Lifecycle.Event.ON_RESUME] event.
         */
        private val resumeActions: MutableList<ViewAction<V>> by lazy { mutableListOf() }

        /**
         * List of registered actions for [ON_DESTROY][Lifecycle.Event.ON_PAUSE] event.
         */
        private val pauseActions: MutableList<ViewAction<V>> by lazy { mutableListOf() }

        /**
         * List of registered actions for [ON_DESTROY][Lifecycle.Event.ON_STOP] event.
         */
        private val stopActions: MutableList<ViewAction<V>> by lazy { mutableListOf() }

        /**
         * List of registered actions for [ON_DESTROY][Lifecycle.Event.ON_DESTROY] event.
         */
        private val destroyActions: MutableList<ViewAction<V>> by lazy { mutableListOf() }

        /**
         * Registers the given [action] for the specified [event]. The registered action will be run
         * when this registry receives callback for the associated lifecycle event from the lifecycle
         * pipeline.
         *
         * @param event The event for which to register the desired action.
         * @param action The desired action to be performed when the specified lifecycle event occurs.
         * @return Registration for the registered action which may be canceled if desired.
         * @throws IllegalArgumentException If the event is [ON_ANY][Lifecycle.Event.ON_ANY].
         */
        fun registerEventAction(event: Lifecycle.Event, action: ViewAction<V>): ViewActionRegistration {
            val actions = when (event) {
                Lifecycle.Event.ON_CREATE -> createActions.apply { add(action) }
                Lifecycle.Event.ON_START -> startActions.apply { add(action) }
                Lifecycle.Event.ON_RESUME -> resumeActions.apply { add(action) }
                Lifecycle.Event.ON_PAUSE -> pauseActions.apply { add(action) }
                Lifecycle.Event.ON_STOP -> stopActions.apply { add(action) }
                Lifecycle.Event.ON_DESTROY -> destroyActions.apply { add(action) }
                Lifecycle.Event.ON_ANY -> throw IllegalArgumentException("Cannot register lifecycle action for event($event)!")
            }

            ArkhitektonLogging.d(presenter.name(), "Registered lifecycle action for event($event).")

            return ViewActionRegistrationImpl(actions, action)
        }

        /**
         * Invoked by the lifecycle pipeline whenever view becomes **created**.
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
        fun onViewCreated() = performViewStateActions(createActions, "CREATED")

        /**
         * Invoked by the lifecycle pipeline whenever view becomes **started**.
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        fun onViewStarted() = performViewStateActions(startActions, "STARTED")

        /**
         * Invoked by the lifecycle pipeline whenever view becomes **resumed**.
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun onViewResumed() = performViewStateActions(resumeActions, "RESUMED")

        /**
         * Invoked by the lifecycle pipeline whenever view becomes **paused**.
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun onViewPaused() = performViewStateActions(pauseActions, "PAUSED")

        /**
         * Invoked by the lifecycle pipeline whenever view becomes **stopped**.
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onViewStop() = performViewStateActions(stopActions, "STOPPED")

        /**
         * Invoked by the lifecycle pipeline whenever view becomes **destroyed**.
         */
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onViewDestroyed() = performViewStateActions(destroyActions, "DESTROYED")

        /**
         * Runs all specified [actions]. When run, the list of actions will be cleared.
         *
         * @param actions List of actions to be run.
         * @param viewStateName Name of the current view state for which the actions will be run.
         */
        private fun performViewStateActions(actions: MutableList<ViewAction<V>>, viewStateName: String) {
            ArkhitektonLogging.d(
                presenter.name(),
                "View $viewStateName. ${
                    if (actions.size == 0) "No registered actions to run."
                    else "Running registered actions in count(${actions.size})."
                }"
            )

            if (actions.isNotEmpty()) {
                val view = presenter.getView()

                actions.forEach { it.perform(view) }

                actions.clear()
            }
        }

        /**
         * Clears all registered actions.
         */
        fun clear() {
            createActions.clear()
            startActions.clear()
            resumeActions.clear()
            pauseActions.clear()
            stopActions.clear()
            destroyActions.clear()
        }
    }
}