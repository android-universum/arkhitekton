Arkhitekton-Interaction-Base
===============

This module contains **base implementation** of `Interactor` and `UseCase` which may be used as base
for specific interactor and use case implementations.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Aarkhitekton/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Aarkhitekton/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:arkhitekton-interaction-base:${DESIRED_VERSION}@aar"

_depends on:_
[arkhitekton-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-core),
[arkhitekton-util](https://bitbucket.org/android-universum/arkhitekton/src/main/library-util),
[arkhitekton-interaction-core](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ParamRequest](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-base/src/main/java/universum/studios/android/arkhitekton/interaction/ParamRequest.kt)
- [BaseInteractor](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-base/src/main/java/universum/studios/android/arkhitekton/interaction/BaseInteractor.kt)
- [BaseUseCase](https://bitbucket.org/android-universum/arkhitekton/src/main/library-interaction-base/src/main/java/universum/studios/android/arkhitekton/interaction/usecase/BaseUseCase.kt)
