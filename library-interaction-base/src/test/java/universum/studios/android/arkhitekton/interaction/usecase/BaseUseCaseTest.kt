/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction.usecase

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.greaterThan
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import universum.studios.android.arkhitekton.interaction.Request
import universum.studios.android.arkhitekton.interaction.Response
import universum.studios.android.arkhitekton.interaction.usecase.BaseUseCaseTest.TestUseCase.TestBuilder
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.ErrorException
import universum.studios.android.arkhitekton.util.ExecutionUtils
import universum.studios.android.arkhitekton.util.Value
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class BaseUseCaseTest : TestCase {

    @Test fun contract() {
        // Assert:
        assertThat(BaseUseCase.IMMEDIATE_EXECUTION, `is`(0L))
    }

    @Test fun instantiation() {
        // Arrange:
        val useCase = TestUseCase.TestBuilder().build()

        // Assert:
        assertThat(useCase.minExecutionDuration, `is`(BaseUseCase.IMMEDIATE_EXECUTION))
    }

    @Test fun instantiationWithMinExecutionDuration() {
        // Arrange:
        val useCase = TestUseCase.TestBuilder()
            .minExecutionDuration(1000L)
            .build()

        // Assert:
        assertThat(useCase.minExecutionDuration, `is`(1000L))
    }

    @Test fun name() {
        // Arrange:
        val useCase = TestUseCase.TestBuilder().build()

        // Assert:
        assertThat(useCase.name(), `is`("TestUseCase"))
    }

    @Test fun execute() {
        // Arrange:
        val useCase = TestUseCase.TestBuilder().build()

        // Act:
        val response = useCase.execute(Request.empty())

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
    }

    @Test fun executeWithMinimumExecutionDuration() {
        // Arrange:
        val systemTime = System.currentTimeMillis()
        val useCase = TestUseCase.TestBuilder().minExecutionDuration(250L).build()

        // Act:
        val response = useCase.execute(Request.empty())

        // Assert:
        assertThat(System.currentTimeMillis() - systemTime, `is`(greaterThan(249L)))
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
    }

    @Test fun executeWithMinimumExecutionDurationLowerThanRealExecutionDuration() {
        // Arrange:
        val systemTime = System.currentTimeMillis()
        val useCase = TestUseCase.TestBuilder().minExecutionDuration(250L).build()
        useCase.executionDuration = 500L

        // Act:
        val response = useCase.execute(Request.empty())

        // Assert:
        assertThat(System.currentTimeMillis() - systemTime, `is`(greaterThan(499L)))
        assertThat(response, `is`(notNullValue()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
    }

    @Test fun createSuccess() {
        // Arrange:
        val useCase = TestUseCase.TestBuilder().build()

        // Act:
        val response = useCase.callCreateSuccess(Request.empty(), Value.EMPTY)

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertTrue(response.isSuccess())
        assertThat(response.getValue(), `is`(Value.EMPTY))
        assertFalse(response.isFailure())
    }

    @Test fun createFailure() {
        // Arrange:
        val error = ErrorException.create("ERROR")
        val useCase = TestUseCase.TestBuilder().build()

        // Act:
        val response = useCase.callCreateFailure(Request.empty(), error)

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(error))
        assertFalse(response.isSuccess())
    }

    @Test fun createFailureWithRequestOnly() {
        // Arrange:
        val useCase = TestUseCase.TestBuilder().build()

        // Act:
        val response = useCase.callCreateFailure(Request.empty())

        // Assert:
        assertThat(response, `is`(notNullValue()))
        assertThat(response.getRequest(), `is`(Request.empty()))
        assertTrue(response.isFailure())
        assertThat(response.getError(), `is`(Error.unknown()))
        assertFalse(response.isSuccess())
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val useCase = TestBuilder().build()

        // Assert:
        assertThat(useCase.toString(), `is`("TestUseCase(minExecutionDuration: 0)"))
    }

    class TestUseCase(builder: TestBuilder) : BaseUseCase<Request.Empty, Value.Empty>(builder) {

        var executionDuration = 0L

        override fun onExecute(request: Request.Empty): Response<Value.Empty> {
            if (executionDuration > 0L) {
                ExecutionUtils.sleepFor(executionDuration)
            }

            return createSuccess(request, Value.EMPTY)
        }

        fun callCreateSuccess(request: Request.Empty, resultValue: Value.Empty) = createSuccess(request, resultValue)

        fun callCreateFailure(request: Request.Empty, error: Error = Error.unknown()) = createFailure(request, error)

        class TestBuilder : BaseUseCase.BaseBuilder<TestBuilder>() {

            override val self = this

            override fun build() = TestUseCase(this)
        }
    }
}