/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import universum.studios.android.testing.TestCase

/**
 * @author Martin Albedinsky
 */
class BaseInteractorTest : TestCase {

    @Test fun instantiation() {
        // Act:
        val interactor = TestInteractor.TestBuilder().build()

        // Assert:
        assertThat(interactor, `is`(notNullValue()))
    }

    @Test fun name() {
        // Arrange:
        val interactor = TestInteractor.TestBuilder().build()

        // Assert:
        assertThat(interactor.name(), `is`("TestInteractor"))
    }

    @Test fun toStringImplementation() {
        // Arrange:
        val interactor = TestInteractor.TestBuilder().build()

        // Assert:
        assertThat(interactor.toString(), `is`("TestInteractor()"))
    }

    private class TestInteractor(builder: TestBuilder) : BaseInteractor(builder) {

        class TestBuilder : BaseInteractor.BaseBuilder<TestBuilder>() {

            override val self = this
            override fun build() = TestInteractor(this)
        }
    }
}