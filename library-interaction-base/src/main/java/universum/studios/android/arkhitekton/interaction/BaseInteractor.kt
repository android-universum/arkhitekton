/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction

import androidx.annotation.NonNull

/**
 * An [Interactor] base implementation which is recommended to be inherited by concrete interactor
 * implementations in order to take advantage of already implemented stubs of [Interactor] interface
 * and also to be 'resistant' against future changes in that interface.
 *
 * Inheritance hierarchies should provide their specific builder which implements [BaseInteractor.BaseBuilder]
 * for convenience.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BaseInteractor with configuration provided by the given `builder`.
 * @param builder The builder with configuration for the new interactor.
 */
abstract class BaseInteractor protected constructor(
    @Suppress("UNUSED_PARAMETER") @NonNull builder: BaseBuilder<*>
) : Interactor {

    /*
	 * Companion ===================================================================================
	 */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        // internal const val TAG = "BaseInteractor"
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the name of this interactor.
     *
     * @return This use interactor's name.
     */
    @NonNull open fun name(): String = javaClass.simpleName

    /*
     */
    @NonNull override fun toString(): String = "${name()}()"

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Base implementation of [Interactor.Builder] which should be used by [BaseInteractor] inheritance
     * hierarchies.
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with empty state.
     */
    abstract class BaseBuilder<B : BaseBuilder<B>> protected constructor() : Interactor.Builder<Interactor> {

        /**
         * Obtains reference to this builder instance which is used for purpose of methods chaining.
         *
         * @return This builder instance.
         */
        protected abstract val self: B
    }
}