/*
 * *************************************************************************************************
 *                                 Copyright 2018 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.arkhitekton.interaction.usecase

import androidx.annotation.IntRange
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import universum.studios.android.arkhitekton.interaction.Response
import universum.studios.android.arkhitekton.interaction.Responses
import universum.studios.android.arkhitekton.util.Error
import universum.studios.android.arkhitekton.util.ExecutionUtils
import universum.studios.android.arkhitekton.util.Requirements

/**
 * A [UseCase] base implementation which is recommended to be inherited by concrete use case
 * implementations in order to take advantage of already implemented stubs of [UseCase] interface
 * and also to be 'resistant' against future changes in that interface.
 *
 * Inheritance hierarchies should provide their specific builder which implements [BaseUseCase.BaseBuilder]
 * for convenience.
 *
 * @author Martin Albedinsky
 * @since 1.0
 *
 * @constructor Creates a new instance of BaseUseCase with the given `builder's` configuration.
 * @param builder The builder with configuration for the new use case.
 */
abstract class BaseUseCase<in Request : universum.studios.android.arkhitekton.interaction.Request, ResultValue>
protected constructor(@NonNull builder: BaseBuilder<*>) : UseCase<Request, ResultValue> {

    /*
	 * Companion ===================================================================================
	 */

    /**
     */
    companion object {

        /**
         * Log TAG.
         */
        // internal const val TAG = "BaseUseCase"

        /**
         * Duration which ensures immediate execution.
         */
        const val IMMEDIATE_EXECUTION = 0L
    }

    /*
	 * Interface ===================================================================================
	 */

    /*
     * Members =====================================================================================
     */

    /**
     * Duration in milliseconds that the execution of this use case should take at the minimum.
     */
    @VisibleForTesting internal val minExecutionDuration = Requirements.requireNonNegative(
        builder.minExecutionDuration,
        "Min execution duration must be non-negative!"
    )

    /*
     * Initialization ==============================================================================
     */

    /*
     * Functions ===================================================================================
     */

    /**
     * Returns the name of this use case.
     *
     * @return This use case's name.
     */
    @NonNull open fun name(): String = javaClass.simpleName

    /*
     */
    override fun execute(@NonNull request: Request): Response<ResultValue> {
        if (minExecutionDuration == IMMEDIATE_EXECUTION) {
            return onExecute(request)
        }

        val startTime = System.currentTimeMillis()

        val response = onExecute(request)

        val executionDuration = System.currentTimeMillis() - startTime
        if (executionDuration < minExecutionDuration) {
            ExecutionUtils.sleepFor(minExecutionDuration - executionDuration)
        }

        return response
    }

    /**
     * Called whenever [execute] is invoked upon this use case instance.
     *
     * @param request The request specified by an executor of this use case.
     * @return Either success or failure response to be delivered to the executor of this use case.
     */
    protected abstract fun onExecute(@NonNull request: Request): Response<ResultValue>

    /**
     * Creates a new response which will represent a success.
     *
     * @param request The request to associate the new response with.
     * @param resultValue  The desired result value for the new response.
     * @return Response with the given [resultValue].
     *
     * @see Responses.createSuccess
     */
    protected fun createSuccess(
        @NonNull request: Request,
        @NonNull resultValue: ResultValue
    ): Response<ResultValue> = Responses.createSuccess(request, resultValue)

    /**
     * Creates a new response which will represent a failure.
     *
     * @param request The request to associate the new response with.
     * @param error The desired error for the new response. Default is [UNKNOWN][Error.unknown].
     * @return Response with the given [error].
     *
     * @see Responses.createFailure
     */
    @JvmOverloads protected fun createFailure(
        @NonNull request: Request,
        @NonNull error: Error = Error.unknown()
    ): Response<ResultValue> = Responses.createFailure(request, error)

    /*
     */
    @NonNull override fun toString(): String = "${name()}(minExecutionDuration: $minExecutionDuration)"

    /*
     * Inner classes ===============================================================================
     */

    /**
     * Base implementation of [UseCase.Builder] which should be used by [BaseUseCase] inheritance
     * hierarchies.
     *
     * @param B Type of the builder used as return type for builder's chain-able methods.
     *
     * @author Martin Albedinsky
     * @since 1.0
     *
     * @constructor Creates a new instance of BaseBuilder with [IMMEDIATE_EXECUTION] as minimum
     * execution duration.
     */
    abstract class BaseBuilder<B : BaseBuilder<B>> protected constructor() : UseCase.Builder<UseCase<*, *>> {

        /**
         * See [BaseUseCase.minExecutionDuration].
         */
        var minExecutionDuration: Long = IMMEDIATE_EXECUTION

        /**
         * Obtains reference to this builder instance which is used for purpose of methods chaining.
         *
         * @return This builder instance.
         */
        protected abstract val self: B

        /**
         * Specifies a duration that the execution of the new use case should take at the minimum.
         *
         * This may be useful in cases when a specific use case executes really fast (in terms of
         * user interface context) and there is about to be displayed a loading indicator in a view
         * with an animation taking more time than the actual execution duration of that use case.
         *
         * @param duration The desired duration in milliseconds.
         * @return This builder to allow methods chaining.
         */
        fun minExecutionDuration(@IntRange(from = 0) duration: Long): B {
            this.minExecutionDuration = duration
            return self
        }
    }
}