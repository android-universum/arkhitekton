Change-Log
===============
> Regular maintenance: _02.03.2024_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 3.0 ##

### [3.0.2](https://bitbucket.org/android-universum/arkhitekton/wiki/version/3.x)
> 02.11.2021

- `ErrorCause` now extends `RuntimeException` for better propagation in **reactive streams**.
- `Errors.unwrap(Throwable)` now properly returns **error** of `ErrorCause`.

### [3.0.1](https://bitbucket.org/android-universum/arkhitekton/wiki/version/3.x)
> 28.10.2021

- **Deprecated** `Error.asError(Throwable)` along with `Error.unwrap(Throwable)` and replaced with
  improved implementations in `Errors` utility class.
- Removed **deprecated functions** from version `2.x`.

### [3.0.0](https://bitbucket.org/android-universum/arkhitekton/wiki/version/3.x)
> 30.05.2021

- Added ability to check whether a `Controller` is **inactive** or **disabled**. [#37](https://bitbucket.org/android-universum/arkhitekton/issues/37)
- `Error` now allows **null** cause via `Error.cause()` function.
    - Error constants defined in `Error` interface by default have no cause specified (cause is `null`).
    - Removed `Cause` class.
- Regular **maintenance**:
    - compiled with **Kotlin 1.5.10**
    - compiled with **AGP 4.2.1**

## [Version 2.0](https://bitbucket.org/android-universum/arkhitekton/wiki/version/2.x) ##
## [Version 1.0](https://bitbucket.org/android-universum/arkhitekton/wiki/version/1.x) ##
## [Version 0.x](https://bitbucket.org/android-universum/arkhitekton/wiki/version/0.x) ##
